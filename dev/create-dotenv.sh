#!/bin/sh

if [ -f ".env" ]; then
    echo ".env file alredy exists"
    exit 1
fi

cat > .env <<- EOF
DEBUG=1
DATABASE_URL=psql://intake_platform:intake_platform@127.0.0.1:5432/intake_platform_dev
SECRET_KEY="50 symbols of randomness or even less"
TELEGRAM_BOT_TOKEN=1
ROBOT_PASSWORD=sara
MAILGUN_API_KEY=1
SENTRY_DSN=1
AUTH0_DOMAIN=auth0.com
AUTH0_KEY=1
AUTH0_SECRET=1
REDIS_URL=redis://127.0.0.1:6379
EOF
