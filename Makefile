PORT?=8000
SRC_DIR = intake_platform/

init_env:
	./dev/create-dotenv.sh
	docker-compose -p intake_platform -f dev/docker-compose.yml up -d
	sleep 10
	poetry run ./manage.py migrate
	./dev/create-superuser.sh

start_env:
	docker-compose -p intake_platform -f dev/docker-compose.yml start

stop_env:
	docker-compose -p intake_platform -f dev/docker-compose.yml stop

django-run:
	set -a && \
	    poetry run python ./manage.py runserver ${PORT}

django-shell:
	set -a && \
	    poetry run python ./manage.py shell

django-makemigrations:
	set -a && \
	    poetry run python ./manage.py makemigrations

django-applymigrations:
	set -a && \
	    poetry run python ./manage.py migrate

django-dbshell:
	set -a && \
	    poetry run python ./manage.py dbshell

poetry-shell:
	set -a && \
	    poetry shell


#
# Linting
#

lint-isort:
	find ${SRC_DIR} -iname '*.py' -exec isort --check-only {} +

lint-black:
	black --check --skip-string-normalization --line-length 100 ${SRC_DIR}

lint-black-fix:
	black --skip-string-normalization --line-length 100 ${SRC_DIR}

lint-flake:
	flake8 ${SRC_DIR}

@lint: lint-black lint-flake lint-isort
