#!/bin/sh

./manage.py collectstatic --noinput
./manage.py migrate
./manage.py create_robot
mkdir -p /var/run
gunicorn --bind unix:/var/run/intake_platform.sock wsgi:application
