"""
Django settings for intake-platform project.
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

import environ
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from intake_platform.intake_platform.curators_dict_2020 import curators_dict

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

env = environ.Env(
    DEBUG=(bool, False),
    ALLOWED_HOSTS=(str,),
    USE_MAILGUN=(bool, False),
    SENTRY_DSN=str,
    AUTH0_DOMAIN=str,
    AUTH0_KEY=str,
    AUTH0_SECRET=str,
    REDIS_URL=str,
    VERSION=(str, "master"),
)

SECRET_KEY = env("SECRET_KEY")
DEBUG = env("DEBUG")
VERSION = env("VERSION")

REDIS_URL = env("REDIS_URL")
REDIS_KEY_WP_SELECT_UNDERAGE = 'wp_select_underage_cache'
REDIS_KEY_WP_SELECT_ADULT = 'wp_select_adult_cache'
REDIS_WP_SELECT_TTL = 30 * 60  # 30 minutes

if not DEBUG:
    sentry_sdk.init(
        dsn=env("SENTRY_DSN"),
        integrations=[DjangoIntegration()],
        send_default_pii=True
    )

    sentry_sdk.set_extra("project_version", VERSION)

AUTH_PROFILE_MODULE = "intake_platform.common.UserProfile"

if DEBUG:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

if env("USE_MAILGUN"):
    ANYMAIL = {
        "MAILGUN_API_KEY": env("MAILGUN_API_KEY"),
        "MAILGUN_SENDER_DOMAIN": "mg.letnyayashkola.org",
        "MAILGUN_API_URL": "https://api.eu.mailgun.net/v3",
    }
    EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"

ALLOWED_HOSTS = ["*"] if DEBUG else env("ALLOWED_HOSTS")
TELEGRAM_BOT_TOKEN = env("TELEGRAM_BOT_TOKEN")

ROBOT_USERNAME = "sarah.connor"
ROBOT_PASSWORD = env("ROBOT_PASSWORD")

ADMINS = (("garik", "garik@letnyayashkola.org"),)
MANAGERS = ADMINS

SERVER_EMAIL = "no-reply-no-plz@letnyayashkola.ru"

# This email is used when we send an email using regular django stuff
DEFAULT_FROM_EMAIL = "no-reply@letnyayashkola.ru"

# Application definition
INSTALLED_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    # Third-party
    "selectable",  # django-selectable
    "annoying",  # JSONField and other goodness
    "bootstrap3_datetime",  # datetime3 datepicker widget
    "crispy_forms",
    "anymail",
    # My apps
    "social_django",
    "intake_platform.common",
    "intake_platform.intake",
    "intake_platform.auth0login",
    # 'veggies', # test app
)

CRISPY_TEMPLATE_PACK = "bootstrap3"

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "intake_platform.intake_platform.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR + "/templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "intake_platform.intake_platform.context_processors.now",
                "intake_platform.intake_platform.context_processors.version",
            ]
        },
    }
]


DATABASES = {"default": env.db()}
LANGUAGE_CODE = "ru-RU"
TIME_ZONE = "Europe/Moscow"
USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"

STATIC_ROOT = os.path.join(BASE_DIR, "static")
if DEBUG:
    STATICFILES_DIRS = (
        ('css', os.path.join(BASE_DIR, 'static', 'css')),
        ('fonts', os.path.join(BASE_DIR, 'static', 'fonts')),
        ('js', os.path.join(BASE_DIR, 'static', 'js')),
        ('img', os.path.join(BASE_DIR, 'static', 'img')),
    )
STATIC_URL = "/static/"

AUTHENTICATION_BACKENDS = (
    "intake_platform.auth0login.auth0backend.Auth0",
    "django.contrib.auth.backends.ModelBackend",
)

SOCIAL_AUTH_TRAILING_SLASH = False  # Remove trailing slash from routes
SOCIAL_AUTH_AUTH0_DOMAIN = env('AUTH0_DOMAIN')
SOCIAL_AUTH_AUTH0_KEY = env('AUTH0_KEY')
SOCIAL_AUTH_AUTH0_SECRET = env('AUTH0_SECRET')

SOCIAL_AUTH_AUTH0_SCOPE = ['openid', 'profile', 'email']

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
    'intake_platform.auth0login.enrichers.create_profile',
    # TODO: Add forms to save email/gender/birthdate
    # Also update location?
    # see https://github.com/omab/django-social-auth/blob/master/example/app/pipeline.py
)


LOGIN_URL = "/login/auth0"
LOGIN_REDIRECT_URL = "/accounts/profile/"

SITE_ID = 1

# TODO: read start/end dates from env variables?
LSH_DATES_DICT = {"start": (2020, 7, 11), "end": (2020, 8, 10)}

INTAKE_IS_CLOSED = False
CURATORS = curators_dict
