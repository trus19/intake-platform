import json
from typing import Any, Dict, List

import redis
import requests

from annoying.functions import get_config


def get_redis_wp_select_key(is_adult: bool = True) -> str:
    if is_adult:
        return get_config('REDIS_KEY_WP_SELECT_ADULT')

    return get_config('REDIS_KEY_WP_SELECT_UNDERAGE')


def warm_up_wp_info_cache(fail_silently: bool = True, verbose: bool = False) -> None:
    BASE_URL = get_config('BASE_LSH_ORG_URL', 'https://letnyayashkola.org')
    urls = {
        'adult': f'{BASE_URL}/api/v1.0/workshops/',
        'underage': f'{BASE_URL}/api/v1.0/workshops/underage/',
    }

    data = {}
    for tag, url in urls.items():
        if verbose:
            print(tag, url)
        r = requests.get(url, verify=False, timeout=3)
        data[tag] = r.json()

    redis_url = get_config('REDIS_URL')
    redis_connection = redis.from_url(redis_url)

    if not fail_silently:
        assert redis_connection.ping(), f'Unable to ping redis server at {redis_url}'
    redis_keys = {
        'adult': get_redis_wp_select_key(is_adult=True),
        'underage': get_redis_wp_select_key(is_adult=False),
    }
    ttl = get_config('REDIS_WP_SELECT_TTL')

    for tag, wp_info in data.items():
        redis_key = redis_keys[tag]
        serialized_data = json.dumps(wp_info)
        redis_connection.set(redis_key, serialized_data, ex=ttl)
        if verbose:
            print(f'Saved data for {tag} at {redis_key} in {redis_url}')


def get_wp_cache(
    is_adult: bool = True, fail_silently: bool = True, verbose: bool = False,
) -> List[Dict[str, Any]]:
    """ Return data for underage and adult users """
    redis_url = get_config('REDIS_URL')
    redis_connection = redis.from_url(redis_url)

    if not fail_silently:
        assert redis_connection.ping(), f'Unable to ping redis server at {redis_url}'

    redis_key = get_redis_wp_select_key(is_adult=is_adult)
    tag = 'adult' if is_adult else 'underage'

    value = redis_connection.get(redis_key)
    if value is None:
        if verbose:
            print(f'Value for {tag} was not found in redis cache')
        return []

    data = json.loads(value.decode())
    return data
