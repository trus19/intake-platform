from django.conf.urls import url

from intake_platform.intake import staff_views, views
from intake_platform.intake.api.views import bucks_by_date

first_step_urls = [
    url(r'^api/v1.0/bucks-by-date/$', bucks_by_date),
    url(
        r"^tests/lsh101/$",
        views.check_user_not_dumb_prequel,
        name="check_user_not_dumb_prequel",
    ),
    url(
        r"^tests/lsh101/actual-test/$",
        views.check_user_not_dumb_actual_test,
        name="check_user_not_dumb_actual_test",
    ),
    url(r"^single-shot/wp-select/$", views.wp_select, name="program_select"),
    url(r"^single-shot/basic-info/$", views.provide_basic_info, name="provide_basic_info"),
    url(r"^single-shot/contacts/$", views.provide_contacts, name="provide_contacts"),
    url(
        r"^single-shot/education/$",
        views.collect_education_info,
        name="collect_education_info",
    ),
    url(r"^single-shot/activity/$", views.get_activity_info, name="get_activity_info"),
    url(
        r"^single-shot/handyman/$",
        views.demand_handymanship,
        name="demand_handymanship",
    ),
    url(r"^single-shot/money/$", views.fill_in_money, name="fill_in_money"),
    url(r"^single-shot/logistics/$", views.choose_my_way, name="choose_my_way"),
    url(r"^single-shot/promise-help/$", views.promise_help, name="promise_help"),
    url(r"^single-shot/retrospect/$", views.retrospect, name="retrospect"),
    url(r"^single-shot/leak/$", views.leakage, name="leakage"),
    url(r"^single-shot/upload-photo/$", views.upload_photo, name="upload_photo"),
    url(r"^single-shot/parents/$", views.parents, name="parents"),
    url(
        r"^single-shot/complete-first-step/$",
        views.complete_first_step,
        name="complete_first_step",
    ),
    url(r"^timeline/$", views.timeline, name="timeline"),
    url(r"^my-application/$", views.OwnAppReadOnly.as_view(), name="own_app_readonly"),
    url(r"^messages/$", views.Messages.as_view()),
    url(
        r"^emails/show-decline/$",
        views.ReadDeclineEmail.as_view(),
        name="read_decline_email",
    ),
    url(
        r"^homework/do/$",
        views.AssignmentResponse.as_view(),
        name="assignment_response",
    ),
    url(
        r"^homework/download/$",
        views.AssignmentDownload.as_view(),
        name="assignment_download",
    ),
    url(
        r"^homework/my-response-download/$",
        views.AssignmentMyResponseDownload.as_view(),
        name="assignment_my_response_download",
    ),
    #   url(
    #       r"^go-rozetka/$",
    #       views.SecretRozetkaSignUp.as_view(),
    #       name="organizer-rozetka-override",
    #   ),
    #   url(
    #       r"^go-neudobno/$",
    #       views.SecretNeudobnoSignUp.as_view(),
    #       name="organizer-neudobno-override",
    #   ),
    #   url(
    #       r"^go-inclusio/$",
    #       views.SecretInclusioSignUp.as_view(),
    #       name="organizer-inclusio-override",
    #   ),
    #   url(r"^go-field/$", views.SecretFRCSignUp.as_view(), name="frc-override"),
    #   url(r"^go-bypass/$", views.SecretGenericSignUp.as_view(), name="generic-override"),
    url(
        r"^singe-shot/wpc-motivation-edit/$",
        views.WPCMotivationEdit.as_view(),
        name="wpc-motivation-edit",
    ),
]

ajax_urls = [
    url(r"^emails/$", views.emails, name="ajax_emails"),
    url(
        r"^workshop-programs/$", views.workshop_programs, name="ajax_workshop_programs"
    ),
]

staff_urls = [
    url(r"^staff/apps/$", staff_views.AppSearch.as_view(), name="app_search"),
    url(r"^staff/apps/fresh/$", staff_views.FreshAppsView.as_view(), name="fresh_apps"),
    url(
        r"^staff/apps/photo-change-requested/$",
        staff_views.PhotoChangeRequestedAppsView.as_view(),
        name="photo_change_requested_apps",
    ),
    url(r"^staff/twats/$", staff_views.BlackListView.as_view(), name="blacklist"),
    url(
        r"^staff/apps/fresh/(?P<w_slug>[\w-]+)/$",
        staff_views.WorkshopFreshAppsView.as_view(),
        name="workshop_fresh_apps_specific",
    ),
    url(
        r"^staff/apps/suspicious/$",
        staff_views.SuspiciousAppsView.as_view(),
        name="suspicious_apps",
    ),
    url(
        r"^staff/apps/passed/$",
        staff_views.PassedAppsView.as_view(),
        name="passed_apps",
    ),
    url(
        r"^staff/apps/accepted/$",
        staff_views.AllAcceptedApps.as_view(),
        name="workshop_accepted_apps",
    ),
    url(
        r"^staff/apps/accepted/(?P<w_slug>[\w-]+)/$",
        staff_views.WorkshopAcceptedApps.as_view(),
        name="workshop_accepted_apps_specific",
    ),
    url(
        r"^staff/apps/accepted/(?P<w_slug>[\w-]+)/(?P<p_slug>[\w-]+)/$",
        staff_views.WorkshopProgramAcceptedApps.as_view(),
        name="workshop_program_accepted_apps",
    ),
    url(
        r"^staff/apps/rejected/$",
        staff_views.RejectedAppsView.as_view(),
        name="rejected_apps",
    ),
    url(
        r"^staff/apps/suspicious/$",
        staff_views.SuspiciousAppsView.as_view(),
        name="suspicious_apps",
    ),
    url(
        r"^staff/apps/workshop-declined/$",
        staff_views.WorkshopDeclinedApps.as_view(),
        name="workshop_declined_apps",
    ),
    url(
        r"^staff/apps/workshop-declined/(?P<w_slug>[\w-]+)/$",
        staff_views.WorkshopDeclinedAppsSpecific.as_view(),
        name="workshop_declined_apps_specific",
    ),
    url(
        r"^staff/apps/(?P<pk>[0-9]+)/$",
        staff_views.AppDetailView.as_view(),
        name="app_detail",
    ),
    # actions
    url(
        r"^staff/apps/(?P<app_pk>[0-9]+)/mark-suspicious/$",
        staff_views.mark_suspicious,
        name="mark_suspicious",
    ),
    url(
        r"^staff/apps/(?P<app_pk>[0-9]+)/demark-suspicious/$",
        staff_views.demark_suspicious,
        name="demark_suspicious",
    ),
    url(
        r"^staff/apps/(?P<app_pk>[0-9]+)/accept/$",
        staff_views.accept_app,
        name="accept_app",
    ),
    url(
        r"^staff/apps/(?P<pk>[0-9]+)/reject-a-reno/$",
        staff_views.RejectAppFromNabor.as_view(),
        name="reject_app",
    ),
    url(
        r"^staff/apps/(?P<app_pk>[0-9]+)/unreject/$",
        staff_views.unreject_app,
        name="unreject_app",
    ),
    url(
        r"^staff/apps/(?P<app_pk>[0-9]+)/case-reopen/$",
        staff_views.reopen_app_case,
        name="reopen_app_case",
    ),
    url(r"^staff/apps/return-app/$", staff_views.return_app, name="return_app"),
    url(
        r"^staff/apps/(?P<pk>[0-9]+)/comment/$",
        staff_views.AppCommentView.as_view(),
        name="comment_app",
    ),
    url(
        r"^staff/apps/(?P<pk>[0-9]+)/request-photo-change/$",
        staff_views.RequestPhotoChange.as_view(),
        name="request_photo_change",
    ),
    url(
        r"^staff/apps/(?P<pk>[0-9]+)/change-workshop/$",
        staff_views.ChangeAppWorkshop.as_view(),
        name="change_app_workshop",
    ),
    url(
        r"^staff/ask-nicely/replace-photo/$",
        views.ReplacePhoto.as_view(),
        name="replace_photo",
    ),
]

urlpatterns = first_step_urls + ajax_urls + staff_urls
