import os
from wsgiref.util import FileWrapper

from annoying.functions import get_object_or_None

from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.views import generic

from intake_platform.common.tools import get_user
from intake_platform.intake.curators.forms import CuratorAssignmentUpdateForm
from intake_platform.intake.mixins import CuratorRequiredMixin
from intake_platform.intake.models import ApplicationAssignmentResponse, ApplicationCase
from intake_platform.intake.tools import (
    prepare_second_round,
    send_mail_to_applicant,
    workshop_assignments_for_curator,
)


class CuratorHomeworkControl(CuratorRequiredMixin, generic.View):
    template_name = 'intake/curators/homework_control.html'
    form_class = CuratorAssignmentUpdateForm
    ext_whitelist = ('.doc', '.docx', '.zip', '.pdf', '.xlsx', '.xls')

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()
        w_homeworks = workshop_assignments_for_curator(user)

        form = self.form_class()
        form.fields['workshop_program'].choices = [
            (wp.pk, f'{wp.workshop_name}: {wp.program_name}') for wp in w_metas
        ]

        return render(
            request,
            self.template_name,
            {'form': form, 'w_metas': w_metas, 'w_homeworks': w_homeworks},
        )

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()
        w_homeworks = workshop_assignments_for_curator(user)

        # TODO: check that user не мудило ёбаное, и не пытается залить чужой ассайнмент сучара

        form = self.form_class(request.POST, request.FILES)
        form.fields['workshop_program'].choices = [
            (wp.pk, f'{wp.workshop_name}: {wp.program_name}') for wp in w_metas
        ]
        if form.is_valid():
            data = form.cleaned_data['assignment']
            filename = data.name
            ext = os.path.splitext(filename)[1]
            ext = ext.lower()
            if ext not in self.ext_whitelist:
                whitelist_str = ', '.join(self.ext_whitelist)
                messages.error(
                    request, f'Запрещённый тип документа! Только {whitelist_str}, никаких {ext}'
                )
            else:
                hw = form.save()
                hw.save()
                messages.success(request, f'Побйеда. Обновили {hw}')
                return redirect('curators:homework_control')

        return render(
            request,
            self.template_name,
            {'form': form, 'w_metas': w_metas, 'w_homeworks': w_homeworks},
        )


class SendHomework(CuratorRequiredMixin, generic.View):
    def get(self, request, *args, **kwargs):
        user = get_user(request)
        app = get_object_or_404(ApplicationCase, pk=self.kwargs['pk'])
        if not user.userprofile.is_curator_for_app(pk=app.pk):
            messages.error(request, f'{app} -- не твоя, вот и бесишься? Ты чё-то попутал, дружище.')
            messages.error(request, 'Может ты вообще не куратор?')
            return redirect('profile')

        if prepare_second_round(app, intention=f'set by {user.username}', automatic=False):
            # FIXME: use template
            subject = 'ЛШ2020: вступительное задание'
            text = '''Добрый день.

Вам предлагается выполнить вступительное задание для выбранной программы.

Чтобы это сделать, необходимо войти на сайт https://nabor.letnyayashkola.org
и проследовать указанным там инструкциям.

Всего доброго
Сара Коннор
'''
            send_mail_to_applicant(app, subject, text)

            messages.success(request, f'{app}: Отправлено вступительное задание')
        else:
            statement = (
                f'{app} (pk#{app.pk}): вступительное задание отправить не получилось. '
                'Или оно уже было отправлено, или что-то случилось посерьёзней.'
            )
            messages.error(request, statement)

        w_slug, p_slug = app.wp_slug_tuple
        return redirect('curators:apps_list', w_slug, p_slug)


class InspectHomework(CuratorRequiredMixin, generic.View):
    template_name = 'intake/curators/app_inspect_homework.html'

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        app = get_object_or_None(ApplicationCase, pk=self.kwargs['app_pk'])
        hw = get_object_or_None(
            ApplicationAssignmentResponse, pk=self.kwargs['ass_pk'], user=app.user
        )
        if hw is None or app is None:
            messages.error(request, 'Не могу найти такую анкету или вступительное задание')
            return redirect('profile')
        if not user.userprofile.is_curator_for_app(pk=app.pk):
            messages.error(request, f'{app} -- не твоя, вот и бесишься? Ты чё-то попутал, дружище.')
            messages.error(request, 'Может ты вообще не куратор?')
            return redirect('profile')
        if not hw.is_complete:
            messages.error(request, 'Это задание не завершено.')
            return redirect('profile')

        return render(request, self.template_name, {'application': app, 'homework': hw})


class DownloadAppHomework(CuratorRequiredMixin, generic.View):
    def get_filename(self, username, user_email_tail, w_slug, p_slug):
        return f'otvet_{username}_{user_email_tail}_2020_{w_slug}_{p_slug}'

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        app = get_object_or_None(ApplicationCase, pk=self.kwargs['app_pk'])
        hw = get_object_or_None(
            ApplicationAssignmentResponse, pk=self.kwargs['ass_pk'], user=app.user
        )
        if hw is None or app is None:
            messages.error(request, 'Не могу найти такую анкету или вступительное задание')
            return redirect('profile')
        if not user.userprofile.is_curator_for_app(pk=app.pk):
            messages.error(request, f'{app} -- не твоя, вот и бесишься? Ты чё-то попутал, дружище.')
            messages.error(request, 'Может ты вообще не куратор?')
            return redirect('profile')
        if not hw.is_complete:
            messages.error(request, 'Это задание не завершено.')
            return redirect('profile')

        ass = hw.response
        filepath = ass.path

        wpc = app.user.workshoppetitionconnection
        fname = self.get_filename(
            app.user.email.split('@')[0],
            app.user.email.split('@')[1],
            wpc.workshop_slug,
            wpc.program_slug,
        )
        ext = os.path.splitext(filepath)[1]

        wrapper = FileWrapper(open(filepath, 'rb'))

        content_type = {
            '.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            '.zip': 'application/zip',
            '.pdf': 'application/pdf',
            '.doc': 'application/msword',
        }.get(ext, 'text/plain')
        response = HttpResponse(wrapper, content_type=content_type)
        response['Content-Disposition'] = f'attachment; filename={fname}{ext}'
        response['Content-Length'] = ass.size
        response['mimetype'] = content_type

        return response
