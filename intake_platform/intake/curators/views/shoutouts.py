from intake_platform.common.tools import get_user, send_delayed_mails
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect, render
from django.template import loader
from django.views import generic

from intake_platform.intake.models import (
    WorkshopProgramMetaInfo,
    WorkshopProgramShoutOut,
)
from intake_platform.intake.mixins import CuratorRequiredMixin
from intake_platform.intake.curators.forms import ShoutOutForm
from intake_platform.intake.tools import (
    get_wp_accepted_apps,
    get_wp_declined_apps,
    get_wp_passed_apps,
)


class CuratorShoutOut(CuratorRequiredMixin, generic.View):
    template_name = 'intake/curators/shout_out.html'
    email_subject = '[ЛШ2020] Сообщение от кураторов'
    email_template_name = 'intake/curators/emails/shout_out.txt'
    shout_type = WorkshopProgramShoutOut.TO_PASSED_APPS
    form_class = ShoutOutForm

    def get_apps_qs(self, workshop_slug, program_slug):
        # FIXME: check if this is not empty?
        wp_apps = get_wp_passed_apps(workshop_slug, program_slug)
        return wp_apps

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        selected_wp = get_object_or_404(
            WorkshopProgramMetaInfo,
            workshop_slug=kwargs['w_slug'],
            program_slug=kwargs['p_slug'],
        )
        w_metas = user.workshopprogrammetainfo_set.all()

        form = self.form_class()
        form.fields['workshop_program'].choices = [
            (wp.pk, 'Участникам программы {wp.workshop_name}: {wp.program_name}')
            for wp in w_metas
        ]
        form.fields['subject'].initial = self.email_subject
        t = loader.get_template(self.email_template_name)
        form.fields['text'].initial = t.render({'user_profile': user.userprofile,
                                                'w_meta': w_metas})
        form.fields['workshop_program'].initial = selected_wp.pk

        return render(request, self.template_name, {'form': form, 'w_metas': w_metas})

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()

        form = self.form_class(request.POST)
        form.fields['workshop_program'].choices = [
            (
                wp.pk,
                'Участникам программы {wp.workshop_name}: {wp.program_name}',
            )
            for wp in w_metas
        ]
        if form.is_valid():
            shout = form.save(commit=False)
            shout.author = user
            shout.shout_type = self.shout_type
            w_s = shout.workshop_program.workshop_slug
            p_s = shout.workshop_program.program_slug
            wp_apps = self.get_apps_qs(w_s, p_s)
            emails = [a.user.email for a in wp_apps]
            shout.save()
            shout.users.set([a.user for a in wp_apps])
            send_delayed_mails(emails, shout.subject, shout.text)
            messages.success(
                request,
                f'Ушло массовое сообщение всем участникам {shout.workshop_program}',
            )
            return redirect('profile')

        return render(request, self.template_name, {'form': form, 'w_metas': w_metas})


class CuratorShoutOutWPAccepted(CuratorShoutOut):
    template_name = 'intake/curators/shout_out_wp_accepted.html'
    email_subject = '[ЛШ2020] Сообщение принятым участникам от кураторов'
    email_template_name = 'intake/curators/emails/shout_out_wp_accepted.txt'
    shout_type = WorkshopProgramShoutOut.TO_ACCEPTED_APPS

    def get_apps_qs(self, workshop_slug, program_slug):
        return get_wp_accepted_apps(workshop_slug, program_slug)


class CuratorShoutOutWPDeclined(CuratorShoutOut):
    template_name = 'intake/curators/shout_out_wp_declined.html'
    email_subject = '[ЛШ2020] Сообщение отказанным участникам от кураторов'
    email_template_name = 'intake/curators/emails/shout_out_wp_declined.txt'
    shout_type = WorkshopProgramShoutOut.TO_DECLINED_APPS

    def get_apps_qs(self, workshop_slug, program_slug):
        return get_wp_declined_apps(workshop_slug, program_slug)


class CuratorShoutOutWPSlackers(CuratorShoutOut):
    template_name = 'intake/curators/shout_out_wp_slackers.html'
    email_subject = '[ЛШ2020] Не можем найти ваше вступительное задание'
    email_template_name = 'intake/curators/emails/shout_out_wp_slackers.txt'
    shout_type = WorkshopProgramShoutOut.TO_SLACKERS_APPS

    def get_apps_qs(self, w_slug, p_slug):
        passed_apps = get_wp_passed_apps(w_slug, p_slug)
        return [app for app in passed_apps if not app.has_complete_homework()]
