import datetime
import io

from django.http import HttpResponse
from django.views import generic

from intake_platform.common.utils import UnicodeWriter
from intake_platform.intake.mixins import CuratorRequiredMixin
from intake_platform.intake.tools import get_wp_accepted_apps, get_wp_passed_apps

import openpyxl


def read_and_flush(csvfile):
    csvfile.seek(0)
    data = csvfile.read()
    csvfile.seek(0)
    csvfile.truncate()
    return data


class AcceptedAppsCSV(CuratorRequiredMixin, generic.View):
    fname_prefix = 'report_accepted_apps'

    def get_apps(self, w_slug, p_slug):
        return get_wp_accepted_apps(w_slug, p_slug)

    def get_filename(self, w_slug, p_slug):
        now_str = datetime.datetime.now().strftime('%Y%b%d').lower()
        return f'{self.fname_prefix}_{w_slug}-{p_slug}_{now_str}.csv'

    def get(self, request, *args, **kwargs):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']

        csvfile = io.StringIO()
        csvwriter = UnicodeWriter(csvfile, encoding='cp1251')

        def data():
            for app in self.get_apps(w_slug, p_slug):
                # FIXME: Wtf is going in this list comprehension?
                # Why are we casting fields to string?
                csvwriter.writerow([f'{field}' for (_, field) in app.csv_fields()])

                data = read_and_flush(csvfile)
                yield data

        response = HttpResponse(data())
        fname = self.get_filename(w_slug, p_slug)
        response['Content-Disposition'] = 'attachment; filename=%s' % fname
        response['mimetype'] = 'text/csv'
        return response


class AcceptedAppsExcel(CuratorRequiredMixin, generic.View):
    fname_prefix = 'report_accepted_apps'

    def get_apps(self, w_slug, p_slug):
        return get_wp_accepted_apps(w_slug, p_slug)

    def get_filename(self, w_slug, p_slug):
        now_str = datetime.datetime.now().strftime('%Y%b%d').lower()
        return f'{self.fname_prefix}_{w_slug}-{p_slug}_{now_str}.xlsx'

    def get(self, request, *args, **kwargs):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']

        fname = self.get_filename(w_slug, p_slug)
        response = HttpResponse()
        response['mimetype'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        response['Content-Disposition'] = 'attachment; filename=%s' % fname

        workbook = openpyxl.Workbook()
        worksheet = workbook['Sheet']

        cell_alignment = openpyxl.styles.Alignment(
            horizontal='left', vertical='bottom', wrap_text=True
        )

        for i, app in enumerate(self.get_apps(w_slug, p_slug), start=1):
            row = [val for (_, val, _) in app.xsl_fields()]
            for col_num in range(len(row)):
                cell = worksheet.cell(row=i, column=col_num + 1)
                cell.value = row[col_num]
                cell.alignment = cell_alignment

        workbook.save(response)
        return response


class PassedAppsCSV(AcceptedAppsCSV):
    fname_prefix = 'report_passed_apps'

    def get_apps(self, w_slug, p_slug):
        return get_wp_passed_apps(w_slug, p_slug)


class PassedAppsExcel(AcceptedAppsExcel):
    fname_prefix = 'report_passed_apps'

    def get_apps(self, w_slug, p_slug):
        return get_wp_passed_apps(w_slug, p_slug)
