from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect, render
from django.template import loader
from django.urls import reverse
from django.views import generic

from intake_platform.common.tools import get_user
from intake_platform.intake.curators.forms import CuratorEmailTemplateForm
from intake_platform.intake.forms import AppEmailWithCommentForm
from intake_platform.intake.mixins import CuratorRequiredMixin
from intake_platform.intake.models import ApplicationCase, WorkshopProgramEmailTemplate
from intake_platform.intake.tools import (
    attach_buck,
    comment_on_case,
    detach_buck,
    has_buck,
    has_buck_after_buck,
    send_mail_to_applicant,
)


class CuratorControlPanel(CuratorRequiredMixin, generic.TemplateView):
    template_name = 'intake/curators/cp.html'

    def get_context_data(self, **kwargs):
        context = super(CuratorControlPanel, self).get_context_data(**kwargs)

        # URL = 'https://letnyayashkola.org/api/v1.0/workshops/'
        # context['workshops'] = requests.get(URL).json()

        user = get_user(self.request)
        prof = user.userprofile
        context['profile'] = prof

        context['this_user_workshop_programs'] = prof.workshop_programs(include_apps=True)

        return context


class CuratorEmailTemplatesControl(CuratorRequiredMixin, generic.View):
    template_name = 'intake/curators/email_templates_control.html'
    form_class = CuratorEmailTemplateForm
    ext_whitelist = ('.doc', '.docx', '.zip', '.pdf')

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()
        wp_templates = WorkshopProgramEmailTemplate.all_for_wp_curator(user)

        form = self.form_class()
        form.fields['workshop_program'].choices = [
            (wp.pk, '%s: %s' % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        return render(
            request,
            self.template_name,
            {'form': form, 'w_metas': w_metas, 'wp_templates': wp_templates},
        )

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_metas = user.workshopprogrammetainfo_set.all()
        wp_templates = WorkshopProgramEmailTemplate.all_for_wp_curator(user)

        # TODO: check that user не пытается править чужой темплейт

        form = self.form_class(request.POST)
        form.fields['workshop_program'].choices = [
            (wp.pk, f'{wp.workshop_name}: {wp.program_name}') for wp in w_metas
        ]
        if form.is_valid():
            tmpl = form.save()
            tmpl.save()
            messages.success(request, 'Готово. Новый шаблон сохранён')
            return redirect('curators:email_templates_control')

        return render(
            request,
            self.template_name,
            {'form': form, 'w_metas': w_metas, 'wp_templates': wp_templates},
        )


class CuratorAcceptApp(CuratorRequiredMixin, generic.View):
    def post(self, request, *args, **kwargs):
        user = get_user(request)
        back_url = request.POST.get('back_url', 'profile')

        a_pk = request.POST.get('a_pk', None)
        if a_pk is None:
            messages.error(request, 'Заявку не удалось принять. Напишите Саре.')
            return redirect(back_url)

        app = get_object_or_404(ApplicationCase, pk=a_pk)
        w_slug, p_slug = app.wp_slug_tuple

        if user.userprofile.is_castrated:
            messages.error(request, 'У вас отключен доступ к этой операции')
            return redirect('profile')

        if not user.userprofile.is_curator_for_app(pk=a_pk):
            messages.error(request, f'Ты чё-то попутал, дружище. {app} -- не из твоих')
            messages.error(request, 'Может ты вообще не куратор?')
            return redirect('profile')

        if attach_buck(app.user, 'accepted_by_workshop'):
            messages.success(request, f'{app} ({app.pk}) теперь "принята на ЛШ".')

            subject = 'ЛШ2020: Вас берут'
            tmpl = WorkshopProgramEmailTemplate.get_after_wp_accept_for_app(app)
            text = ''
            if tmpl is not None:
                text = tmpl.render()
                if len(text) > 0:
                    attach_buck(app.user, 'email_template_wp_accepted_auto_sentt')

            # FIXME: UGH!!! Use a template here, and don't use such a weird ternary
            text = (
                text
                if len(text) > 0
                else '''Добрый день.

Поздравляю, вы приняты на ЛШ2020. Мы вас посчитали, и ждём, что вы к нам приедете.

До встречи на ЛШ.

Не забывайте время от времени заходить на https://nabor.letnyayashkola.org,
мы можем иногда запрашивать дополнительную справочную информацию.

Всякие новости можно найти на https://letnyayashkola.org
и в нашей группе вконтакте https://vk.com/shkola_rr

Всего доброго
Сара Коннор
'''
            )
            send_mail_to_applicant(app, subject, text)

            messages.success(request, 'Пользователю направлено письмо счастья.')

            comment_on_case(
                case=app,
                author=user,
                comment='[auto] Заявка принята на {}/{}.'.format(w_slug, p_slug),
            )
        else:
            messages.error(
                request,
                f'Weirdest shit happens. Анкета {app} (#{app.pk}) уже была принятой.'
            )

        return redirect(back_url)


class CuratorAppMarkSonOfABitch(CuratorRequiredMixin, generic.View):
    def post(self, request, *args, **kwargs):
        user = get_user(request)
        back_url = request.POST.get('back_url')

        a_pk = self.kwargs['app_pk']
        if a_pk is None:
            messages.error(request, 'Заявку не удалось пометить. Напишите Саре.')
            return redirect('profile')

        app = get_object_or_404(ApplicationCase, pk=a_pk)
        w_slug, p_slug = app.wp_slug_tuple

        if back_url is None:
            retval = redirect('curators:accepted_apps_list', w_slug=w_slug, p_slug=p_slug)
        else:
            retval = redirect(back_url)

        if not user.userprofile.is_curator_for_app(pk=a_pk):
            messages.error(request, f'Ты чё-то попутал, дружище. {app} -- не из твоих')
            messages.error(request, 'Может ты вообще не куратор?')
            return redirect('profile')

        if attach_buck(app.user, 'son_of_a_bitch'):
            messages.success(request, f'{app} (#{app.pk}) теперь с пометкой "Не доехал до ЛШ".')
        else:
            messages.error(
                request,
                f'Weirdest shit happens. Анкета {app} (#{app.pk}) уже была помечена недоехавшей.'
            )

        return retval


class CuratorAppDemarkSonOfABitch(CuratorRequiredMixin, generic.View):
    def get(self, request, *args, **kwargs):
        user = get_user(request)

        a_pk = self.kwargs['app_pk']
        if a_pk is None:
            messages.error(request, 'Заявку не удалось пометить. Напишите Саре.')
            return redirect('profile')

        app = get_object_or_404(ApplicationCase, pk=a_pk)
        w_slug, p_slug = app.wp_slug_tuple

        retval = redirect('curators:accepted_apps_list', w_slug=w_slug, p_slug=p_slug)

        # FIXME: too much copy-paste
        if not user.userprofile.is_curator_for_app(pk=a_pk):
            messages.error(request, f'Ты чё-то попутал, дружище. {app} -- не из твоих')
            messages.error(request, 'Может ты вообще не куратор?')
            return redirect('profile')

        if detach_buck(app.user, 'son_of_a_bitch'):
            messages.success(request, f'{app} (#{app.pk}) Метка теперь "Не доехал до ЛШ" снята.')
        else:
            messages.error(
                request,
                f'Чёт я не смог снять метку непоехавшего с анкеты {app} (#{app.pk})'
            )

        return retval


class CuratorDeclineApp(CuratorRequiredMixin, generic.View):
    template_name = 'intake/curators/app_decline.html'
    email_template = 'intake/curators/emails/app_decline.txt'
    form_class = AppEmailWithCommentForm

    def __init__(self, *args, **kwargs):
        super(CuratorDeclineApp, self).__init__(*args, **kwargs)

    def back_url(self, w_slug, p_slug):
        return reverse('curators:apps_list', kwargs={'w_slug': w_slug, 'p_slug': p_slug})

    def allowed_to_alter_this_app(self, request, app):
        prof = get_user(request).userprofile
        if prof.is_castrated:
            return False
        allowed = prof.is_curator_for_app(pk=app.pk)
        if not allowed:
            messages.error(
                request, '{a} (#{a.pk}) -- не из твоих. Ты вообще куратор?'.format(a=app)
            )

        return allowed

    def get(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs['pk'])
        if not self.allowed_to_alter_this_app(request, app):
            return redirect('profile')

        w_slug, p_slug = app.wp_slug_tuple
        w_name, p_name = app.wp_names_tuple

        if has_buck(app.user, 'declined_by_workshop'):
            if not has_buck_after_buck(app.user, 'retry_granted', 'declined_by_workshop'):
                messages.error(request, f'{app} уже помечена как отклонённая')
                return redirect(self.back_url(w_slug, p_slug))

        form = self.form_class()
        form.fields['subject'].initial = f'[ЛШ2020] Отказ от участия в программе {w_name}/{p_name}'
        email_template = loader.get_template(self.email_template)
        form.fields['text'].initial = email_template.render(
            {
                'user_profile': get_user(request).userprofile,
                'w_name': w_name,
                'p_name': p_name
            },
        )

        return render(
            request,
            self.template_name,
            {
                'form': form,
                'application': app,
                'back_urls': [(self.back_url(w_slug, p_slug), 'Назад')],
            },
        )

    def post(self, request, *args, **kwargs):
        app = get_object_or_404(ApplicationCase, pk=self.kwargs['pk'])
        if not self.allowed_to_alter_this_app(request, app):
            return redirect('profile')

        w_slug, p_slug = app.wp_slug_tuple

        form = self.form_class(request.POST)
        if form.is_valid():
            buck = attach_buck(app.user, 'declined_by_workshop', get_buck=True)
            if buck is not None:
                messages.success(
                    request, f'{app} (#{app.pk}) теперь имеет отметку "отклонена мастерской"',
                )

                mejl = form.save(commit=False)
                mejl.author = get_user(request)
                mejl.to_user = app.user
                mejl.relevant_buck = buck
                mejl.save()

                if send_mail_to_applicant(app, mejl.subject, mejl.text):
                    messages.success(request, 'Пользователь уведомлён.')
                else:
                    messages.error(
                        request, 'Не получилось отправить пользователю письмо, проблемы с сервером.'
                    )

                messages.info(
                    request,
                    'В таймлайне пользователя появится информация, дублирующая отправленное письмо.',
                )

                comment_on_case(
                    case=app,
                    author=mejl.author,
                    comment=f'[auto] Заявку не хотят на {w_slug}/{p_slug}.',
                )
                comment = form.cleaned_data.get('comment')
                if len(comment) > 0:
                    comment_on_case(case=app, author=mejl.author, comment=comment)
            else:
                messages.error(request, 'Не получилось отказать этой заявке. Попробуй ещё раз.')

            return redirect('curators:apps_list', w_slug, p_slug)

        return render(
            request,
            self.template_name,
            {
                'form': form,
                'application': app,
                'back_urls': [(self.back_url(w_slug, p_slug), 'Назад')],
            },
        )


class CuratorDeclineAppBecauseSlow(CuratorDeclineApp):
    email_template = 'intake/curators/emails/app_decline_slow.txt'
