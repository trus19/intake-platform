from django.shortcuts import get_object_or_404, redirect
from django.views import generic

from intake_platform.common.tools import get_user
from intake_platform.intake.mixins import CuratorRequiredMixin
from intake_platform.intake.models import (
    WorkshopProgramAssignment,
    WorkshopProgramMetaInfo,
)
from intake_platform.intake.tools import (
    get_wp_accepted_apps,
    get_wp_counts_dict,
    get_wp_declined_apps,
    get_wp_passed_apps,
    get_wp_retry_apps,
)
import operator


class CuratorWPAppsList(CuratorRequiredMixin, generic.ListView):
    template_name = 'intake/curators/apps_list.html'
    context_object_name = 'applications'
    paginate_by = 10

    def apps_qs(self):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']
        return get_wp_passed_apps(w_slug, p_slug, order_by_created=False)

    def get_queryset(self):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect('profile')

        qs = self.apps_qs()

        return sorted(qs, key=operator.attrgetter('dt_when_passed_nabor'), reverse=True)

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(CuratorWPAppsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context['profile'] = prof
        context['workshop_name'] = w_meta.workshop_name
        context['program_name'] = w_meta.program_name
        context['workshop_slug'] = w_meta.workshop_slug
        context['program_slug'] = w_meta.program_slug
        context['wp_has_assignment'] = WorkshopProgramAssignment.objects.filter(
            workshop_program=w_meta
        ).exists()
        context['wp_counts'] = get_wp_counts_dict(w_slug, p_slug)

        return context


class CuratorWPAppsListHideSlackers(CuratorWPAppsList):
    template_name = 'intake/curators/apps_list_hide_slackers.html'

    def apps_qs(self):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']
        qs = get_wp_passed_apps(w_slug, p_slug, order_by_created=False)
        nu_apps = [app for app in qs if app.has_complete_homework()]
        return nu_apps


class CuratorAppsListTables(CuratorRequiredMixin, generic.ListView):
    template_name = 'intake/curators/apps_list_tables.html'
    context_object_name = 'applications'

    def get_queryset(self):
        u = get_user(self.request)
        from intake_platform.intake import tools as int_too

        qs = []
        for v in u.userprofile.workshop_programs().values():
            w_slug, p_slug = v['workshop_slug'], v['program_slug']
            if not u.userprofile.is_curator_for(w_slug, p_slug):
                return redirect('profile')

            qs += list(int_too._get_wp_apps_filtered(w_slug, p_slug, int_too._passed))

        return sorted(qs, key=operator.attrgetter('dt_when_passed_nabor'), reverse=True)


class RetryAppsList(CuratorRequiredMixin, generic.ListView):
    template_name = 'intake/curators/retry_apps_list.html'
    context_object_name = 'applications'
    paginate_by = 10

    def apps_qs(self):
        # FIXME: FINISH ME
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']
        return get_wp_retry_apps(w_slug, p_slug)

    def get_queryset(self):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect('profile')

        qs = self.apps_qs()

        return qs

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(RetryAppsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context['profile'] = prof
        context['workshop_name'] = w_meta.workshop_name
        context['program_name'] = w_meta.program_name
        context['workshop_slug'] = w_meta.workshop_slug
        context['program_slug'] = w_meta.program_slug
        context['wp_has_assignment'] = WorkshopProgramAssignment.objects.filter(
            workshop_program=w_meta
        ).exists()
        context['wp_counts'] = get_wp_counts_dict(w_slug, p_slug)

        return context


class AcceptedAppsList(CuratorWPAppsList):
    template_name = 'intake/curators/accepted_apps_list.html'

    def apps_qs(self):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']
        return get_wp_accepted_apps(w_slug, p_slug)


class DeclinedAppsList(CuratorWPAppsList):
    template_name = 'intake/curators/declined_apps_list.html'

    def apps_qs(self):
        w_slug = self.kwargs['w_slug']
        p_slug = self.kwargs['p_slug']
        return get_wp_declined_apps(w_slug, p_slug)
