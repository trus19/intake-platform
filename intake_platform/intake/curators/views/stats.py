from django.shortcuts import render
from django.views import generic

from intake_platform.common.tools import get_user
from intake_platform.intake.mixins import CuratorRequiredMixin
from intake_platform.intake.tools import get_wp_accepted_apps, get_wp_counts_dict


def get_wp_info_tuple(wp_metainfo):
    w_slug = wp_metainfo.workshop_slug
    p_slug = wp_metainfo.program_slug
    counts = get_wp_counts_dict(w_slug, p_slug)
    apps = get_wp_accepted_apps(w_slug, p_slug)
    return (
        wp_metainfo.workshop_slug,
        wp_metainfo.program_slug,
        wp_metainfo.workshop_name,
        wp_metainfo.program_name,
        counts,
        apps,
    )


class Stats(CuratorRequiredMixin, generic.View):
    template_name = 'intake/curators/stats.html'

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        context = {
          'workshop_programs': [
            get_wp_info_tuple(wp) for wp in user.workshopprogrammetainfo_set.all()
          ]
        }
        return render(request, self.template_name, context)
