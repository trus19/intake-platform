from django.contrib import messages
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect
from django.views import generic

from intake_platform.common.tools import get_user
from intake_platform.intake.mixins import CuratorRequiredMixin
from intake_platform.intake.models import ApplicationCase, WorkshopPetitionConnection


class CuratorAppDetails(CuratorRequiredMixin, generic.DetailView):
    model = ApplicationCase
    template_name = 'intake/curators/app_details.html'

    def get_context_data(self, **kwargs):
        context = super(CuratorAppDetails, self).get_context_data(**kwargs)

        user = get_user(self.request)
        prof = user.userprofile
        if not prof.is_curator_for_app(pk=self.kwargs['pk']):
            return redirect('profile')

        context['profile'] = prof
        return context


class CuratorAppSearch(CuratorRequiredMixin, generic.ListView):
    template_name = 'intake/curators/app_search.html'
    context_object_name = 'applications'

    def get_queryset(self):
        user = get_user(self.request)
        accessible_users = []
        for wp in user.workshopprogrammetainfo_set.all():
            accessible_users += [
                p.user
                for p in WorkshopPetitionConnection.objects.filter(
                    workshop_slug=wp.workshop_slug, program_slug=wp.program_slug
                )
            ]
        my_applicants = ApplicationCase.objects.filter(user__in=accessible_users, is_closed=True)

        if self.request.GET.get('search_1', False):
            s1 = self.request.GET.get('search_1')
            return my_applicants.filter(pk=s1)

        substr = self.request.GET.get('search_42', '').split()
        if len(substr) == 3:
            query = Q(user__email__icontains=substr[-1][1:-1])
            query &= Q(user__last_name__icontains=substr[1])
            query &= Q(user__first_name__icontains=substr[0])
            queryset = my_applicants.filter(query).order_by('created')[:10]
        else:
            substr = self.request.GET.get('search_42', '')
            if substr != '':
                if substr.find('@') != -1:
                    query = Q(user__email__icontains=substr)
                else:
                    query = Q(user__last_name__icontains=substr)
                    query |= Q(user__first_name__icontains=substr)
                queryset = my_applicants.filter(query).order_by('created')[:10]
            else:
                queryset = []
        return queryset


class AppCuratorCommentView(CuratorRequiredMixin, generic.View):
    def post(self, request, *args, **kwargs):
        user = get_user(request)
        back_url = request.POST.get('back_url', 'profile')

        a_pk = request.POST.get('a_pk')
        if a_pk is None:
            messages.error(request, 'Заявку не удалось принять. Напишите Саре.')
            return redirect(back_url)

        app = get_object_or_404(ApplicationCase, pk=a_pk)
        w_slug, p_slug = app.wp_slug_tuple

        if not user.userprofile.is_curator_for_app(pk=a_pk):
            messages.error(request, f'Ты чё-то попутал, дружище. {app} -- не из твоих')
            messages.error(request, 'Может ты вообще не куратор?')
            return redirect(back_url)

        app.short_comment = request.POST.get('short_comment_hidden', '')
        app.save()

        return redirect(back_url)
