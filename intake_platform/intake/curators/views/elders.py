"""
Unused logic regarding curators adding other curators and prepods
"""
import json

from annoying.functions import get_config, get_object_or_None
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.shortcuts import get_object_or_404, redirect, render
from django.template import loader
from django.urls import reverse
from django.views import generic

from intake_platform.common.models import UserProfile
from intake_platform.common.tools import get_user
from intake_platform.intake.curators.forms import AddCuratorForm, AddPrepodForm
from intake_platform.intake.mixins import CuratorRequiredMixin
from intake_platform.intake.models import (
    ApplicationCase,
    UserAdditionalContacts,
    UserCuratorInfo,
    UserPhoto,
    UserPrepodInfo,
    WorkshopProgramAssignment,
    WorkshopProgramMetaInfo,
)
from intake_platform.intake.tools import (
    generate_random_password,
    get_wp_counts_dict,
    get_wp_curators,
    get_wp_prepods,
)


class EldersCuratorsList(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/elders/curators_list.html"
    context_object_name = "curators"
    paginate_by = 10

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        print(w_slug)
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect("profile")

        qs = get_wp_prepods(w_slug, p_slug)

        return qs

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(EldersCuratorsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context["profile"] = prof
        context["workshop_name"] = w_meta.workshop_name
        context["program_name"] = w_meta.program_name
        context["workshop_slug"] = w_meta.workshop_slug
        context["program_slug"] = w_meta.program_slug
        context["wp_has_assignment"] = WorkshopProgramAssignment.objects.filter(
            workshop_program=w_meta
        ).exists()
        context["wp_counts"] = get_wp_counts_dict(w_slug, p_slug)

        # UGLY
        context["a"] = ApplicationCase.objects.get(pk=20)

        return context


class CuratorPrepods(CuratorRequiredMixin, generic.View):
    """ FIXME: Not sure why I need this one """

    template_name = "intake/curators/apps_list.html"
    context_object_name = "applications"
    paginate_by = 10


class CuratorPrepodAdd(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/elders/prepod_add.html"
    form_class = AddPrepodForm

    def back_url(self, w_slug, p_slug):
        return reverse("curators:prepods_list", kwargs={"w_slug": w_slug, "p_slug": p_slug})

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        w_slug, p_slug = kwargs["w_slug"], kwargs["p_slug"]

        w_metas = user.workshopprogrammetainfo_set.all()

        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так")
            return redirect("profile")
        form = self.form_class()
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        form.fields["workshop_program"].initial = w_metas.get(
            workshop_slug=w_slug, program_slug=p_slug
        )

        return render(
            request,
            self.template_name,
            {
                "form": form,
                "workshop_slug": w_slug,
                "program_slug": p_slug,
                "back_urls": [(self.back_url(w_slug, p_slug), u"Назад к списку преподов")],
            },
        )

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_slug, p_slug = kwargs["w_slug"], kwargs["p_slug"]

        w_metas = user.workshopprogrammetainfo_set.all()
        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так")
            return redirect("profile")

        context = {}
        context["workshop_slug"] = w_slug
        context["program_slug"] = p_slug

        context["back_urls"] = [(self.back_url(w_slug, p_slug), u"Назад к списку преподов")]

        form = self.form_class(request.POST, request.FILES)
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        if "lectures" in form.data:
            context["lectures_data"] = json.loads(form.data["lectures"])

        if form.is_valid():
            messages.error(request, u"Неподдерживаемый функционал.")
        else:
            messages.error(request, u"Форма заполнена неполностью и/или с ошибками")

        context["form"] = form
        return render(request, self.template_name, context)


class CuratorPrepodDelete(CuratorRequiredMixin, generic.View):
    def back_url(self, w_slug, p_slug):
        return reverse("curators:prepods_list", kwargs={"w_slug": w_slug, "p_slug": p_slug})

    def post(self, request, *args, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        user = get_user(self.request)

        back_url = self.back_url(w_slug, p_slug)
        success_url = back_url

        w_metas = user.workshopprogrammetainfo_set.all()
        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так")
            return redirect(back_url)

        prep = get_object_or_None(
            UserPrepodInfo,
            pk=self.kwargs["prepod_pk"],
            workshop_program__workshop_slug=w_slug,
            workshop_program__program_slug=p_slug,
        )

        if prep is None:
            messages.error(
                request, u"? Или у вас не хватает прав для этого действия, или такого препода нет"
            )
            return redirect(back_url)

        prep.user.delete()
        messages.success(request, u"Препод удалён")

        return redirect(success_url)


class CuratorPrepodsList(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/elders/prepods_list.html"
    context_object_name = "prepods"
    paginate_by = 10

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect("profile")

        qs = get_wp_prepods(w_slug, p_slug)

        return qs

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(CuratorPrepodsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context["profile"] = prof
        context["workshop_name"] = w_meta.workshop_name
        context["program_name"] = w_meta.program_name
        context["workshop_slug"] = w_meta.workshop_slug
        context["program_slug"] = w_meta.program_slug
        context["wp_counts"] = get_wp_counts_dict(w_slug, p_slug)

        return context


class CuratorCuratorAdd(CuratorRequiredMixin, generic.View):
    template_name = "intake/curators/elders/curator_add.html"
    form_class = AddCuratorForm

    def back_url(self, w_slug, p_slug):
        return reverse("curators:curators_list", kwargs={"w_slug": w_slug, "p_slug": p_slug})

    def get(self, request, *args, **kwargs):
        user = get_user(request)
        w_slug, p_slug = kwargs["w_slug"], kwargs["p_slug"]
        if not user.userprofile.is_beta_tester:
            messages.error(request, u"Эта функция недоступна непродвинутым пользователям.")
            return redirect("profile")

        w_metas = user.workshopprogrammetainfo_set.all()

        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так")
            return redirect("profile")
        form = self.form_class()
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        form.fields["workshop_program"].initial = w_metas.get(
            workshop_slug=w_slug, program_slug=p_slug
        )

        context = {}
        context["form"] = form
        context["workshop_slug"] = w_slug
        context["program_slug"] = p_slug
        context["back_urls"] = [(self.back_url(w_slug, p_slug), u"Назад к списку кураторов")]

        me_curator = get_object_or_None(UserCuratorInfo, user=user)
        if me_curator is None:
            data_about_me = {
                "first_name": user.first_name,
                "last_name": user.last_name,
                "email": user.email,
            }
            uac = get_object_or_None(UserAdditionalContacts, user=user)
            if uac is not None:
                data_about_me["vk"] = uac.vk
                data_about_me["phone"] = uac.phone
            up = get_object_or_None(UserProfile, user=user)
            if up is not None:
                data_about_me["middle_name"] = up.middle_name
            context["me"] = data_about_me
        context["has_me"] = me_curator is not None

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        user = get_user(request)
        w_slug, p_slug = kwargs["w_slug"], kwargs["p_slug"]

        w_metas = user.workshopprogrammetainfo_set.all()
        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так")
            return redirect("profile")

        context = {}
        context["workshop_slug"] = w_slug
        context["program_slug"] = p_slug

        context["back_urls"] = [(self.back_url(w_slug, p_slug), u"Назад к списку кураторов")]

        form = self.form_class(request.POST, request.FILES)
        form.fields["workshop_program"].choices = [
            (wp.pk, u"%s: %s" % (wp.workshop_name, wp.program_name)) for wp in w_metas
        ]

        if "lectures" in form.data:
            context["lectures_data"] = json.loads(form.data["lectures"])

        if form.is_valid():
            cd = form.cleaned_data

            if cd["email"] != user.email:
                # FIXME: This part is not working, drop it
                # adapter = get_adapter()
                # u = adapter.new_user(None)
                # adapter.save_user(request, u, form, commit=True)

                User = get_user_model()
                u = User()
                password = generate_random_password()
                u.set_password()
                u.save()

                # em = EmailAddress(
                #    user=u, email=cd["email"], primary=True, verified=True
                # )
                # em.save()

                profile = get_object_or_None(UserProfile, user=u)
                if profile is None:
                    # FIXME is_male=False... Should opt to nullablefield in model
                    profile = UserProfile(user=u, is_male=False)
                    profile.middle_name = cd["middle_name"]
                    profile.is_curator = cd["apps_access"]
                    profile.save()

                    if cd["apps_access"]:
                        u.workshopprogrammetainfo_set.add(cd["workshop_program"])

                        subject = u"ЛШ2020: Добро пожаловать, ты куратор"
                        template = loader.get_template("intake/curators/emails/welcome_curator.txt")
                        text = template.render(
                            {
                                "w_slug": w_slug,
                                "p_slug": p_slug,
                                "email": cd["email"],
                                "password": password,
                            }
                        )

                        from_email = get_config("DEFAULT_FROM_EMAIL", "no-reply@letnyayashkola.ru")
                        if send_mail(subject, text, from_email, [cd["email"]]) != 1:
                            messages.error(
                                request,
                                'Приветственный емейл не отправился. Свяжись с куратором сам',
                            )
                        else:
                            messages.success(request, u"Приветственный емейл отправлен!")

                else:
                    messages.error(request, u"Не могу создать профиль. Cry/Try again")

                uac = get_object_or_None(UserAdditionalContacts, user=u)
                if uac is None:
                    uac = UserAdditionalContacts(user=u, phone=cd["phone"], vk=cd["vk"])
                    uac.save()
                else:
                    messages.error(request, u"Не могу сохранить контакты. Cry/Try again")
            else:
                u = user

            ph = cd.get("photo")
            if ph:
                uf, _ = UserPhoto.objects.get_or_create(user=u)
                uf.photo = ph
                uf.save()

            curator = form.save(commit=False)
            curator.user = u
            curator.save()

            messages.success(request, u"Добавлен куратор '%s'." % curator)
            return redirect("curators:curators_list", w_slug=w_slug, p_slug=p_slug)
        else:
            messages.error(request, u"Форма заполнена неполностью и/или с ошибками")

        context["form"] = form
        return render(request, self.template_name, context)


class CuratorCuratorDelete(CuratorRequiredMixin, generic.View):
    def back_url(self, w_slug, p_slug):
        return reverse("curators:curators_list", kwargs={"w_slug": w_slug, "p_slug": p_slug})

    def post(self, request, *args, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        user = get_user(self.request)

        back_url = self.back_url(w_slug, p_slug)
        success_url = back_url

        w_metas = user.workshopprogrammetainfo_set.all()
        if not w_metas.filter(workshop_slug=w_slug, program_slug=p_slug).exists():
            messages.error(request, u"? Вы пытаетесь сделать что-то нехорошое. Не надо так")
            return redirect(back_url)

        curator = get_object_or_None(
            UserCuratorInfo,
            pk=self.kwargs["curator_pk"],
            workshop_program__workshop_slug=w_slug,
            workshop_program__program_slug=p_slug,
        )

        if curator is None:
            messages.error(
                request, u"? Или у вас не хватает прав для этого действия, или такого препода нет"
            )
            return redirect(back_url)

        if user != curator.user:
            curator.user.delete()
        else:
            curator.delete()
        messages.success(request, u"Куратор удалён")

        return redirect(success_url)


class CuratorCuratorsList(CuratorRequiredMixin, generic.ListView):
    template_name = "intake/curators/elders/curators_list.html"
    context_object_name = "curators"
    paginate_by = 10

    def get_queryset(self):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        u = get_user(self.request)

        if not u.userprofile.is_curator_for(w_slug, p_slug):
            return redirect("profile")

        qs = get_wp_curators(w_slug, p_slug)

        return qs

    def get_context_data(self, **kwargs):
        w_slug = self.kwargs["w_slug"]
        p_slug = self.kwargs["p_slug"]
        w_meta = get_object_or_404(
            WorkshopProgramMetaInfo, workshop_slug=w_slug, program_slug=p_slug
        )

        context = super(CuratorCuratorsList, self).get_context_data(**kwargs)

        u = get_user(self.request)
        prof = u.userprofile
        context["profile"] = prof
        context["workshop_name"] = w_meta.workshop_name
        context["program_name"] = w_meta.program_name
        context["workshop_slug"] = w_meta.workshop_slug
        context["program_slug"] = w_meta.program_slug
        context["wp_counts"] = get_wp_counts_dict(w_slug, p_slug)

        return context
