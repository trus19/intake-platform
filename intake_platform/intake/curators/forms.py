# from bootstrap3_datetime.widgets import DateTimePicker
from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Field, Fieldset, Layout, Submit
from django import forms
from django.core.exceptions import ValidationError

from ..models import (
    UserCuratorInfo,
    UserPrepodInfo,
    WorkshopProgramAssignment,
    WorkshopProgramEmailTemplate,
    WorkshopProgramShoutOut,
)


class CuratorAssignmentUpdateForm(forms.ModelForm):
    class Meta:
        model = WorkshopProgramAssignment
        fields = ("assignment", "workshop_program")
        widgets = {"workshop_program": forms.RadioSelect}

    def __init__(self, *args, **kwargs):
        super(CuratorAssignmentUpdateForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("workshop_program"),
            Field("assignment"),
            FormActions(Submit("submit", u"Отправить", css_class="btn btn-primary")),
        )
        self.helper = helper


class CuratorEmailTemplateForm(forms.ModelForm):
    class Meta:
        model = WorkshopProgramEmailTemplate
        fields = ("text", "email_trigger_type", "workshop_program")
        widgets = {"workshop_program": forms.RadioSelect}

    def __init__(self, *args, **kwargs):
        super(CuratorEmailTemplateForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("workshop_program"),
            HTML(
                u"Начало письма &mdash; подбирается из базы в зависимости от триггера"
            ),
            Field("text"),
            HTML(u"Хвост письма почти всегда одинаков"),
            Field("email_trigger_type"),
            HTML(
                u"<i>короче да, эта вот штука когда будет меняться будет показываться ещё пример шаблона сразу</i>"
            ),
            FormActions(
                Submit("submit", u"Создать такой шаблон", css_class="btn btn-primary")
            ),
        )
        self.helper = helper


class ShoutOutForm(forms.ModelForm):
    class Meta:
        model = WorkshopProgramShoutOut
        fields = ("workshop_program", "subject", "text")
        widgets = {"workshop_program": forms.RadioSelect}

    def __init__(self, *args, **kwargs):
        super(ShoutOutForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("workshop_program"),
            Field("subject"),
            Field("text"),
            FormActions(Submit("submit", u"Отправить", css_class="btn btn-primary")),
        )
        self.helper = helper


class AddPrepodForm(forms.ModelForm):
    first_name = forms.CharField(label=u"Имя")
    middle_name = forms.CharField(label=u"Отчество")
    last_name = forms.CharField(label=u"Фамилия")
    email = forms.EmailField(label=u"Контактный e-mail", help_text=u"основной")
    phone = forms.CharField(label=u"Телефон", help_text=u"действующий")
    vk = forms.CharField(label=u"Вконтактик", required=False, help_text=u"опционально")
    education = forms.CharField(
        label=u"Образование", widget=forms.Textarea, required=False
    )
    academic_status = forms.CharField(
        label=u"Академический статус", widget=forms.Textarea, required=False
    )
    affiliation = forms.CharField(
        label=u"Место работы", widget=forms.Textarea, required=False
    )
    photo = forms.ImageField(label=u"Фотка")

    def clean_photo(self):
        photo = self.cleaned_data.get("photo", False)

        if photo:
            if photo._size > 2 * 1024 * 1024:
                raise ValidationError(u"Картинка большая (> 2mb).")
        else:
            raise ValidationError(u"Не смог прочитать загруженную картинку")

        return photo

    class Meta:
        model = UserPrepodInfo
        fields = [
            "housing",
            "comment",
            "workshop_program",
            "regalia",
            "topic",
            "lectures",
            "arrival_date",
            "departure_date",
        ]
        widgets = {
            "workshop_program": forms.RadioSelect,
            "status": forms.RadioSelect,
            "housing": forms.RadioSelect,
            "lectures": forms.HiddenInput,  # JSON
            "regalia": forms.HiddenInput,  # JSON
            # 'arrival_date': DateTimePicker(
            #     options={
            #         'format': "YYYY-MM-DD",
            #         'startDate': "2019-07-01",
            #         'endDate': "2019-08-11",
            #         'pickTime': False}
            # ),
            # 'departure_date': DateTimePicker(
            #     options={
            #         'format': "YYYY-MM-DD",
            #         'startDate': "2019-07-09",
            #         'endDate': "2019-08-11",
            #         'pickTime': False}
            # )
        }

    def __init__(self, *args, **kwargs):
        super(AddPrepodForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Fieldset(
                u"Кто, где, когда",
                Div(
                    Div("last_name", css_class="col-md-4"),
                    Div("first_name", css_class="col-md-4"),
                    Div("middle_name", css_class="col-md-4"),
                    css_class="row",
                ),
                Div(
                    Div("arrival_date", css_class="col-md-6"),
                    Div("departure_date", css_class="col-md-6"),
                    css_class="row",
                ),
                Div(
                    Div("photo", css_class="col-md-4"),
                    Div("housing", css_class="col-md-8"),
                    css_class="row",
                ),
                "comment",
            ),
            Fieldset(
                u"Контакты",
                Div(
                    Div("email", css_class="col-md-4"),
                    Div("phone", css_class="col-md-4"),
                    Div("vk", css_class="col-md-4"),
                    css_class="row",
                ),
            ),
            Fieldset(
                u"Полезная нагрузка",
                Div("education", css_class="col-md-4"),
                Div("academic_status", css_class="col-md-4"),
                Div("affiliation", css_class="col-md-4"),
                Field("topic"),
                Field("lectures", data_bind="value: ko.toJSON(lectures)"),
                Div(
                    HTML(
                        u"""
<h4>Лекции с предварительными датами
<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal">
  Добавить
</button>
</h4>
<ul data-bind="foreach: lectures">
    <li>
        &laquo;<span data-bind="text: title"></span>&raquo; (<span data-bind="text: date"></span>)
        <a class="btn btn-xs" href="#" data-bind="click: $root.removeLecture">удалить</a>
    </li></ul>"""
                    )
                ),
            ),
            HTML(u"<h3>Закреплён за мастерской/программой</h3>"),
            HTML(
                u"<p>Тут может быть скорее не то, где он(а) читает лекции или следит за участниками, а скорее, кто встречает и кому сдают деньги.</p>"
            ),
            "workshop_program",
            FormActions(
                Submit("submit", u"Добавляем", css_class="btn btn-primary"),
                HTML(
                    u"""
                     {% for url, caption in back_urls %}
                     <a class="btn btn-default" href="{{ url }}">{{ caption }}</a>
                     {% endfor %}
                     """
                ),
            ),
        )
        self.helper = helper


class AddCuratorForm(forms.ModelForm):
    first_name = forms.CharField(label=u"Имя")
    middle_name = forms.CharField(label=u"Отчество")
    last_name = forms.CharField(label=u"Фамилия")
    email = forms.EmailField(label=u"Контактный e-mail", help_text=u"основной")
    phone = forms.CharField(label=u"Телефон", help_text=u"действующий")
    vk = forms.CharField(label=u"Вконтактик", required=False, help_text=u"опционально")
    photo = forms.ImageField(label=u"Фотка")
    apps_access = forms.BooleanField(
        label=u"А кстати дать ему такой же доступ к анкетам, как у тебя? (нажми галочку чтобы 'да')",
        required=False,
    )  # Allow for "Non, merci" response

    def clean_photo(self):
        photo = self.cleaned_data.get("photo", False)

        if photo:
            if photo._size > 2 * 1024 * 1024:
                raise ValidationError(u"Картинка большая (> 2mb).")
        else:
            raise ValidationError(u"Не смог прочитать загруженную картинку")

        return photo

    class Meta:
        model = UserCuratorInfo
        fields = [
            "housing",
            "comment",
            "workshop_program",
            "arrival_date",
            "departure_date",
        ]
        widgets = {
            "workshop_program": forms.RadioSelect,
            "housing": forms.RadioSelect,
            # 'arrival_date': DateTimePicker(
            #     options={
            #         'format': "YYYY-MM-DD",
            #         'startDate': "2019-07-01",
            #         'endDate': "2019-08-11",
            #         'pickTime': False}
            # ),
            # 'departure_date': DateTimePicker(
            #     options={
            #         'format': "YYYY-MM-DD",
            #         'startDate': "2019-07-01",
            #         'endDate': "2019-08-11",
            #         'pickTime': False}
            # )
        }

    def __init__(self, *args, **kwargs):
        super(AddCuratorForm, self).__init__(*args, **kwargs)

        def f(field):
            return Field(field, data_bind="value: my_%s" % field)

        helper = FormHelper(self)
        helper.layout = Layout(
            Fieldset(
                u"Кто, где, когда",
                Div(
                    Div(f("last_name"), css_class="col-md-4"),
                    Div(f("first_name"), css_class="col-md-4"),
                    Div(f("middle_name"), css_class="col-md-4"),
                    css_class="row",
                ),
                Div(
                    Div("arrival_date", css_class="col-md-6"),
                    Div("departure_date", css_class="col-md-6"),
                    css_class="row",
                ),
                Div(
                    Div("photo", css_class="col-md-4"),
                    Div("housing", css_class="col-md-8"),
                    css_class="row",
                ),
                "comment",
            ),
            Fieldset(
                u"Контакты",
                Div(
                    Div(f("email"), css_class="col-md-4"),
                    Div(f("phone"), css_class="col-md-4"),
                    Div(f("vk"), css_class="col-md-4"),
                    css_class="row",
                ),
                "apps_access",
            ),
            HTML(u"<h3>Закреплён за мастерской/программой</h3>"),
            HTML(
                u"<p>Тут может быть скорее не то, где он(а) читает лекции или следит за участниками, а скорее, кто встречает и кому сдают деньги.</p>"
            ),
            "workshop_program",
            FormActions(
                Submit("submit", u"Добавляем", css_class="btn btn-primary"),
                HTML(
                    u"""
                     {% for url, caption in back_urls %}
                     <a class="btn btn-default" href="{{ url }}">{{ caption }}</a>
                     {% endfor %}
                     """
                ),
            ),
        )
        self.helper = helper
