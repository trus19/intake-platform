from django.conf.urls import url

from . import views

urlpatterns = [
    url(r"^guru/check_user/$", views.CheckUser.as_view(), name="check_user"),
    url(r"^guru/revert_user/$", views.RevertUser.as_view(), name="revert_user"),
    url(r"^guru/curators/$", views.CuratorsList.as_view(), name="curators_list"),
    url(r"^guru/curators/add/$", views.AddCurator.as_view(), name="add_curator"),
    url(
        r"^guru/assignments/$",
        views.WPAssignmentsList.as_view(),
        name="assignments_list",
    ),
    url(r"^guru/bucks/$", views.BucksStats.as_view(), name="bucks_stats"),
    url(r"^guru/handymanship/$", views.Handymanship.as_view(), name="handymanship"),
    url(r"^guru/shkololo/$", views.Shkololo.as_view(), name="shkololo"),
    url(r"^guru/upload_blacklist/$", views.UploadBlackList.as_view(), name="upload_blacklist"),
    url(r"^guru/spam_list/$", views.SpamList.as_view(), name="spam_list"),
]

# vim: set ts=4 sw=4 sts=4 et :
