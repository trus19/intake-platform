# coding: utf-8

from django.http import HttpResponseRedirect
from django.views import generic
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib import messages
from django.urls import reverse
from django.shortcuts import redirect, render
from annoying.functions import get_object_or_None

from intake_platform.intake.tools import (
    first_step_completed,
    workshops_dict,
    programs_dict,
    has_buck,
)
from intake_platform.intake.models import (
    WorkshopPetitionConnection,
    UserAdditionalContacts,
    UserEducationInfo,
    UserActivityInfo,
    UserHandymanshipInfo,
    GiveBackInfo,
    UserLogisticsInfo,
    PastFuturePresentInfo,
    UserPhoto,
    UnderageUserInfo,
    WorkshopProgramAssignment,
    UserBuck,
    ApplicationCase,
)

from intake_platform.intake import black_list_helpers
from intake_platform.common.models import UserProfile
from intake_platform.common.tools import get_user, get_start_date
from ..mixins import GuruRequiredMixin

from .forms import WPSelectMultipleForm, BlackListUploadForm


class CheckUser(GuruRequiredMixin, generic.TemplateView):
    template_name = "intake/guru/check_user.html"

    def get_context_data(self, **kwargs):
        context = super(CheckUser, self).get_context_data(**kwargs)

        user = get_user(self.request)
        profile = user.userprofile
        context["profile"] = profile

        email = self.request.GET.get("email")
        context["user_email"] = email or ""

        if email is None:
            return context

        context_users = []
        users = User.objects.filter(email__icontains=email)[:10]
        related_cls = {
            "WorkshopPetitionConnection": WorkshopPetitionConnection,
            "UserAdditionalContacts": UserAdditionalContacts,
            "UserEducationInfo": UserEducationInfo,
            "UserActivityInfo": UserActivityInfo,
            "UserHandymanshipInfo": UserHandymanshipInfo,
            "GiveBackInfo": GiveBackInfo,
            "UserLogisticsInfo": UserLogisticsInfo,
            "PastFuturePresentInfo": PastFuturePresentInfo,
            "UserPhoto": UserPhoto,
            "ApplicationCase": ApplicationCase,
        }

        for u in users:
            u.data = {"first_step_fields": [], "user_opts": u._meta}

            for name in related_cls:
                c = related_cls[name]
                obj = get_object_or_None(c, user=u)
                if obj is not None:
                    u.data["first_step_fields"] += [(name, True, obj.pk, obj._meta)]
                else:
                    u.data["first_step_fields"] += [(name, False, False, False)]
            prof = get_object_or_None(UserProfile, user=u)
            if (prof is not None) and prof.is_underage:
                obj = get_object_or_None(UnderageUserInfo, user=u)
                u.data["first_step_fields"] += [
                    (
                        "UnderageUserInfo",
                        obj is not None,
                        obj.pk if obj is not None else -1,
                        obj._meta if obj is not None else -1,
                    )
                ]
            u.first_step_completed = first_step_completed(u.userprofile)
            u.data["bucks_list"] = [
                {"slug": b.slug, "when": b.when, "pk": b.pk, "opts": b._meta}
                for b in UserBuck.objects.filter(user=u)
            ]

            photo = get_object_or_None(UserPhoto, user=u)
            if photo is not None:
                u.data['photo'] = {
                    'url': photo.photo_absolute_url,
                    'needs_to_be_changed': photo.needs_to_be_changed
                }

            context_users.append(u)
        context["users"] = context_users

        return context


class RevertUser(GuruRequiredMixin, generic.View):
    def get(self, request, *args, **kwargs):
        email = self.request.GET.get('email')
        back_to_user = redirect('guru:check_user')

        if email is None:
            messages.error(request, 'Телепаты в отпуске. Введи емейл')
            return back_to_user

        user = User.objects.get(email__icontains=email)
        app = ApplicationCase.objects.get(user=user)
        app.is_closed = False
        app.save()

        for b in UserBuck.objects.filter(user=user).exclude(slug='passed_idiot_test'):
            b.delete()

        messages.success(request, 'Откатили. Инжой')
        back_to_user = HttpResponseRedirect(f'{reverse("guru:check_user")}?email={email}')
        return back_to_user


class CuratorsList(GuruRequiredMixin, generic.ListView):
    template_name = "intake/guru/curators_list.html"
    context_object_name = "curators"

    def get_queryset(self):
        return UserProfile.objects.filter(is_curator=True)


class WPAssignmentsList(GuruRequiredMixin, generic.ListView):
    template_name = "intake/guru/assignments_list.html"
    context_object_name = "assignments"

    def get_queryset(self):
        return WorkshopProgramAssignment.objects.all()


class AddCurator(GuruRequiredMixin, generic.View):
    template_name = "intake/guru/add_curator.html"
    form_class = WPSelectMultipleForm

    def get_back_urls(self):
        return [(reverse("guru:curators_list"), u"Назад к списку кураторов")]

    def get(self, request, *args, **kwargs):
        email = self.request.GET.get("email")

        back_to_list = redirect("guru:curators_list")
        if email is None:
            messages.error(request, u"Телепаты в отпуске. Введи емейл")
            return back_to_list
        else:
            user = get_object_or_None(User, email=email)
            if user is None:
                messages.error(
                    request, u"Нет такого пользователя с емейлом %s." % email
                )
                return back_to_list
            else:
                programs = user.workshopprogrammetainfo_set.all()
                form = self.form_class(
                    initial={"programs": [x.pk for x in programs], "user_pk": user.pk}
                )
                context = {
                    "form": form,
                    "user": user,
                    "back_urls": self.get_back_urls(),
                }
                if user.userprofile.has_goodies():
                    context["has_goodies"] = True
                    context["programs"] = programs
                    context["goodies"] = {
                        "is_nabor": user.userprofile.is_nabor,
                        "is_guru": user.userprofile.is_guru,
                        "is_curator": user.userprofile.is_curator,
                    }

                return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = get_object_or_None(User, pk=cd["user_pk"])
            if user is None:
                messages.error(
                    request, u"Внезапно пользователь потерялся. Please try again"
                )
            else:
                prof = user.userprofile
                prof.is_curator = True
                prof.save()

                user.workshopprogrammetainfo_set.set(cd["programs"])
                messages.success(
                    request,
                    u"Пользователь с емейлом %s теперь почётный куратор %s"
                    % (user.email, ", ".join([u"%s" % p for p in cd["programs"]])),
                )
        else:
            messages.error(request, u"Что-то пошло не так. Попробуй снова")

        return redirect("guru:curators_list")


class BucksStats(GuruRequiredMixin, generic.TemplateView):
    template_name = "intake/guru/bucks_stats.html"

    def get_context_data(self, *args, **kwargs):
        context = super(BucksStats, self).get_context_data(*args, **kwargs)
        distinct_slugs = UserBuck.objects.values_list("slug", flat=True).distinct()
        context["total_users"] = len(UserProfile.objects.all())
        context["total_bucks"] = {
            "distinct": len(distinct_slugs),
            "count": UserBuck.objects.all().count(),
        }

        context["bucks"] = [
            (
                slug,
                UserBuck.objects.filter(slug=slug).count(),
                UserBuck.objects.filter(slug=slug).latest("when"),
            )
            for slug in distinct_slugs
        ]
        return context


class Handymanship(GuruRequiredMixin, generic.ListView):
    template_name = "intake/guru/handymanship.html"
    context_object_name = "handyguys"

    def get_queryset(self):
        q = Q(userhandymanshipinfo__can_before=True) | Q(
            userhandymanshipinfo__can_after=True
        )
        raw_data = (
            User.objects.filter(
                pk__in=[
                    u.pk
                    for u in filter(
                        lambda u: has_buck(u, "accepted_by_workshop"),
                        User.objects.all(),
                    )
                ]
            )
            .filter(q)
            .values_list(
                "workshoppetitionconnection__workshop_slug",
                "workshoppetitionconnection__program_slug",
                "last_name",
                "first_name",
                "userprofile__middle_name",
                "email",
                "useradditionalcontacts__phone",
                "userhandymanshipinfo__can_before",
                "userhandymanshipinfo__can_after",
                "userhandymanshipinfo__relevant_skills",
            )
        )
        return raw_data

    def get_context_data(self, **kwargs):
        context = super(Handymanship, self).get_context_data(**kwargs)

        context["workshops_slug2name_dict"] = workshops_dict()
        context["programs_slug2name_dict"] = programs_dict()

        return context


class Shkololo(GuruRequiredMixin, generic.ListView):
    template_name = 'intake/guru/shkololo.html'
    context_object_name = 'accepted_underage_apps'

    def get_queryset(self):
        startdate = get_start_date()
        eighteen_years_in_days = 365 * 18 + 4  # 4 -- account for leap years

        raw_data = User.objects.filter(
            pk__in=[
                u.pk
                for u in filter(
                    lambda u: has_buck(u, 'accepted_by_workshop'), User.objects.all()
                )
            ]
        ).values_list('pk', 'userprofile__birthdate')

        underage_users_pk = filter(
            lambda x: (startdate - x[1]).days < eighteen_years_in_days, raw_data
        )

        data = (
            User
            .objects
            .filter(pk__in=[x[0] for x in underage_users_pk])
            .values_list(
                'workshoppetitionconnection__workshop_slug',
                'workshoppetitionconnection__program_slug',
                'last_name',
                'first_name',
                'userprofile__middle_name',
                'userprofile__birthdate',
                'userprofile__location',
                'underageuserinfo__parental_name',
                'underageuserinfo__parental_phone',
                'underageuserinfo__parental_email',
            )
        )

        return data

    def get_context_data(self, **kwargs):
        context = super(Shkololo, self).get_context_data(**kwargs)

        context['workshops_slug2name_dict'] = workshops_dict()
        context['programs_slug2name_dict'] = programs_dict()

        return context


class SpamList(GuruRequiredMixin, generic.ListView):
    template_name = "intake/guru/spam_list.html"
    context_object_name = "all_apps"

    def get_queryset(self):
        data = User.objects.all().values_list(
            "workshoppetitionconnection__workshop_slug",
            "workshoppetitionconnection__program_slug",
            "last_name",
            "first_name",
            "userprofile__middle_name",
            "userprofile__birthdate",
            "email",
        )

        return data

    def get_context_data(self, **kwargs):
        context = super(SpamList, self).get_context_data(**kwargs)

        context["workshops_slug2name_dict"] = workshops_dict()
        context["programs_slug2name_dict"] = programs_dict()

        return context


class UploadBlackList(GuruRequiredMixin, generic.View):
    template_name = "intake/guru/upload_black_list.html"
    form_class = BlackListUploadForm

    def get_back_urls(self):
        return [(reverse("guru:curators_list"), u"Назад к списку кураторов")]

    def get(self, request, *args, **kwargs):
        context = {'form': BlackListUploadForm()}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            xlsx = form.cleaned_data['xlsx']

            rows_in_file = black_list_helpers.process_xlsx(xlsx)
            count = black_list_helpers.rows_to_django_models(rows_in_file, save=True)

            messages.success(
                request,
                f'В чёрный список было добавлено {count} записей'
            )
        else:
            messages.error(request, 'Что-то пошло не так. Попробуй снова')

        return redirect('guru:upload_blacklist')

# vim: set ts=4 sw=4 sts=4 et :
