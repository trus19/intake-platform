# coding: utf-8

from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, HTML  # Div, Hidden
from crispy_forms.bootstrap import FormActions

from ..models import WorkshopProgramMetaInfo
from ..widgets import CheckboxSelectMultiple


class WPSelectMultipleForm(forms.Form):
    programs = forms.ModelMultipleChoiceField(
        queryset=WorkshopProgramMetaInfo.objects.all(), widget=CheckboxSelectMultiple
    )
    user_pk = forms.IntegerField(widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        super(WPSelectMultipleForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("programs"),
            Field("user_pk"),
            FormActions(
                Submit(
                    "submit",
                    u"Сделать куратором выбранных направлений",
                    css_class="btn btn-primary",
                ),
                HTML(
                    u"""
                     {% for url, caption in back_urls %}
                     <a class="btn btn-default" href="{{ url }}">{{ caption }}</a>
                     {% endfor %}
                     """
                ),
            ),
        )
        self.helper = helper


class BlackListUploadForm(forms.Form):
    xlsx = forms.FileField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field('xlsx'),
            FormActions(
                Submit(
                    'submit',
                    'Загрузить ёпт',
                    css_class='btn btn-primary',
                ),
            ),
        )
        self.helper = helper
