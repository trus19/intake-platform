from django.contrib import admin

from .models import (
    WorkshopPetitionConnection,
    GiveBackInfo,
    PastFuturePresentInfo,
    UserPhoto,
    UserBuck,
    UserHandymanshipInfo,
    ApplicationCase,
    ApplicationCaseComment,
    WorkshopProgramMetaInfo,
    ApplicationAssignmentResponse,
    WorkshopProgramAssignment,
    WorkshopProgramShoutOut,
    UserEducationInfo,
    UserActivityInfo,
    UserLogisticsInfo,
    AppEmail,
    UserPrepodInfo,
    UserAdditionalContacts,
    UnderageUserInfo,
    UserCuratorInfo,
    BlackListCategory,
    BlackListEntry,
    WorkshopProgramEmailTemplate,
)

admin.site.register(UserPhoto)
admin.site.register(GiveBackInfo)
admin.site.register(WorkshopPetitionConnection)
admin.site.register(PastFuturePresentInfo)
admin.site.register(UserBuck)
admin.site.register(UserHandymanshipInfo)
admin.site.register(ApplicationCase)
admin.site.register(ApplicationCaseComment)
admin.site.register(UserEducationInfo)
admin.site.register(UserActivityInfo)
admin.site.register(UserAdditionalContacts)
admin.site.register(UserLogisticsInfo)
admin.site.register(UnderageUserInfo)
admin.site.register(ApplicationAssignmentResponse)
admin.site.register(WorkshopProgramEmailTemplate)


@admin.register(BlackListCategory)
class BlackListEntryAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "limitations", "severity")


@admin.register(BlackListEntry)
class BlackListEntryAdmin(admin.ModelAdmin):
    list_display = ("__str__", "first_name", "last_name", "category")
    list_filter = ["category"]


@admin.register(AppEmail)
class AppEmailAdmin(admin.ModelAdmin):
    list_display = ("__str__", "to_user", "subject", "date_created")


@admin.register(WorkshopProgramShoutOut)
class WPShoutAdmin(admin.ModelAdmin):
    filter_horizontal = ("users",)
    list_display = ("__str__", "w_slug", "p_slug", "shout_type", "date_created")
    readonly_fields = ("author", "workshop_program", "users", "shout_type")
    list_filter = ["shout_type"]


@admin.register(WorkshopProgramMetaInfo)
class WPMetaAdmin(admin.ModelAdmin):
    filter_horizontal = ("curators",)
    list_display = (
        "__str__",
        "workshop_slug",
        "program_slug",
        "workshop_name",
        "program_name",
    )
    list_editable = ("workshop_slug", "program_slug")


@admin.register(WorkshopProgramAssignment)
class AssignmentAdmin(admin.ModelAdmin):
    list_display = ("__str__", "assignment", "date_uploaded", "workshop_program")
    list_editable = ("workshop_program",)
    list_filter = ["workshop_program"]


class ElderAdmin(admin.ModelAdmin):
    list_display = ("__str__", "workshop_program")
    list_filter = ["workshop_program"]
    radio_fields = {"housing": admin.VERTICAL, "workshop_program": admin.VERTICAL}


admin.site.register(UserPrepodInfo, ElderAdmin)
admin.site.register(UserCuratorInfo, ElderAdmin)
