# coding: utf-8

import os
from datetime import datetime, timedelta
from typing import Optional
from uuid import uuid4

from annoying.fields import JSONField
from annoying.functions import get_object_or_None
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.validators import MaxLengthValidator
from django.db import models
from django.db.models import Q
from django.utils.deconstruct import deconstructible


@deconstructible
class PathAndRename(object):
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split(".")[-1]
        # set filename as random string
        filename = "{}.{}".format(uuid4().hex, ext)
        # return the whole path to the file
        path = datetime.now().strftime(self.path)
        return os.path.join(path, filename)


path_and_rename = PathAndRename("user-photos/%Y-%m-%d/")
assignment_rename = PathAndRename("workshop-assignments/%Y-%m-%d/")
wpresponse_rename = PathAndRename("w-assignments-response/%Y-%m-%d/")


class ApplicationNewbie(models.Model):
    phone = models.CharField(u"Телефон", max_length=20)
    skype = models.CharField(u"Skype", max_length=100, blank=True, null=True)
    vk = models.CharField(u"Вконтакте", max_length=100, blank=True, null=True)

    parents_info = JSONField(blank=True, null=True)
    other_schools_info = JSONField(blank=True, null=True)

    # LSH-before
    been_before = JSONField(u"Раньше на ЛШ", blank=True, null=True)
    nostalgia = models.CharField(u"Ностальгия", max_length=500, blank=True, null=True)

    # education
    finished_ed = models.CharField(u"Образование", max_length=100)
    affiliation = models.CharField(
        u"Название учебного заведения", max_length=100, blank=True, null=True
    )

    main_activity = models.CharField(u"Основной вид деятельности", max_length=1000)
    additional_activity = models.CharField(u"Другие виды деятельности", max_length=1000)

    about_self = models.CharField(u"О себе", max_length=1000)

    leakage_source = JSONField(u"Откуда о ЛШ знает", blank=True, null=True)

    @property
    def motivation(self):
        return ''

    @classmethod
    def from_dict(cls, d):
        new_acc = cls()
        for k, v in d.items():
            new_acc.__dict__[k] = v
        new_acc.save()
        return new_acc

    def __str__(self):
        u = self.user_profile
        if u is None:
            email = 'unkwnon@unknown.com'
        else:
            email = u.user.email
        return f'[#{self.pk}] {email}'

    class Meta:
        app_label = "intake"
        verbose_name = u"Базовая анкета (Новые участники)"
        verbose_name_plural = u"Базовые анкеты (Новые участники)"


class WorkshopProgramMetaInfo(models.Model):
    """ a class for holding workshop program info that shows for user-selection.
        This is to speed up loading and facilitate curator access
    """

    curators = models.ManyToManyField(
        User, limit_choices_to={"userprofile__is_curator": True}, blank=True
    )

    workshop_slug = models.SlugField(max_length=200)
    program_slug = models.SlugField(max_length=200)

    workshop_name = models.CharField(
        u"Название мастерской (300 символов)", max_length=300
    )
    program_name = models.CharField(u"Название программы", max_length=200)

    is_underage_friendly = models.BooleanField('Принимает школоту', default=False)

    class Meta:
        app_label = "intake"
        verbose_name = u"Программа мастерской"
        verbose_name_plural = u"Программы мастерских"

    @property
    def underage_str(self):
        if self.is_underage_friendly:
            return 'принимает школьников'
        else:
            return 'не принимает школьников'

    def __str__(self):
        return f'{self.workshop_slug}: {self.program_slug} ({self.underage_str})'

    def as_dict(self):
        return {
            "workshop_slug": self.workshop_slug,
            "workshop_name": self.workshop_name,
            "program_slug": self.program_slug,
            "program_name": self.program_name,
            "underage_str": self.underage_str,
        }


class WorkshopProgramAssignment(models.Model):
    assignment = models.FileField(u"Задание", upload_to=assignment_rename)
    date_uploaded = models.DateTimeField(auto_now_add=True)
    autosend = models.BooleanField(u"Задание авторассылается", default=True)

    workshop_program = models.ForeignKey(
        WorkshopProgramMetaInfo, on_delete=models.CASCADE
    )

    def __str__(self):
        w_slug = self.workshop_program.workshop_slug
        p_slug = self.workshop_program.program_slug,
        return f'Задание для {w_slug}/{p_slug} от {self.date_uploaded}'

    @property
    def file_absolute_url(self):
        if self.assignment:
            domain = Site.objects.get_current().domain
            return "https://%s%s" % (domain, self.assignment.url)
        else:
            return "https://HELPIMTRAPPEDINLSHSITE%s" % self.pk

    class Meta:
        app_label = "intake"
        verbose_name = u"Вступительное задание (само задание)"
        verbose_name_plural = u"Вступительные задания (сами задания)"


class WorkshopProgramEmailTemplate(models.Model):
    TRIGGER_WHEN_NABOR_PASSED = 1
    TRIGGER_WHEN_WORKSHOP_ACCEPTS = 2

    EMAIL_TRIGGER_CHOICES = [
        (TRIGGER_WHEN_NABOR_PASSED, u"Когда письмо прошло через 'Набор'"),
        (TRIGGER_WHEN_WORKSHOP_ACCEPTS, u"Когда заявка принимается на мастерскую"),
    ]

    text = models.TextField(
        verbose_name=u"Текст шаблона письма",
        help_text=u"Все сообщения будут отправлены от no-reply@letnyayshkola.ru",
    )
    email_trigger_type = models.IntegerField(
        u"Когда это письмо отсылается",
        choices=EMAIL_TRIGGER_CHOICES,
        default=TRIGGER_WHEN_NABOR_PASSED,
    )
    date_created = models.DateTimeField(auto_now_add=True)
    workshop_program = models.ForeignKey(
        WorkshopProgramMetaInfo, on_delete=models.CASCADE
    )

    class Meta:
        app_label = "intake"
        verbose_name = u"Шаблон письма программе"
        verbose_name_plural = u"Шаблоны писем программе"

    def __str__(self):
        wp = self.workshop_program
        trigger = self.get_email_trigger_type_display()
        return f'Шаблон письма для {wp} от {self.date_created} по случаю {trigger}'

    @classmethod
    def all_for_wp_curator(cls, user, w_slug=None, p_slug=None):
        if not user.userprofile.is_curator or (
            (w_slug is None) and (p_slug is not None)
        ):
            return []

        user_wp_metas = user.workshopprogrammetainfo_set
        if w_slug is not None:
            wp_metas = user_wp_metas.filter(workshop_program__workshop_slug=w_slug)
        if p_slug is not None:
            wp_metas = user_wp_metas.filter(workshop_program__program_slug=p_slug)
        if (w_slug is None) and (p_slug is None):
            wp_metas = user_wp_metas.all()

        templates = []
        for trigger_type in [x[0] for x in cls.EMAIL_TRIGGER_CHOICES]:
            for w_p in wp_metas:
                t = cls.objects.filter(
                    workshop_program__workshop_slug=w_p.workshop_slug,
                    workshop_program__program_slug=w_p.program_slug,
                    email_trigger_type=trigger_type,
                )
                if len(t) == 0:
                    t = None
                elif len(t) > 1:
                    templates.append(t.latest("date_created"))
                elif len(t) == 1:
                    templates.append(t[0])
        return templates

    @classmethod
    def get_for_wp(cls, w_slug=None, p_slug=None, email_trigger_type=None):
        if any(x is None for x in (email_trigger_type, w_slug, p_slug)):
            return None
        t = cls.objects.filter(
            workshop_program__workshop_slug=w_slug,
            workshop_program__program_slug=p_slug,
            email_trigger_type=email_trigger_type,
        )
        if len(t) == 0:
            return None
        elif len(t) > 1:
            return t.latest("date_created")
        elif len(t) == 1:
            return t[0]

    @classmethod
    def get_after_nabor_for_app(cls, app):
        w, p = app.wp_slug_tuple
        return cls.get_for_wp(w, p, cls.TRIGGER_WHEN_NABOR_PASSED)

    @classmethod
    def get_after_wp_accept_for_app(cls, app):
        w, p = app.wp_slug_tuple
        return cls.get_for_wp(w, p, cls.TRIGGER_WHEN_WORKSHOP_ACCEPTS)

    def render(self):
        if self.email_trigger_type == self.TRIGGER_WHEN_NABOR_PASSED:
            prefix = (
                u"Добрый день.\n\nВам предлагается выполнить вступительное задание\n"
            )
            coda = u"\nВсего доброго,\nСара Коннор\n\nP.s. Ящик no-reply@letnyayashkola.ru никто не читает, на это письмо отвечать не нужно"
            return u"\n".join([prefix, self.text, coda])
        elif self.email_trigger_type == self.TRIGGER_WHEN_WORKSHOP_ACCEPTS:
            prefix = u"Добрый день.\n\nПоздравляю, вы приняты на ЛШ2020. Мы вас посчитали, и ждём, что вы к нам приедете.\n\nДо встречи на ЛШ.\n"
            coda = u"\nВсякие новости про ЛШ можно найти на https://letnyayashkola.org и в нашей группе вконтакте https://vk.com/shkola_rr\n\nВсего доброго,\nСара Коннор\n\nP.s. Ящик no-reply@letnyayashkola.ru никто не читает, на это письмо отвечать не нужно"
            return u"\n".join([prefix, self.text, coda])
        else:
            return ""


class WorkshopProgramShoutOut(models.Model):
    TO_PASSED_APPS = 1
    TO_ACCEPTED_APPS = 2
    TO_DECLINED_APPS = 3
    TO_SLACKERS_APPS = 4

    SHOUT_CHOICES = [
        (TO_PASSED_APPS, u"Впроцессникам"),
        (TO_ACCEPTED_APPS, u"Только принятым"),
        (TO_DECLINED_APPS, u"Только отказникам"),
        (TO_SLACKERS_APPS, u"Раздолбам-впроцессникам"),
    ]

    author = models.ForeignKey(
        User, blank=True, null=True, related_name="shout", on_delete=models.SET_NULL
    )
    date_created = models.DateTimeField(auto_now_add=True)
    workshop_program = models.ForeignKey(
        WorkshopProgramMetaInfo, blank=True, null=True, on_delete=models.SET_NULL
    )
    subject = models.CharField(
        verbose_name=u"Тема письма", max_length=500, help_text=u"Не более 500 символов."
    )
    text = models.TextField(
        verbose_name=u"Тело письма для сообщения анкетам",
        help_text=u"Все сообщения будут отправлены от no-reply@letnyayshkola.ru",
    )
    users = models.ManyToManyField(
        User, verbose_name=u"Пользователи, получившие рассылку"
    )
    shout_type = models.IntegerField(
        u"Вид выхлопа", choices=SHOUT_CHOICES, default=TO_PASSED_APPS
    )

    class Meta:
        app_label = "intake"
        verbose_name = u"Массовое сообщение программе"
        verbose_name_plural = u"Массовые сообщения программе"

    def __str__(self):
        return f'Shout to {self.workshop_program} from {self.author}'

    def w_slug(self):
        return self.workshop_program.workshop_slug

    def p_slug(self):
        return self.workshop_program.program_slug


class AppEmail(models.Model):
    author = models.ForeignKey(
        User,
        blank=True,
        null=True,
        related_name="author_email",
        on_delete=models.SET_NULL,
    )
    date_created = models.DateTimeField(auto_now_add=True)
    subject = models.CharField(
        verbose_name=u"Тема письма", max_length=500, help_text=u"Не более 500 символов."
    )
    text = models.TextField(
        verbose_name=u"Тело письма",
        help_text=u"Сообщение будут отправлены от no-reply@letnyayshkola.ru",
    )
    to_user = models.ForeignKey(User, on_delete=models.CASCADE)
    relevant_buck = models.ForeignKey(
        "UserBuck", null=True, blank=True, on_delete=models.SET_NULL
    )

    class Meta:
        app_label = "intake"
        verbose_name = u"Письмо участнику"
        verbose_name_plural = u"Письма участникам"

    def __str__(self):
        return f'Письмо от {self.author} к {self.to_user}'

    def as_text(self):
        return '\n'.join(
            [
                f'from: {self.author} to: {self.to_user} date: {self.date_created}',
                f'relevant_buck: {self.relevant_buck}',
                f'subject: {self.subject}',
                f'body: {self.text}',
            ]
        )


class ApplicationAssignmentResponse(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    response = models.FileField(
        u"Ответ",
        upload_to=wpresponse_rename,
        help_text=u"Кураторы ожидают ответ в таком же формате, что и само задание. Око за око, зуб за зуб, вордовый документ за вордовый документ",
    )
    date_uploaded = models.DateTimeField(auto_now=True)
    assignment = models.ForeignKey(WorkshopProgramAssignment, on_delete=models.CASCADE)

    comment = models.TextField(
        verbose_name=u"Комментарий к ответу",
        help_text=u"Сюда можно писать какие-то короткие предположения или другие замечания, которые вы хотите передать куратору.",
        blank=True,
        null=True,
    )
    is_complete = models.BooleanField(u"Задание закончено", default=False)

    def __str__(self):
        return 'Ответ {} на задание {}/{}'.format(
            self.user.email,
            self.assignment.workshop_program.workshop_slug,
            self.assignment.workshop_program.program_slug,
        )

    @property
    def assignment_absolute_url(self):
        return self.assignment.file_absolute_url

    @property
    def response_absolute_url(self):
        if self.response:
            domain = Site.objects.get_current().domain
            return "//%s%s" % (domain, self.response.url)
        else:
            return None

    @classmethod
    def all_for_user(cls, user, w_slug=None, p_slug=None):
        if (w_slug is None) and (p_slug is None):
            return cls.objects.filter(user=user)
        else:
            return cls.objects.filter(
                user=user,
                assignment__workshop_program__workshop_slug=w_slug,
                assignment__workshop_program__program_slug=p_slug,
            )

    class Meta:
        app_label = "intake"
        verbose_name = u"Ответ на вступительное задание"
        verbose_name_plural = u"Ответы на вступительные задания"


class BlackListCategory(models.Model):
    name = models.CharField(
        u"Категория",
        help_text="Например, 'Группа Z'. 100 символов max",
        max_length=100,
        blank=True,
        null=True,
    )
    description = models.TextField(verbose_name="Кто эти люди?")
    limitations = models.TextField(
        verbose_name="Ограничения",
        help_text=u"Отвечает на вопрос 'что теперь?', но может быть туманным",
    )
    severity = models.SmallIntegerField(
        verbose_name=u"Суровость.",
        help_text=u"Эмоциональный показатель. По нему заодно сортируем",
    )

    class Meta:
        app_label = "intake"
        verbose_name = u"Категория отщепенцов в чёрном списке"
        verbose_name_plural = u"Категории отщепенцов для чёрного списка"

    def __str__(self):
        return self.name


class BlackListEntry(models.Model):
    first_name = models.TextField()
    last_name = models.TextField()
    reason = models.TextField(blank=True, null=True)
    who_added_this_twat = models.TextField(blank=True, null=True)
    last_known_workshop = models.TextField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    category = models.ForeignKey(
        BlackListCategory, blank=True, null=True, on_delete=models.SET_NULL
    )

    class Meta:
        app_label = "intake"
        verbose_name = u"Отщепенец в чёрном списке"
        verbose_name_plural = u"Отщепенцы в чёрном списке"

    def __str__(self):
        return f'{self.first_name} {self.last_name} ({self.category})'


class WorkshopPetitionConnection(models.Model):
    workshop_slug = models.SlugField(max_length=200)
    program_slug = models.SlugField(max_length=200)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    wait_but_why = models.TextField(
        help_text=u"Чем мотивирован выбор",
        verbose_name=u"Почему именно это направление?",
    )
    is_jailbait = models.BooleanField(
        'возможно пытается притвориться школьником',
        default=False,
    )

    def __str__(self):
        return f'{self.user} -> {self.workshop_slug}:{self.program_slug}'

    class Meta:
        app_label = "intake"
        verbose_name = u"Связка пользователь-программа на мастерской"
        verbose_name_plural = u"Петиции пользователей"

    def to_dict(self):
        return {
            "workshop_slug": self.workshop_slug,
            "program_slug": self.program_slug,
            "user": self.user,
            "wait_but_why": self.wait_but_why,
            "underage_str": self.is_jailbait,
        }


class UserAdditionalContacts(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    phone = models.CharField(
        u"Телефон",
        validators=[MaxLengthValidator(20, message=u"Не более 20 символов")],
        max_length=20,
    )
    skype = models.CharField(
        u"Skype",
        max_length=100,
        validators=[MaxLengthValidator(100, message=u"Не более 100 символов")],
        null=True,
        blank=True,
    )
    vk = models.CharField(
        u"Ссылка на профиль вконтакте",
        max_length=100,
        validators=[MaxLengthValidator(100, message=u"Не более 100 символов")],
        null=True,
        blank=True,
    )
    fb = models.CharField(
        u"Ссылка на профиль в фейсбуке",
        max_length=100,
        validators=[MaxLengthValidator(100, message=u"Не более 100 символов")],
        null=True,
        blank=True,
    )
    tg = models.CharField(
        u"Телеграм (без @)",
        max_length=100,
        validators=[MaxLengthValidator(100, message=u"Не более 100 символов")],
        null=True,
        blank=True,
    )

    def get_data(self):
        non_empty = []
        f2pre = {"vk": "vk.com", "fb": "fb.com", "tg": "t.me"}
        for field in ["phone", "skype", "vk", "fb", "tg"]:
            if self.__dict__[field]:
                is_link = field in ["vk", "fb", "tg"]
                val = self.__dict__[field]
                if is_link and not val.startswith("http"):
                    if (
                        val.startswith("vk")
                        or val.startswith("facebook")
                        or val.startswith("fb.com")
                    ):
                        non_empty += [(field, "https://%s" % val, is_link)]
                    else:
                        non_empty += [
                            (field, "https://%s/%s" % (f2pre[field], val), is_link)
                        ]
                else:
                    if field == "phone":
                        non_empty += [('✆', val, is_link)]
                    else:
                        non_empty += [(field, val, is_link)]
        return non_empty

    class Meta:
        app_label = "intake"

    def to_dict(self):
        return {
            "user": self.user,
            "phone": self.phone,
            "skype": self.skype,
            "vk": self.vk,
            "fb": self.fb,
            "tg": self.tg,
        }


class UserEducationInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    EDU_CHOICES_2016 = [
        (201600, u"учусь в школе"),
        (201601, u"учусь в ВУЗе в бакалавриате"),
        (201801, u"учусь в ВУЗе на специалиста"),
        (201602, u"учусь в ВУЗе в магистратуре"),
        (201603, u"учусь в среднем специальном учебном заведении"),
        (201604, u"учусь в начальном профессиональном учебном заведении"),
        (201605, u"закончил школу"),
        (201606, u"закончил ПТУ"),
        (201607, u"среднее"),
        (201608, u"бакалавриат"),
        (201609, u"закончил магистратуру"),
        (201802, u"окончил специалитет"),
        (201610, u"учусь в ординатуре"),
        (201611, u"учусь в интернатуре"),
        (201612, u"учусь в адъюнктуре"),
        (201613, u"учусь в аспирантуре"),
        (201614, u"учусь в докторантуре"),
        (201615, u"являюсь кандидатом наук"),
        (201616, u"являюсь доктором наук"),
    ]

    finished_ed = models.IntegerField(u"Образование", choices=EDU_CHOICES_2016)
    affiliation = models.TextField(
        u"Название учебного заведения",
        help_text=u"Сюда можно писать названия степеней, факультетов, направлений и тому подобные штуки",
    )

    class Meta:
        app_label = "intake"


class UserActivityInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    main_occupation = models.TextField(verbose_name=u"Основная деятельность")
    hobbies = models.TextField(verbose_name=u"хобби", blank=True, null=True)

    class Meta:
        app_label = "intake"

    def to_dict(self):
        return {
            "user": self.user,
            "main_occupation": self.main_occupation,
            "hobbies": self.hobbies,
        }


class UserHandymanshipInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    relevant_skills = models.TextField(
        verbose_name=u"Квартирьерство-skills", blank=True, null=True
    )
    can_before = models.BooleanField(
        verbose_name=u"Могу быть на квартирьерстве (1-11 июля)", blank=True
    )
    can_after = models.BooleanField(
        verbose_name=u"Могу участвовать в разборе лагеря (9-14 августа)", blank=True
    )

    class Meta:
        app_label = "intake"


class GiveBackInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    SKILLS_CHOICES = (
        ("ORG", u"Организационная помощь"),
        ("BYT", u"Бытовая помощь"),
        ("TEX", u"Техническая помощь"),
    )
    MONEY_CHOICES = (
        ("550", u"550 рублей/день"),
        ("600", u"600 рублей/день"),
        ("700", u"700 рублей/день"),
        ("800", u"800 рублей/день"),
        ("900", u"900 рублей/день"),
        ("1000", u"1000 рублей/день"),
        ("1100", u"1100 рублей/день"),
        ("1200", u"1200 рублей/день"),
        (">1200", u"более 1200 рублей/день"),
    )
    relevant_skills = JSONField(u"Быт/Организатор-madskillz", blank=True, null=True)
    money = models.CharField(
        u"Какая сумма в день вам кажется адекватной",
        max_length=20,
        choices=MONEY_CHOICES,
        blank=True,
        null=True,
    )

    def __str__(self):
        return f'GiveBackInfo от {self.user} ({self.user.email})'

    def skills_list(self):
        EMPTY = []
        if self.relevant_skills:
            if isinstance(self.relevant_skills, str):
                return [("", "Тут случилась ОШИБКА. НАПИШИ window@letnyayashkola.org. #GIBE%s" % self.pk)]
            s = self.relevant_skills.get("skills")
            if s is None:
                return EMPTY
            return [(c, dict(self.SKILLS_CHOICES)[c]) for c in s]
        else:
            return EMPTY

    def skills_other(self):
        EMPTY = ""
        if self.relevant_skills:
            s = self.relevant_skills.get("skills_other")
            if s is None:
                return EMPTY
            return s
        else:
            return EMPTY

    class Meta:
        app_label = "intake"


class PastFuturePresentInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    LEAKAGE_CHOICES = [
        (0, u"от знакомых"),
        (1, u'из журнала "Русский Репортёр"'),
        (2, u'из журнала "Кот Шрёдингера"'),
        (3, u"в социальных сетях"),
        (4, u"где-то ещё в Интернете"),
        (5, u"как-то ещё (укажите ниже)"),  # MAGIC NUMBER!
    ]
    been_before = JSONField(u"Раньше на ЛШ", blank=True, null=True)
    leakage_source = JSONField(u"Откуда о ЛШ знает", blank=True, null=True)

    @property
    def motivation(self):
        return ''

    def leak_src_list(self):
        EMPTY = []
        if self.leakage_source:
            if isinstance(self.leakage_source, str):
                return [
                    (
                        "",
                        f"Тут случилась ОШИБКА. Напишите на почту window@letnyayashkola.org, скажите #PFPI{self.pk}"
                    )
                ]
            elif isinstance(self.leakage_source, dict):
                leak_src = self.leakage_source
            else:
                return EMPTY
            s = leak_src.get("src_select")
            s = filter(lambda x: int(x) != 5, s)  # MAGIC NUMBER
            if s is None:
                return EMPTY
            return [(c, dict(self.LEAKAGE_CHOICES)[int(c)]) for c in s]
        else:
            return EMPTY

    def leak_src_other(self):
        EMPTY = ""
        if self.leakage_source:
            if isinstance(self.leakage_source, str):
                return "Тут случилась ОШИБКА. НАПИШИ window@letnyayashkola.org. #PFPI%s.OTHER" % self.pk
            s = self.leakage_source.get("src_other")
            if s is None:
                return EMPTY
            return s
        else:
            return EMPTY

    def lsh_schools(self):
        if isinstance(self.been_before, str):
            return []
        try:
            return filter(lambda x: x["lsh"].startswith(u"Да"), self.been_before)
        except (KeyError, TypeError):
            return []

    def other_schools(self):
        if not self.been_before:
            return []
        if isinstance(self.been_before, str):
            return []
        try:
            return filter(lambda x: x["lsh"].startswith(u"Нет"), self.been_before)
        except (KeyError, TypeError):
            return []

    class Meta:
        app_label = "intake"


class UserLogisticsInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    TRIP_CHOICES = (
        ("AVTOBUS", u"на общем автобусе"),
        ("ELECTRO", u"своим ходом"),
        ("WITHCAR", u"на машине"),
    )

    itsmyway = models.CharField(u"Путь до ЛШ", max_length=20, choices=TRIP_CHOICES)

    class Meta:
        app_label = "intake"


class UserPhoto(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    photo = models.ImageField(
        'Фотка',
        blank=True,
        null=True,
        upload_to=path_and_rename,
        default='user-photos/default.png',
    )
    needs_to_be_changed = models.BooleanField(default=False)

    @property
    def photo_absolute_url(self):
        if self.photo:
            return self.photo.url
        else:
            return '(no photo)'

    class Meta:
        app_label = 'intake'

    def to_dict(self):
        return {
            'user': self.user,
            'photo': self.photo,
            'needs_to_be_changed': self.needs_to_be_changed,
        }

    def __str__(self):
        return f'Фотка {self.user.username}'


class UnderageUserInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    parental_confirm = models.BooleanField(
        verbose_name='Родители знают о моём решении', default=False
    )
    parental_phone = models.CharField(
        verbose_name='Телефон одного из родителей', max_length=20
    )
    parental_email = models.EmailField(
        verbose_name='E-mail одного из родителей', help_text='(лучше того же чей телефон)',
    )
    parental_name = models.CharField(
        'ФИО одного из родителей',
        validators=[MaxLengthValidator(300, message='Не более 300 символов')],
        help_text='Не более 300 символов',
        max_length=300,
    )
    cls_finished = models.TextField(
        'Класс школы (или аналогичное)',
        help_text='Класс общеобразовательной школы, который несовершеннолетний участник заканчивает в мае 2020 г. или иное (для учащихся учреждений начального профессионального, среднего специального или высшего образования)',
    )

    class Meta:
        app_label = 'intake'


class UserBuck(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    slug = models.SlugField(
        verbose_name=u"Слаг фишки",
        validators=[MaxLengthValidator(100, message=u"Не более 100 символов")],
        help_text=u"Не более 100 символов",
        max_length=100,
    )
    intention = models.CharField(
        verbose_name=u"Почему фишка поставлена",
        validators=[MaxLengthValidator(200, message=u"Не более 200 символов")],
        help_text=u"Не более 200 символов",
        null=True,
        blank=True,
        max_length=200,
    )
    when = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = "intake"
        verbose_name = u"Метка (тэг, фишка) пользователя"
        verbose_name_plural = u"Метки (тэги, фишки) пользователей"

    def __str__(self):
        return f'фишка {self.user} "{self.slug}"'


class ApplicationCase(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    is_suspicious = models.BooleanField(
        verbose_name=u"Заявка является подозрительной", default=False
    )
    created = models.DateTimeField(verbose_name=u"Открыто", blank=True, null=True)
    last_modified = models.DateTimeField(
        verbose_name=u"Последнее изменение", auto_now=True
    )
    datetime_workshop_notified = models.DateTimeField(
        verbose_name=u"Передано в мастерскую", blank=True, null=True
    )

    is_closed = models.BooleanField(verbose_name=u"Дело закрыто", default=False)

    short_comment = models.TextField(
        verbose_name=u"Краткий комментарий", blank=True, null=True
    )

    @property
    def wp_slug_tuple(self):
        wpc = WorkshopPetitionConnection.objects.filter(user=self.user)
        wpc = wpc[0]
        return (wpc.workshop_slug, wpc.program_slug)

    @property
    def wp_names_tuple(self):
        wpc = WorkshopPetitionConnection.objects.filter(user=self.user)
        w_meta = get_object_or_None(
            WorkshopProgramMetaInfo,
            workshop_slug=wpc[0].workshop_slug,
            program_slug=wpc[0].program_slug,
        )
        if w_meta is not None:
            return (w_meta.workshop_name, w_meta.program_name)
        else:
            return ("UNKNOWN", "UNKNOWN")

    def short_info(self):
        u = self.user
        d = {}

        d["photo"] = get_object_or_None(UserPhoto, user=u)
        d["first_name"] = u.first_name
        d["middle_name"] = u.userprofile.middle_name
        d["last_name"] = u.last_name
        d["email"] = u.email
        d["location"] = u.userprofile.location
        d["age"] = u.userprofile.age
        d["birthdate"] = u.userprofile.birthdate
        d["is_male"] = u.userprofile.is_male
        d["is_female"] = u.userprofile.is_female
        d["is_underage"] = u.userprofile.is_underage

        d["wpc"] = WorkshopPetitionConnection.objects.filter(user=u).values(
            "workshop_slug", "program_slug", "wait_but_why", "is_jailbait",
        )[0]

        if d["is_underage"]:
            d["underage_data"] = UnderageUserInfo.objects.filter(user=u).first()

        return d

    @property
    def is_blacklisted(self):
        q_tuda = Q(first_name=self.user.first_name, last_name=self.user.last_name)
        q_obratno = Q(last_name=self.user.first_name, first_name=self.user.last_name)

        return BlackListEntry.objects.filter(q_tuda | q_obratno).exists()

    def blacklist_category(self):
        if self.is_blacklisted:
            try:
                b = BlackListEntry.objects.get(
                    first_name=self.user.first_name, last_name=self.user.last_name
                )
            except BlackListEntry.MultipleObjectsReturned:
                # FIXME
                b = BlackListEntry.objects.filter(
                    first_name=self.user.first_name, last_name=self.user.last_name
                )
                return ", ".join([x.category.name for x in b])
            else:
                return b.category

    def full_info(self):
        u = self.user
        d = self.short_info()

        d["full_name"] = "%s %s %s" % (
            d["last_name"],
            d["first_name"],
            d["middle_name"],
        )
        d["pfpi"] = PastFuturePresentInfo.objects.get(user=u)
        d["uac"] = UserAdditionalContacts.objects.get(user=u)

        d["uei"] = UserEducationInfo.objects.get(user=u)
        d["uact"] = UserActivityInfo.objects.get(user=u)
        d["gibe"] = get_object_or_None(GiveBackInfo, user=u)

        # d['trip'] = UserLogisticsInfo.objects.get(user=u)
        # d['uhnd'] = get_object_or_None(UserHandymanshipInfo, user=u)

        # d['pfpi']['leakage_source'] = pfpi.leakage_source

        return d

    def csv_fields(self):
        for k, v in self.full_info().items():
            if k in [
                "first_name",
                "middle_name",
                "last_name",
                "email",
                "location",
                "birthdate",
            ]:
                yield (k, v)
            elif k == "uac":
                yield ("phone", v.phone)
            else:
                continue

    @classmethod
    def export_fields(self):
        fields_with_widths = {
            "first_name": 20,
            "middle_name": 20,
            "last_name": 20,
            "email": 30,
            "location": 100,
            "birthdate": 20,
            "phone": 20,
        }
        return fields_with_widths

    def xsl_fields(self):
        """ Same as csv_fields, but with tuple containing widths """

        f2w = self.export_fields()

        for k, v in self.full_info().items():
            if k in [
                "first_name",
                "middle_name",
                "last_name",
                "email",
                "location",
                "birthdate",
            ]:
                yield (k, v, f2w[k])
            elif k == "uac":
                yield ("phone", v.phone, f2w["phone"])
            else:
                continue

    def bucks(self):
        return {b.slug: b.when for b in UserBuck.objects.filter(user=self.user)}

    @property
    def bucks_list(self):
        return [
            {"slug": b.slug, "when": b.when}
            for b in UserBuck.objects.filter(user=self.user).order_by("-when")
        ]

    @property
    def dt_when_passed_nabor(self) -> Optional[datetime]:
        buck = UserBuck.objects.filter(user=self.user, slug="passed_first_step").first()
        if buck is None:
            return None

        return buck.when

    class Meta:
        app_label = "intake"
        verbose_name = u"<<Дело>> участника"
        verbose_name_plural = u"<<Дела>> участников"

    def __str__(self):
        first_name = self.user.first_name
        last_name = self.user.last_name
        email = self.user.email
        return f'Анкета участника {first_name} {last_name} ({email})'

    def comments(self):
        qs = ApplicationCaseComment.objects.filter(application=self).order_by("when")
        return [(q, q._meta) for q in qs]

    def expected_date_workshop_notified(self):
        return self.created + timedelta(days=10)

    def has_homework(self):
        return ApplicationAssignmentResponse.objects.filter(user=self.user).exists()

    def has_complete_homework(self):
        w, p = self.wp_slug_tuple
        return ApplicationAssignmentResponse.objects.filter(
            user=self.user,
            is_complete=True,
            assignment__workshop_program__workshop_slug=w,
            assignment__workshop_program__program_slug=p,
        ).exists()

    def get_homework(self):
        if self.has_complete_homework():
            w, p = self.wp_slug_tuple
            return ApplicationAssignmentResponse.objects.get(
                user=self.user,
                assignment__workshop_program__workshop_slug=w,
                assignment__workshop_program__program_slug=p,
                is_complete=True,
            )
        else:
            return None

    def homework_sent_datetime(self):
        return self.bucks().get(
            "assignment_auto_sent", self.bucks().get("assignment_sent", None)
        )

    def is_changeable_by_curator(self):
        my_bucks = UserBuck.objects.filter(user=self.user)
        final_state = my_bucks.filter(
            slug__in=["accepted_by_workshop", "declined_by_workshop"]
        )
        if len(final_state) == 0:
            return True
        else:
            retry_buck = my_bucks.filter(slug="retry_granted")
            if retry_buck:
                retry_buck = retry_buck[0]
                if all(retry_buck.when > b.when for b in final_state):
                    return True
            return False

    def has_decline_email(self):
        return AppEmail.objects.filter(
            to_user=self.user, relevant_buck__slug="declined_by_workshop"
        ).exists()

    def get_decline_email(self):
        b = AppEmail.objects.filter(
            to_user=self.user, relevant_buck__slug="declined_by_workshop"
        )
        return b[0] if b else b

    def accept(self, commit=False):
        self.is_closed = True
        if commit:
            self.save()

    def mark_passed(self, commit=False):
        now = datetime.now()
        self.datetime_workshop_notified = now
        self.last_modified = now
        if commit:
            self.save()

    @property
    def has_bad_photo(self):
        return UserPhoto.objects.filter(
            user=self.user, needs_to_be_changed=True
        ).exists()


class ApplicationCaseComment(models.Model):
    author = models.ForeignKey(
        User,
        limit_choices_to={"userprofile__is_nabor": True},
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    comment = models.TextField(u"Комментарий", null=True, blank=True)
    when = models.DateTimeField(auto_now_add=True)
    application = models.ForeignKey(ApplicationCase, on_delete=models.CASCADE)

    def __str__(self):
        app, author, when = self.application, self.author, self.when
        return f'{app} Комментарий от {author} ({when})'

    def author_photo(self):
        photo = get_object_or_None(UserPhoto, user=self.author)
        return photo

    def author_name(self):
        return u"{} {}".format(self.author.first_name, self.author.last_name)

    class Meta:
        app_label = "intake"

    @property
    def is_autocomment(self):
        return self.comment.startswith(u"[auto]")


class UserPrepodInfo(models.Model):
    """ Модель для хранения информации о преподе
        Везде где можно опирается на уже существующие модели.

        FUNFACT: Может существовать с пустым юзером, хотя это как-то странно
    """

    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    BRINGS_HIS_OWN_TENT = 1
    WILL_LIVE_IN_TENT = 2
    REQUIRES_VIP_HOUSE = 3
    HOUSING_CHOICES = [
        (BRINGS_HIS_OWN_TENT, u"привезёт свою палатку"),
        (WILL_LIVE_IN_TENT, u"подселится к кому-то в палатку"),
        (REQUIRES_VIP_HOUSE, u"нужно место в вип-домике"),
    ]

    housing = models.IntegerField(
        u"жильё",
        help_text=u"Если преп не самодостаточен, в комментариях обязательно поясните почему",
        choices=HOUSING_CHOICES,
        default=BRINGS_HIS_OWN_TENT,
    )
    comment = models.TextField(
        u"Пояснение/Комментарий",
        null=True,
        blank=True,
        help_text=u"В это поле можно писать всё то, что может показаться важным, например, неочевидные причины поселения в вип-домик и то, что препод будет приезжать и уезжать несколько раз",
    )
    workshop_program = models.ForeignKey(
        "WorkshopProgramMetaInfo",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=u"Программа/направление",
    )
    regalia = JSONField(verbose_name=u"Регалии", null=True, blank=True)
    topic = models.TextField(
        u"Предварительные темы лекций",
        null=True,
        blank=True,
        help_text=u"Это общее описание того, на какие темы может рассуждать этот конкретный препод.",
    )
    lectures = JSONField(
        verbose_name=u"Лекции",
        null=True,
        blank=True,
        help_text=u"это даже с датами уже, JSON.",
    )

    arrival_date = models.DateTimeField(
        verbose_name=u"Дата заезда", null=True, blank=True
    )
    departure_date = models.DateTimeField(
        verbose_name=u"Дата отъезда", null=True, blank=True
    )

    @property
    def photo(self):
        if self.user is None:
            return None
        return get_object_or_None(UserPhoto, user=self.user)

    @property
    def contacts(self):
        if self.user is None:
            return None
        return get_object_or_None(UserAdditionalContacts, user=self.user)

    def full_info(self):
        u = self.user
        d = {}

        d["user_obj"] = u
        d["photo"] = self.photo

        if self.user:
            d["email"] = u.email
            d["first_name"] = u.first_name
            d["middle_name"] = u.userprofile.middle_name
            d["last_name"] = u.last_name

        if self.contacts:
            d["contacts_data"] = self.contacts.get_data()
        else:
            d["phone"] = u"Не указан телефон"
            d["other_contacts_data"] = []

        if self.regalia:
            for tag in ("affiliation", "education", "academic_status"):
                d[tag] = self.regalia[tag]

        d["housing"] = self.get_housing_display()
        if self.housing in [self.BRINGS_HIS_OWN_TENT, self.WILL_LIVE_IN_TENT]:
            d["is_tent_friendly"] = True

        d["comment"] = self.comment
        d["topic"] = self.topic

        d["arrival_date"] = self.arrival_date
        d["departure_date"] = self.departure_date

        if self.lectures:
            d["lectures"] = []
            for l in self.lectures:
                d["lectures"].append(
                    {
                        "date": datetime(*(int(x) for x in l["date"].split("/"))),
                        "title": l["title"],
                    }
                )

        return d

    class Meta:
        app_label = "intake"
        verbose_name = u"Преподская анкета"
        verbose_name_plural = u"Анкеты преподавателей"

    def __str__(self):
        if self.user:
            first_name = self.user.first_name
            last_name = self.user.last_name
            if self.workshop_program:
                w_slug = self.workshop_program.workshop_slug
                p_slug = self.workshop_program.program_slug
                return f'{first_name} {last_name} ({w_slug}/{p_slug})'
        else:
            return 'Prepod data about some user'

    @classmethod
    def curators_for_wp(cls, w_slug=None, p_slug=None):
        raise NotImplemented(u"Этот кусок кода тут не работает, обнови меня или выкинь")
        q = Q(status=2)  # Ugh!!!
        if all([lambda x: x is not None for x in [w_slug, p_slug]]):
            q &= Q(
                workshop_program__workshop_slug=w_slug,
                workshop_program__program_slug=p_slug,
            )
        return cls.objects.filter(q)


class UserCuratorInfo(models.Model):
    """ Модель для хранения информации о кураторе
        Везде где можно опирается на уже существующие модели.

        FUNFACT: Может существовать с пустым юзером, хотя это как-то странно
        FIXME: Должны вместе с PrepodInfo отталкиваться от одной общей модели
    """

    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)

    BRINGS_HIS_OWN_TENT = 1
    WILL_LIVE_IN_TENT = 2
    REQUIRES_VIP_HOUSE = 3
    HOUSING_CHOICES = [
        (BRINGS_HIS_OWN_TENT, u"привезёт свою палатку"),
        (WILL_LIVE_IN_TENT, u"подселится к кому-то в палатку"),
        (REQUIRES_VIP_HOUSE, u"нужно место в вип-домике"),
    ]

    housing = models.IntegerField(
        u"жильё",
        help_text=u"Если преп не самодостаточен, в комментариях обязательно поясните почему",
        choices=HOUSING_CHOICES,
        default=BRINGS_HIS_OWN_TENT,
    )
    comment = models.TextField(
        u"Пояснение/Комментарий",
        null=True,
        blank=True,
        help_text=u"В это поле можно писать всё то, что может показаться важным, например, неочевидные причины поселения в вип-домик и то, что препод будет приезжать и уезжать несколько раз",
    )
    workshop_program = models.ForeignKey(
        "WorkshopProgramMetaInfo",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=u"Программа/направление",
    )

    arrival_date = models.DateTimeField(
        verbose_name=u"Дата заезда", null=True, blank=True
    )
    departure_date = models.DateTimeField(
        verbose_name=u"Дата отъезда", null=True, blank=True
    )

    @property
    def photo(self):
        if self.user is None:
            return None
        return get_object_or_None(UserPhoto, user=self.user)

    @property
    def contacts(self):
        if self.user is None:
            return None
        return get_object_or_None(UserAdditionalContacts, user=self.user)

    def full_info(self):
        u = self.user
        d = {}

        d["user_obj"] = u
        d["photo"] = self.photo

        if self.user:
            d["email"] = u.email
            d["first_name"] = u.first_name
            d["middle_name"] = u.userprofile.middle_name
            d["last_name"] = u.last_name

        if self.contacts:
            d["contacts_data"] = self.contacts.get_data()
        else:
            d["phone"] = u"Не указан телефон"
            d["other_contacts_data"] = []

        d["housing"] = self.get_housing_display()
        if self.housing in [self.BRINGS_HIS_OWN_TENT, self.WILL_LIVE_IN_TENT]:
            d["is_tent_friendly"] = True

        d["comment"] = self.comment

        d["arrival_date"] = self.arrival_date
        d["departure_date"] = self.departure_date

        return d

    class Meta:
        app_label = "intake"
        verbose_name = u"Кураторская анкета"
        verbose_name_plural = u"Анкеты кураторов"

    def __str__(self):
        if self.user:
            first_name = self.user.first_name
            last_name = self.user.last_name
            if self.workshop_program:
                w_slug = self.workshop_program.workshop_slug
                p_slug = self.workshop_program.program_slug
                return f'{first_name} {last_name} ({w_slug}/{p_slug})'
        else:
            return u"Curator data about some user"

    @classmethod
    def curators_for_wp(cls, w_slug=None, p_slug=None):
        if all([lambda x: x is not None for x in [w_slug, p_slug]]):
            return cls.objects.filter(
                workshop_program__workshop_slug=w_slug,
                workshop_program__program_slug=p_slug,
            )
        else:
            return []
