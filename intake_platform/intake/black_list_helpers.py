from typing import IO, Any, Dict, List, NamedTuple, Union

import openpyxl

from .models import BlackListCategory, BlackListEntry


class BlackListRow(NamedTuple):
    fullname: str
    category: str
    reason: str
    entry_author: str
    last_known_ws: str

    @property
    def first_name(self):
        return self.fullname.split()[1]

    @property
    def last_name(self):
        return self.fullname.split()[0]

    def as_dict_for_django_model(
        self, category_str2obj_mapper: Dict[str, BlackListCategory]
    ) -> Dict[str, Any]:
        return {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'reason': self.reason,
            'who_added_this_twat': self.entry_author,
            'last_known_workshop': self.last_known_ws,
            'category': category_str2obj_mapper[self.category],
        }


def process_xlsx(xlsx: Union[IO, str]) -> List[BlackListRow]:
    rows = []
    workbook = openpyxl.load_workbook(xlsx)
    sheet = workbook[workbook.sheetnames[0]]

    for row in sheet.iter_rows(min_row=2, max_col=6, values_only=True):
        if row[0] is None:
            continue

        new_row = BlackListRow(
            fullname=row[1].strip(),
            category=row[2].strip(),
            last_known_ws=row[3].strip(),
            entry_author=row[4].strip(),
            reason=row[5].strip(),
        )
        rows.append(new_row)

    return rows


def rows_to_django_models(rows: List[BlackListRow], save=False) -> int:
    blacklist_category_mapper = {
        category.name: category for category in BlackListCategory.objects.all()
    }
    for row in rows:
        entry = BlackListEntry(
            **row.as_dict_for_django_model(blacklist_category_mapper)
        )
        if save:
            entry.save()

    return len(rows)
