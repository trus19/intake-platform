import random

from annoying.decorators import render_to
from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic

from intake_platform.common.tools import get_user
from intake_platform.intake.forms import AssignmentResponseForm, PhotoReplaceForm
from intake_platform.intake.models import (
    AppEmail,
    ApplicationAssignmentResponse,
    ApplicationCase,
    UserPhoto,
)
from intake_platform.intake.tools import has_buck


def context_nabor_rejected_info(user):
    context = {}
    email = get_object_or_None(AppEmail, to_user=user, relevant_buck__slug='nabor_rejected')
    if email is not None:
        context['has_decline_email'] = True
        context['email_subject'] = email.subject
        context['email_text'] = email.text
    else:
        context['has_decline_email'] = False
    return context


def context_workshop_rejected_info(user):
    context = {}
    email = get_object_or_None(
        AppEmail, to_user=user, relevant_buck__slug='declined_by_workshop'
    )
    if email is not None:
        context['has_decline_email'] = True
        context['email_subject'] = email.subject
        context['email_text'] = email.text
    else:
        context['has_decline_email'] = False
    return context


@login_required
@render_to('intake/timeline/timeline.html')
def timeline(request):
    user = get_user(request)

    if not has_buck(user, 'complete_first_step'):
        return redirect('profile')

    waiting_greetings = [
        'Запасаемся попкорном и ждём',
        'Давайте уже после майских',
        'Кто ждёт &mdash; дождётся',
        'Всё идёт по плану (надеемся)',
        'Ваша анкета пристально изучается',
        'Ещё чуть-чуть и уже лето',
    ]

    context = {'jumbo_title': random.choice(waiting_greetings)}

    prof = user.userprofile
    photo = get_object_or_None(UserPhoto, user=user)
    context['profile'] = prof
    context['photo'] = photo
    if photo and photo.needs_to_be_changed:
        photo_form = PhotoReplaceForm()
        photo_form.fields['photo'].label = 'Загрузить новое фото'
        context['photo_change_form'] = photo_form

    case = get_object_or_None(ApplicationCase, user=user)
    context['application_case'] = case

    wpc = user.workshoppetitionconnection
    hw = ApplicationAssignmentResponse.all_for_user(user, wpc.workshop_slug, wpc.program_slug)
    if len(hw) > 1:
        messages.error(
            request,
            'Проблема с вступительным заданием. Свяжитесь с нами? sarah.connor@letnyayashkola.org',
        )
        context['assignment'] = None
    else:
        context['assignment'] = hw[0] if len(hw) == 1 else None

    if has_buck(user, 'nabor_rejected'):
        context.update(context_nabor_rejected_info(user))

    if has_buck(user, 'declined_by_workshop'):
        context.update(context_workshop_rejected_info(user))

    return context


class OwnAppReadOnly(generic.View):
    template_name = 'intake/own_app_readonly.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = get_user(request)
        case = get_object_or_None(ApplicationCase, user=user)
        if case is None:
            messages.error(request, 'Что-то пошло не так с вашей анкетой. Вы её заполняли? Точно?')
            return redirect('profile')
        return render(request, self.template_name, {'object': case})


class ReadDeclineEmail(generic.View):
    form_class = AssignmentResponseForm
    template_name = 'intake/email_render.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = get_user(request)

        email = get_object_or_404(
            AppEmail, to_user=user, relevant_buck__slug='declined_by_workshop'
        )

        return render(
            request,
            self.template_name,
            {
                'email_subject': email.subject,
                'email_text': email.text,
                'back_url': reverse('profile'),
            },
        )
