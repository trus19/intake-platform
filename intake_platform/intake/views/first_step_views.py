from annoying.decorators import render_to
from annoying.functions import get_config, get_object_or_None
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect
from django.template import Context, loader

from intake_platform.common.tools import get_user, get_workshops_info
from intake_platform.intake.decorators import user_has_no_buck_with_slug
from intake_platform.intake.forms import (
    ActivityInfoForm,
    AdditionalContactsForm,
    BasicInfoForm,
    EducationInfoForm,
    GiveBackInfoForm,
    HandymanshipInfoForm,
    LeakageInfoForm,
    LogisticsInfoForm,
    MoneyInfoForm,
    PastFuturePresentInfoForm,
    PhotoUploadForm,
    UnderageUserInfoForm,
    WorkshopProgramSelectForm,
)
from intake_platform.intake.models import (
    ApplicationCase,
    GiveBackInfo,
    PastFuturePresentInfo,
    UnderageUserInfo,
    UserActivityInfo,
    UserAdditionalContacts,
    UserEducationInfo,
    UserHandymanshipInfo,
    UserLogisticsInfo,
    UserPhoto,
    WorkshopPetitionConnection,
)
from intake_platform.intake.tools import (
    attach_buck,
    check_user_jailbait,
    has_buck,
    prepare_second_round,
    robot_comment_on_case,
    send_mail_to_applicant,
)


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/get_activity_info.html')
def get_activity_info(request):
    activity = get_object_or_None(UserActivityInfo, user=get_user(request))
    if request.method == 'POST':
        form = ActivityInfoForm(request.POST, instance=activity)
        if form.is_valid():
            activity_info = form.save(commit=False)
            activity_info.user = get_user(request)
            activity_info.save()
            return redirect('profile')
    else:
        form = ActivityInfoForm(instance=activity)
    return {'form': form}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/collect_education_info.html')
def collect_education_info(request):
    uei = get_object_or_None(UserEducationInfo, user=get_user(request))
    if request.method == 'POST':
        form = EducationInfoForm(request.POST, instance=uei)
        if form.is_valid():
            uei = form.save(commit=False)
            uei.user = get_user(request)
            uei.save()
            return redirect('profile')
    else:
        form = EducationInfoForm(instance=uei)

    return {'form': form}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/provide_basic_info.html')
def provide_basic_info(request):
    user = get_user(request)

    if request.method == 'POST':
        form = BasicInfoForm(request.POST)
        if form.is_valid():
            profile = user.userprofile
            profile.is_male = bool(form.cleaned_data.get('is_male'))
            for target, field in [
                (user, 'first_name'),
                (user, 'last_name'),
                (user, 'email'),
                (profile, 'middle_name'),
                (profile, 'location'),
                (profile, 'birthdate'),
            ]:
                setattr(target, field, form.cleaned_data.get(field))
            profile.save()
            user.save()
            return redirect('profile')
    else:
        form = BasicInfoForm.from_user(user)

    return {'form': form}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/provide_contacts.html')
def provide_contacts(request):
    uac, _ = UserAdditionalContacts.objects.get_or_create(user=get_user(request))
    if request.method == 'POST':
        form = AdditionalContactsForm(request.POST, instance=uac)
        if form.is_valid():
            form.save()
            return redirect('profile')
    else:
        form = AdditionalContactsForm(instance=uac)

    return {'form': form}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/workshop_program_select.html')
def wp_select(request):
    user = get_user(request)

    if not user.userprofile.birthdate:
        messages.error(
            request,
            ' '.join(
                [
                    'Прежде чем выбирать мастерскую, нам надо знать ваш возраст',
                    '(заполните раздел "Базовая информация")',
                ]
            ),
        )
        return redirect('profile')

    wpc = get_object_or_None(WorkshopPetitionConnection, user=user)
    if request.method == 'POST':
        form = WorkshopProgramSelectForm(request.POST, instance=wpc)
        if form.is_valid():
            wpc = form.save(commit=False)

            wpc.user = user
            wpc.is_jailbait = check_user_jailbait(
                user.userprofile, wpc.workshop_slug, wpc.program_slug
            )

            wpc.save()
            return redirect('profile')
    else:
        form = WorkshopProgramSelectForm(instance=wpc)

    try:
        wshops = get_workshops_info(user.userprofile.is_adult)
    except ValueError:
        messages.error(request, 'Ошибка #75. Пожалуйста, свяжитесь с администратором')
        return redirect('profile')

    wshops = [ws for ws in wshops if ws['has_intaking_programs']]
    if len(wshops) == 0:
        messages.error(request, 'Набор закрыт. Мастерских, ведущих набор нет.')
        return redirect('profile')
    # program filtering is done in template. Not a fan of that idea, though

    return {'form': form, 'workshops': wshops}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/demand_handymanship.html')
def demand_handymanship(request):
    hnd = get_object_or_None(UserHandymanshipInfo, user=get_user(request))
    if request.method == 'POST':
        form = HandymanshipInfoForm(request.POST, instance=hnd)
        if form.is_valid():
            handimanship_info = form.save(commit=False)
            handimanship_info.user = get_user(request)
            handimanship_info.save()
            return redirect('profile')
    else:
        form = HandymanshipInfoForm(instance=hnd)
    return {'form': form}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/fill_in_money.html')
def fill_in_money(request):
    gibe = get_object_or_None(GiveBackInfo, user=get_user(request))
    if request.method == 'POST':
        form = MoneyInfoForm(request.POST, instance=gibe)
        if form.is_valid():
            give_back_info = form.save(commit=False)
            give_back_info.user = get_user(request)
            give_back_info.save()
            return redirect('profile')
    else:
        form = MoneyInfoForm(instance=gibe)
    return {'form': form}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/promise_help.html')
def promise_help(request):
    gibe = get_object_or_None(GiveBackInfo, user=get_user(request))
    if request.method == 'POST':
        form = GiveBackInfoForm(request.POST, instance=gibe)
        if form.is_valid():
            give_back_info = form.save(commit=False)

            skills_dict = {
                'skills': form.cleaned_data.get('skills'),
                'skills_other': form.cleaned_data.get('skills_other'),
            }
            give_back_info.relevant_skills = skills_dict
            give_back_info.user = get_user(request)
            give_back_info.save()
            return redirect('profile')
    else:
        if gibe and gibe.relevant_skills:
            form = GiveBackInfoForm(
                instance=gibe,
                initial={
                    'skills': gibe.relevant_skills['skills'],
                    'skills_other': gibe.relevant_skills['skills_other'],
                },
            )
        else:
            form = GiveBackInfoForm()
    return {'form': form}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/choose_my_way.html')
def choose_my_way(request):
    trip = get_object_or_None(UserLogisticsInfo, user=get_user(request))
    if request.method == 'POST':
        form = LogisticsInfoForm(request.POST, instance=trip)
        if form.is_valid():
            user_logistics_info = form.save(commit=False)
            user_logistics_info.user = get_user(request)
            user_logistics_info.save()
            return redirect('profile')
    else:
        form = LogisticsInfoForm(instance=trip)
    return {'form': form}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/retrospect.html')
def retrospect(request):
    pfpi = get_object_or_None(PastFuturePresentInfo, user=get_user(request))
    if request.method == 'POST':
        form = PastFuturePresentInfoForm(request.POST, instance=pfpi)
        if form.is_valid():
            past_future_present_info = form.save(commit=False)
            past_future_present_info.user = get_user(request)
            past_future_present_info.save()
            return redirect('profile')
    else:
        context = {}
        context['form'] = PastFuturePresentInfoForm(instance=pfpi)
        if pfpi is not None and not isinstance(pfpi.been_before, str):
            context['schools_data'] = pfpi.been_before
        return context


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/leakage.html')
def leakage(request):
    if request.method == 'POST':
        form = LeakageInfoForm(request.POST)
        if form.is_valid():
            leak_info, created = PastFuturePresentInfo.objects.get_or_create(user=get_user(request))
            src_dict = {
                'src_select': form.cleaned_data.get('src_select'),
                'src_other': form.cleaned_data.get('src_other'),
            }
            leak_info.leakage_source = src_dict
            leak_info.save()

            return redirect('profile')
    else:
        pfpi = get_object_or_None(PastFuturePresentInfo, user=get_user(request))
        if pfpi and pfpi.leakage_source:
            form = LeakageInfoForm(
                initial={
                    'src_select': pfpi.leakage_source['src_select'],
                    'src_other': pfpi.leakage_source['src_other'],
                }
            )
        else:
            form = LeakageInfoForm()

    return {'form': form}


@login_required
@render_to('intake/parents.html')
def parents(request):
    uu = get_object_or_None(UnderageUserInfo, user=get_user(request))
    if request.method == 'POST':
        form = UnderageUserInfoForm(request.POST, instance=uu)
        if form.is_valid():
            uu_info = form.save(commit=False)
            uu_info.user = get_user(request)
            uu_info.save()

            return redirect('profile')
    else:
        form = UnderageUserInfoForm(instance=uu)

    return {'form': form}


@login_required
@user_has_no_buck_with_slug('complete_first_step')
def complete_first_step(request):
    current_user = get_user(request)
    if get_config('INTAKE_IS_CLOSED', False):
        if not has_buck(current_user, 'locked_to_workshop'):
            messages.error(request, 'Набор закрыт.')
            return redirect('profile')

    # TODO: заменить на похожую штуку из intake.tools
    if attach_buck(
        current_user, 'complete_first_step', intention='Пользователь заполнил профиль на сайте'
    ):
        messages.success(request, 'Вы успешно заполнили анкету')
    else:
        messages.error(request, 'Вы уже выполняли этот шаг!')

    if has_buck(current_user, 'locked_to_workshop'):
        app = get_object_or_404(ApplicationCase, user=current_user)
        app.accept()
        attach_buck(app.user, 'passed_first_step', intention='[auto-pass]')
        app.mark_passed()
        app.save()

        messages.success(request, 'Ваша заявка передана в мастерскую.')

        if prepare_second_round(app):
            subject = 'ЛШ2020: вступительное задание'
            template = loader.get_template('intake/staff/emails/second_round.txt')
            text = template.render(Context({}))
            send_mail_to_applicant(app, subject, text)

            robot_comment_on_case(
                case=app,
                comment='У выбранной программы есть автоматически рассылаемое вступительно задание. Участнику отправлено уведомление.',  # noqa
            )
            messages.success(request, 'Уже можно заполнить вступительно задание')

    return redirect('profile')


@login_required
@user_has_no_buck_with_slug('complete_first_step')
@render_to('intake/upload_photo.html')
def upload_photo(request):
    current_photo = get_object_or_None(UserPhoto, user=get_user(request))
    if request.method == 'POST':
        form = PhotoUploadForm(request.POST, request.FILES)
        if form.is_valid():
            ph = form.cleaned_data.get('photo')
            if ph:
                uf, _ = UserPhoto.objects.get_or_create(user=get_user(request))
                uf.photo = ph
                uf.save()
                messages.success(request, 'Фотка обновлена')
                return redirect('profile')
    else:
        form = PhotoUploadForm()

    return {'form': form, 'current_photo': current_photo}
