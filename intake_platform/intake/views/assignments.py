import os
from wsgiref.util import FileWrapper

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic

from intake_platform.common.tools import get_user
from intake_platform.intake.forms import AssignmentResponseForm
from intake_platform.intake.models import ApplicationAssignmentResponse
from intake_platform.intake.tools import has_buck

SARAH_EMAIL = 'sarah.connor@letnyayashkola.org'


class AssignmentDownload(generic.View):
    def get_filename(self, w_slug, p_slug):
        return f'zadanie_2020_{w_slug}_{p_slug}'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = get_user(request)

        if not has_buck(user, 'complete_first_step'):
            return redirect('profile')

        wpc = user.workshoppetitionconnection
        hw = ApplicationAssignmentResponse.all_for_user(user, wpc.workshop_slug, wpc.program_slug)
        ass = None
        if len(hw) > 1:
            ass = None
        else:
            ass = hw[0] if len(hw) == 1 else None

        if ass is None:
            messages.error(
                request, f'Проблема с вступительным заданием. Свяжитесь с нами? {SARAH_EMAIL}'
            )
            return redirect('/profile/')

        ass = ass.assignment.assignment
        filepath = ass.path
        wrapper = FileWrapper(open(filepath, 'rb'))
        fname = self.get_filename(wpc.workshop_slug, wpc.program_slug)
        ext = os.path.splitext(filepath)[1]

        content_type = {
            '.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            '.zip': 'application/zip',
            '.pdf': 'application/pdf',
            '.xls': 'application/vnd.ms-excel',
            '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            '.doc': 'application/msword',
        }.get(ext, 'text/plain')

        response = HttpResponse(wrapper, content_type=content_type)
        response['Content-Disposition'] = f'attachment; filename={fname}{ext}'
        response['Content-Length'] = ass.size
        response['mimetype'] = content_type
        return response


class AssignmentMyResponseDownload(generic.View):
    def get_filename(self, w_slug, p_slug):
        return f'zadanie_moi_otvet_2020_{w_slug}_{p_slug}'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = get_user(request)

        if not has_buck(user, 'complete_first_step'):
            return redirect('profile')

        wpc = user.workshoppetitionconnection
        hw = ApplicationAssignmentResponse.all_for_user(user, wpc.workshop_slug, wpc.program_slug)
        ass = None
        if len(hw) > 1:
            ass = None
        else:
            ass = hw[0] if len(hw) == 1 else None

        if ass is None:
            messages.error(
                request,
                f'Проблема с вашим ответом на вступительное задание. Напишите нам? {SARAH_EMAIL}',
            )
            return redirect('/profile/')

        ass = ass.response
        filepath = ass.path
        wrapper = FileWrapper(open(filepath, 'rb'))
        fname = self.get_filename(wpc.workshop_slug, wpc.program_slug)
        ext = os.path.splitext(filepath)[1]

        content_type = {
            '.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            '.zip': 'application/zip',
            '.pdf': 'application/pdf',
            '.xls': 'application/vnd.ms-excel',
            '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            '.doc': 'application/msword',
        }.get(ext, 'text/plain')

        response = HttpResponse(wrapper, content_type=content_type)
        response['Content-Disposition'] = f'attachment; filename={fname}{ext}'
        response['Content-Length'] = ass.size
        response['mimetype'] = content_type

        return response


class AssignmentResponse(generic.View):
    form_class = AssignmentResponseForm
    template_name = 'intake/assignments/fill_in.html'
    ext_whitelist = ('.doc', '.docx', '.pdf', '.zip', '.xlsx', '.xls')

    def do_render(self, request, form, hw_obj):
        user = get_user(request)
        return render(
            request,
            self.template_name,
            {
                'form': form,
                'homework': hw_obj,
                'wpc': user.workshoppetitionconnection,
                'back_urls': [(reverse('intake:timeline'), 'Назад')],
            },
        )

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = get_user(request)
        wpc = user.workshoppetitionconnection
        hw = ApplicationAssignmentResponse.all_for_user(user, wpc.workshop_slug, wpc.program_slug)
        if len(hw) > 1:
            messages.error(
                request, f'O_o. Не могу достать ваше задание. Свяжитесь с нами? {SARAH_EMAIL}'
            )
            return redirect('profile')
        else:
            hw = hw[0]
        if hw.is_complete:
            return redirect('profile')

        return self.do_render(request, self.form_class(instance=hw), hw)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        user = get_user(request)
        wpc = user.workshoppetitionconnection
        hw = ApplicationAssignmentResponse.all_for_user(user, wpc.workshop_slug, wpc.program_slug)
        if len(hw) > 1:
            messages.error(
                request, f'O_o. Не могу достать ваше задание. Свяжитесь с нами? {SARAH_EMAIL}'
            )
            return redirect('profile')
        else:
            hw = hw[0]
        if hw.is_complete:
            return redirect('profile')

        form = self.form_class(request.POST, request.FILES, instance=hw)
        if form.is_valid():
            data = form.cleaned_data['response']
            filename = data.name
            ext = os.path.splitext(filename)[1]
            ext = ext.lower()
            if ext not in self.ext_whitelist:
                whitelist_str = ', '.join(self.ext_whitelist)
                messages.error(
                    request, f'Запрещённый тип документа! Только {whitelist_str}. Никаких {ext}'
                )
            else:
                hw = form.save()
                hw.is_complete = True
                hw.save()
                messages.success(request, 'Задание загружено!')
                return redirect('profile')

        return self.do_render(request, form, hw)
