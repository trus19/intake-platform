from django.shortcuts import render
from django.views import generic

from intake_platform.intake.tools import create_locked_wpc
from intake_platform.intake_platform.forms import FancySignupForm, default_signup_fields


class SecretSignUp(generic.View):
    template_name = 'intake/secret-signup.html'
    form_class = FancySignupForm

    def get_locked_wp_tuple(self):
        '''
        define something that returns wp tuple
        like ('organizer', 'rhetoric') or similar
        '''
        raise NotImplementedError

    def get_locked_wp_html_header(self):
        '''
        define something that returns string for this thing
        '''
        raise NotImplementedError

    def get_locked_wp_html_middle_meat(self):
        ''' define str for this explanatory part '''
        raise NotImplementedError

    def get_locked_wp_html_tail(self):
        ''' define str for this tail thing part '''
        return 'Зарегистрироваться на хитрый модуль'

    def do_render(self, request, form):
        return render(request, self.template_name, {'form': form})

    @property
    def order_of_actions(self):
        return 'Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот'

    def get_header(self):
        from crispy_forms.layout import Layout, HTML, Div

        locked_w_slug, locked_p_slug = self.get_locked_wp_tuple()
        header = self.get_locked_wp_html_header()
        middle_meat = self.get_locked_wp_html_middle_meat()
        return Layout(
            Div(
                HTML(f'<h1 class="panel-title">{header}</h1>'),
                css_class='panel-heading text-center',
            ),
            HTML(
                f'''
                <p class='text-center'>Да. Это тут.&nbsp;
                <span class='text-muted'>Уже регистрировались?&nbsp;
                <a href='/accounts/login/'>Войдите</a>
                </span>
                </p>
                <p class='text-center'>{middle_meat}<br/>
                <b>Без мук выбора.</b>
                </p>
                <p class='text-center'>
                Заполняете остальную часть анкеты, отправляете,
                попадаете на рассмотрение кураторам.
                </p>
                '''
            ),
        )

    def get_tail(self):
        from crispy_forms.layout import Layout, Submit
        from crispy_forms.bootstrap import FormActions

        tail = self.get_locked_wp_html_tail()
        return Layout(FormActions(Submit('submit', tail, css_class='btn btn-primary btn-block')))

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        from crispy_forms.layout import Layout, Div
        from crispy_forms.helper import FormHelper

        helper = FormHelper()
        helper.layout = Layout(
            self.get_header(),
            Div(default_signup_fields, self.get_tail(), css_class='panel-body signup'),
        )
        helper.help_text_inline = True
        helper.form_style = 'inline'
        form.helper = helper

        return self.do_render(request, form)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        locked_w_slug, locked_p_slug = self.get_locked_wp_tuple()
        if form.is_valid():
            new_u = form.save(request)
            create_locked_wpc(
                new_u,
                w_slug=locked_w_slug,
                p_slug=locked_p_slug,
                motivation='[вот тут надо что-то вменяемое написать]',
            )
            # FIXME: убрать эту логику вообще

        return self.do_render(request, form)


class SecretRozetkaSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('obrazovanie', 'rozetka')

    def get_locked_wp_html_header(self):
        return 'Это тут у вас можно зарегистрироваться на модуль &laquo;Розетка&raquo;?'

    def get_locked_wp_html_middle_meat(self):
        return ', '.join(
            [
                f'{self.order_of_actions} мастерской &laquo;Образования&raquo;',
                'именно к модулю &laquo;Розетка&raquo;.',
            ]
        )


class SecretNeudobnoSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('obrazovanie', 'neudobno')

    def get_locked_wp_html_header(self):
        return 'Это тут у вас можно зарегистрироваться на модуль &laquo;Неудобный разговор&raquo;?'

    def get_locked_wp_html_middle_meat(self):
        return ', '.join(
            [
                f'{self.order_of_actions} мастерской &laquo;Образования&raquo;',
                'именно к модулю &laquo;Неудобный разговор&raquo;.',
            ]
        )


class SecretInclusioSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('obrazovanie', 'inclusio')

    def get_locked_wp_html_header(self):
        return 'Это тут у вас можно зарегистрироваться на модуль &laquo;Инклюзия&raquo;?'

    def get_locked_wp_html_middle_meat(self):
        return ', '.join(
            [
                f'{self.order_of_actions} мастерской &laquo;Образования&raquo;',
                'именно к модулю &laquo;Инклюзия&raquo;.',
            ]
        )


class SecretObrZhurSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('obrazovanie', 'zhurnalistika')

    def get_locked_wp_html_header(self):
        return 'Это тут у вас можно зарегистрироваться на модуль &laquo;Образовательная журналистика&raquo;?'  # noqa

    def get_locked_wp_html_middle_meat(self):
        return ', '.join(
            [
                f'{self.order_of_actions} мастерской &laquo;Образования&raquo;',
                'именно к модулю &laquo;Образовательная журналистика&raquo;.',
            ]
        )


class SecretHardOnGeo(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('scientificskills', 'geo')

    def get_locked_wp_html_header(self):
        return 'Это тут у вас можно зарегистрироваться на гео-трек?'

    def get_locked_wp_html_middle_meat(self):
        ws_name = '&laquo;Hard&amp;Soft&nbsp;scientific&nbsp;skills&raquo;'
        return ', '.join([f'{self.order_of_actions} мастерской {ws_name}', 'именно к геотреку.'])


class Secret105SignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('105element', 'main')

    def get_locked_wp_html_header(self):
        return 'Это тут у вас можно зарегистрироваться на супер крутую мастерскую физики &laquo;105 элемент&raquo;?'  # noqa

    def get_locked_wp_html_middle_meat(self):
        return f'{self.order_of_actions} мастерской &laquo;105 элемент&raquo;'


class SecretSciPubNauSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('sci-pub', 'nauchkom')

    def get_locked_wp_html_header(self):
        return 'Это тут у вас регистрируют на &laquo;Научные коммунакции&raquo;?'

    def get_locked_wp_html_middle_meat(self):
        return f'{self.order_of_actions} &laquo;ШНЖ&raquo; (направление научных коммуникаций).'


class SecretLawSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('itip-law', 'main')

    def get_locked_wp_html_header(self):
        return 'Это тут у вас регистрируют на &laquo;IT&IP law jam&raquo;?'

    def get_locked_wp_html_middle_meat(self):
        return f'{self.order_of_actions} &laquo;IT&IP law jam&raquo;.'


class SecretFreestyleSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('zhivoyteatr', 'freestyle')

    def get_locked_wp_html_header(self):
        return 'Это тут у вас регистрируют на &laquo;Фристайл&raquo;?'

    def get_locked_wp_html_middle_meat(self):
        return f'{self.order_of_actions} мастерской &laquo;Живой театр&raquo; на направление freestyle.'  # noqa


class SecretFRCSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('frc', 'main')

    def get_locked_wp_html_header(self):
        return 'Это вот это секретная ссылка как попасть на ЦПИ?'

    def get_locked_wp_html_middle_meat(self):
        return 'Порядок действий простой. Вы заполняете анкету и сразу же прикрепляетесь к ЦПИ и без вариантов и прочих мук выбора. Как заполните анкету, её увидят соответствующие кураторы.'  # noqa


class SecretGenericSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ('generic', 'main')

    def get_locked_wp_html_header(self):
        return 'Это вот это секретная ссылка как пройти набор когда он закрыт?'

    def get_locked_wp_html_middle_meat(self):
        return 'Порядок действий простой. Вы заполняете анкету, и пишете человеку с которым вы договорились чтобы вас привязали к нужному направлению. И только после этого вас увидят среди заявок выбранного направления/мастерской.'  # noqa
