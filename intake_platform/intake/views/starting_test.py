from annoying.decorators import render_to

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from intake_platform.common.tools import get_user
from intake_platform.intake.decorators import user_has_no_buck_with_slug
from intake_platform.intake.forms import UserNotDumbForm
from intake_platform.intake.tools import attach_buck


@login_required
@user_has_no_buck_with_slug('passed_idiot_test')
@render_to('intake/check_user_not_dumb_prequel.html')
def check_user_not_dumb_prequel(request):
    return {}


@login_required
@user_has_no_buck_with_slug('passed_idiot_test')
@render_to('intake/check_user_not_dumb_actual_test.html')
def check_user_not_dumb_actual_test(request):
    user = get_user(request)

    if request.method == 'POST':
        form = UserNotDumbForm(request.POST)
        if form.is_valid():
            attach_buck(user, 'passed_idiot_test')
            messages.success(request, 'Тест пройден, всё верно!')
            return redirect('profile')
        else:
            messages.error(request, 'В тесте были неправильные ответы. Попробуем ещё раз')
    else:
        form = UserNotDumbForm()

    return {'form': form}
