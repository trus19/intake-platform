# coding: utf-8

from datetime import datetime

from annoying.decorators import ajax_request
from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import html
from django.utils.decorators import method_decorator
from django.views import generic

from intake_platform.common.tools import get_user
from intake_platform.intake.forms import PhotoReplaceForm, WPCMotivationForm
from intake_platform.intake.models import (
    AppEmail,
    ApplicationCase,
    User,
    UserActivityInfo,
    UserAdditionalContacts,
    UserPhoto,
    WorkshopPetitionConnection,
)
from intake_platform.intake.tools import (
    get_wp_slugs_and_names,
    robot_comment_on_case,
    user_1st_step_info,
)


def try_fetching_old_data(sender, **kwargs):
    # assuming that we have defined 'data_2015' as database
    last_year_db = 'data_%s' % (datetime.today().year - 1)

    if last_year_db in settings.DATABASES:
        user = kwargs.pop('user')
        old_user = get_object_or_None(User.objects.using(last_year_db), email=user.email)
        if old_user is None:
            return

        old_data = user_1st_step_info(old_user, db_tag=last_year_db)

        related_cls = {
            # 'WorkshopPetitionConnection': WorkshopPetitionConnection,
            'UserAdditionalContacts': UserAdditionalContacts,
            # 'UserEducationInfo': UserEducationInfo,
            'UserActivityInfo': UserActivityInfo,
            # 'UserHandymanshipInfo': UserHandymanshipInfo,
            # 'GiveBackInfo': GiveBackInfo,
            # 'UserLogisticsInfo': UserLogisticsInfo,
            # 'PastFuturePresentInfo': PastFuturePresentInfo,
            'UserPhoto': UserPhoto,
        }

        for cls_tag, obj in old_data['tuples']:
            if (obj is None) or (cls_tag not in related_cls):
                continue
            keyword_args = obj.to_dict()
            keyword_args['user'] = user
            new_instance = related_cls[cls_tag](**keyword_args)
            new_instance.save()


class ReplacePhoto(generic.View):
    form_class = PhotoReplaceForm

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        # FIXME: тут надо подумать, нет ли опасности залить чужое фото.
        user = get_user(request)
        photo = get_object_or_404(UserPhoto, user=user)
        if photo.needs_to_be_changed is False:
            messages.error(request, 'Не надо так.')
            return redirect('profile')

        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            photo.photo = form.cleaned_data.get('photo')
            photo.needs_to_be_changed = False
            photo.save()
            messages.success(request, 'Фотка обновлена, спасибо.')

            app = get_object_or_None(ApplicationCase, user=user)
            if app is not None:
                robot_comment_on_case(case=app, comment='Участник заменил фото')
        else:
            messages.error(
                request,
                'Не могу сохранить фото. Вы не забыли приложить файл? Попробуйте закачать фото ещё раз',  # noqa
            )

        return redirect('profile')


@login_required
@ajax_request
def emails(request):
    emails = AppEmail.objects.filter(to_user=get_user(request)).values_list(
        'subject', 'text', 'relevant_buck__slug', 'date_created'
    )
    return [
        {'subject': subject, 'text': html.linebreaks(text), 'buck': buck, 'date': f'{date}'}
        for subject, text, buck, date in emails
    ]


class Messages(generic.View):
    template_name = 'intake/messages.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


@ajax_request
def workshop_programs(request):
    return [
        {
            'workshop_slug': w_slug,
            'program_slug': p_slug,
            'workshop_name': w_name,
            'program_name': p_name,
        }
        for w_slug, w_name, p_slug, p_name in get_wp_slugs_and_names()
    ]


class WPCMotivationEdit(generic.View):
    form_class = WPCMotivationForm

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        user = get_user(request)
        wpc = get_object_or_None(WorkshopPetitionConnection, user=user)
        form = self.form_class(request.POST, instance=wpc)
        if form.is_valid():
            # Get the motivation and update only it
            cd = form.cleaned_data
            wpc.wait_but_why = cd['wait_but_why']
            wpc.save()
        else:
            messages.error(request, 'Ошибка при заполнении мотивации. Try again')
        return redirect('profile')
