from .starting_test import (
    check_user_not_dumb_actual_test,
    check_user_not_dumb_prequel,
)

from .assignments import (
    AssignmentDownload,
    AssignmentMyResponseDownload,
    AssignmentResponse,
)

from .secret_signups import (
    Secret105SignUp,
    SecretFRCSignUp,
    SecretFreestyleSignUp,
    SecretGenericSignUp,
    SecretHardOnGeo,
    SecretInclusioSignUp,
    SecretLawSignUp,
    SecretNeudobnoSignUp,
    SecretObrZhurSignUp,
    SecretRozetkaSignUp,
    SecretSciPubNauSignUp,
    SecretSignUp,
)

from .waiting_views import (
    timeline,
    OwnAppReadOnly,
    ReadDeclineEmail,
)

from .first_step_views import (
    get_activity_info,
    collect_education_info,
    provide_basic_info,
    provide_contacts,
    wp_select,
    demand_handymanship,
    fill_in_money,
    promise_help,
    choose_my_way,
    retrospect,
    leakage,
    parents,
    complete_first_step,
    upload_photo,
)


from .to_refactor import (
    Messages,
    ReplacePhoto,
    WPCMotivationEdit,
    emails,
    workshop_programs,
)