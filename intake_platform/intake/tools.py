# coding: utf-8

import os
import random
import string
from datetime import datetime

from annoying.functions import get_object_or_None, get_config
from django.db.models import Q
from django.contrib.auth.models import User
from django.core.mail import send_mail, mail_admins

from .models import (
    ApplicationNewbie,
    WorkshopPetitionConnection,
    UserAdditionalContacts,
    UserEducationInfo,
    UserActivityInfo,
    UserHandymanshipInfo,
    GiveBackInfo,
    UserLogisticsInfo,
    PastFuturePresentInfo,
    UserPhoto,
    UnderageUserInfo,
    UserBuck,
    ApplicationCase,
    ApplicationCaseComment,
    UserPrepodInfo,
    WorkshopProgramMetaInfo,
    WorkshopProgramAssignment,
    ApplicationAssignmentResponse,
    UserCuratorInfo,
)


def create_application_from_wizard(form_list):
    a = ApplicationNewbie()

    for f in form_list:
        for t in f.fields:
            a.__dict__[t] = f.cleaned_data.get(t)

    a.save()

    return a


def first_step_completed(profile):
    u = profile.user
    related_cls = [
        WorkshopPetitionConnection,
        UserAdditionalContacts,
        UserEducationInfo,
        UserActivityInfo,
        # UserHandymanshipInfo, NOT REQUIRED
        GiveBackInfo,
        # UserLogisticsInfo,
        PastFuturePresentInfo,
        UserPhoto,
    ]
    if profile.is_underage:
        related_cls.append(UnderageUserInfo)

    nons = [get_object_or_None(c, user=u) for c in related_cls]

    return all([x is not None for x in nons])


STEP1_NICE_NAMESDICT = {
    # Formname: (NiceName, SlugName, Required)
    "WorkshopPetitionConnection": (u"Мастерская", "workshop", True),
    "UserAdditionalContacts": (u"Контакты", "contacts", True),
    "UserEducationInfo": (u"Образование", "edu", True),
    "UserActivityInfo": (u"Занятость", "occupation", True),
    "UserHandymanshipInfo": (u"Квартирьерство", "handy", False),
    "GiveBackInfo": (u"Помощь ЛШ (в том числе раздел про деньги)", "giveback", True),
    "UserLogisticsInfo": (u"Способ добраться до ЛШ", "logistics", True),
    "PastFuturePresentInfo": (u"Прошлые школы или источники", "nostalgia", True),
    "UserPhoto": (u"Фото", "photo", True),
    # unedrage fields
    "UnderageUserInfo": (u"Контакты родителей", "parents", True),
}

BUCK_SLUG_NAMESDICT = {
    "complete_first_step": u"Свежак",
    "passed_first_step": u"Одобрено",
    "declined_by_workshop": u"Мастерская отказалась",
    "been_suspicious": u"Сомнительные",
}

ALLOWED_MULTIBUCKS_SLUGS = [
    "declined_by_workshop",
    "assignment_auto_sent",
    "assignment_sent",
]


def user_1st_step_info(user, db_tag=None):
    related_cls = [
        ("WorkshopPetitionConnection", WorkshopPetitionConnection),
        ("UserAdditionalContacts", UserAdditionalContacts),
        ("UserEducationInfo", UserEducationInfo),
        ("UserActivityInfo", UserActivityInfo),
        # ('UserHandymanshipInfo', UserHandymanshipInfo),
        ("GiveBackInfo", GiveBackInfo),
        # ('UserLogisticsInfo', UserLogisticsInfo),
        ("PastFuturePresentInfo", PastFuturePresentInfo),
        ("UserPhoto", UserPhoto),
    ]
    if user.userprofile.is_underage:
        if db_tag is None:
            related_cls.append(("UnderageUserInfo", UnderageUserInfo)),

    if db_tag is None:
        db_tag = "default"

    info = {"completed": first_step_completed(user.userprofile), "tuples": []}
    for name, c in related_cls:
        obj = get_object_or_None(c.objects.using(db_tag), user=user)
        info["tuples"] += [(name, obj)]

    return info


def has_buck(user, buck_slug, is_latest=False):
    """ Checks if the user has the buck_slugged-buck.

        :returns: True if buck was found, False otherwise
        :rtype: bool

        :param user: User to query
        :param buck_slug: the buck_slug User to query
        :param is_latest(default=False): returns True iff this buck_slug is the latest

        If the buck is allowed to be multibuck, returns True is the user has at
        least one buck with this slug. If is_latest was also set, checks if the last buck is of the same type

    """
    # TODO: Refactor
    if buck_slug in ALLOWED_MULTIBUCKS_SLUGS:
        obj = UserBuck.objects.filter(user=user, slug=buck_slug)
        if len(obj) == 0:
            return False
        else:
            if is_latest is False:
                return True
            else:
                latest_b = get_latest_buck(user)
                return obj.latest("when") == latest_b
    else:
        obj = get_object_or_None(UserBuck, user=user, slug=buck_slug)
        if obj is not None:
            if is_latest is False:
                return True
            else:
                latest_b = get_latest_buck(user)
                return obj == latest_b
        else:
            return False


def get_latest_buck(user, buck_slug=None):
    q = Q(user=user)
    if buck_slug:
        q &= Q(slug=buck_slug)
    return UserBuck.objects.filter(q).latest("when")


def get_buck_nice_name(buck_slug):
    """ Returns nice name (in russian) for the slug.

        If no name found, returns just the slug
    """
    return BUCK_SLUG_NAMESDICT.get(buck_slug, buck_slug)


def has_buck_after_buck(user, lhs_buck_slug, rhs_buck_slug):
    """
        General usage:
            "If user has buck lhs_buck after rhs_buck than it's True"

        lhs_buck = userbuck with lhs_buck_slug
        rhs_buck = userbuck with rhs_buck_slug

        has lhs_buck and no rhs_buck -> True
        has lhs_buck and it's later than rhs_buck -> True
        has no lhs_buck -> False (don't even check for rhs)
        has lhs_buck and it's _before_ rhs_buck -> False

        :rtype: boolean

        :param user: User to query
        :param lhs_buck_slug: First buck slug to check
        :param rhs_buck_slug: Second buck slug to check
    """

    def latest_buck(user, slug):
        try:
            return UserBuck.objects.filter(user=user, slug=slug).latest("when")
        except UserBuck.DoesNotExist:
            return None

    l_b = latest_buck(user, lhs_buck_slug)
    r_b = latest_buck(user, rhs_buck_slug)

    if l_b is None:
        return False
    else:  # has lhs_buck
        if r_b is None:
            return True
        else:  # has lhs_buck and rhs_buck
            return l_b.when > r_b.when


def attach_buck(user, buck_slug, intention=None, get_buck=False):
    """ Навесить на пользователя метку со слагом 'buck_slug'.
        В случае успеха возвращает True.

        Если метка есть, возвращает False, кроме тех меток, которых by design
        может быть несколько.

        Если get_buck=True, возвращает созданную метку
    """
    if buck_slug in ALLOWED_MULTIBUCKS_SLUGS:
        buck = None
    else:
        # Вместо того, чтобы ловить здесь MultipleObjectReturned,
        # лучше добавить его выше в список разрешённых множественных меток
        buck = get_object_or_None(UserBuck, user=user, slug=buck_slug)

    # TODO: проверять, что за слаг и, при необходимости, открывать дело
    if buck is not None:  # User already has this buck
        return buck if get_buck else False
    else:
        buck = UserBuck(user=user, slug=buck_slug, intention=intention)
        buck.save()

        if buck_slug == "complete_first_step":
            # user has just completed first step. We need to open the AppCase
            case, created = ApplicationCase.objects.get_or_create(user=user)
            if created:
                case.created = buck.when
                case.last_modified = datetime.now()
                case.save()
            # FIXME: what if created == False?
        if buck_slug == "passed_first_step":
            pass

        if buck_slug == "assignment_auto_sent":
            # FIXME: лучше перенести сюда автосоздание объекта для ответа
            case = ApplicationCase.objects.get(user=user)

        return buck if get_buck else True


def detach_buck(user, buck_slug):
    """ Супер-вспомогательная функция, которая тупо убивает метку без следа.
        Если такая метка была и всё прошло успешно, возвращает True
    """

    buck = get_object_or_None(UserBuck, user=user, slug=buck_slug)
    if buck is not None:
        buck.delete()
        return True
    else:
        return False


def lick_buck(user, buck_slug):
    """ Update datestamp on buck.

        :returns: On success returns true, if no buck found, returns false
        :rtype: boolean
    """
    buck = get_object_or_None(UserBuck, user=user, slug=buck_slug)
    if buck is not None:
        buck.when = datetime.now()
    else:
        return False


def get_fresh_apps_for_nabor(users):
    """ return users whose latest buck is 'complete_first_step'

        this is super-ineffective database-wise
    """

    def f(x):
        first_step = has_buck(x, "complete_first_step")
        was_wdeclined = has_buck(x, "declined_by_workshop")
        return first_step and (not was_wdeclined)

    apps = filter(f, users)
    return apps


def filter_users_by_buck_slug(users, buck_slug):
    bucks = UserBuck.objects.filter(slug=buck_slug, user__in=users)
    return [b.user for b in bucks]


def comment_on_case(case, author, comment, commit=True):
    new_comment = ApplicationCaseComment(
        application=case, comment=comment, author=author
    )
    if commit:
        new_comment.save()

    return new_comment


def robot_comment_on_case(case, comment, commit=True):
    robot_username = get_config("ROBOT_USERNAME", "admin")
    robot = User.objects.get(username=robot_username)
    return comment_on_case(case, robot, comment, commit)


def set_curator(user, workshop, programs=None):
    programs = programs or []
    qs = WorkshopProgramMetaInfo.objects.filter(workshop_slug=workshop)

    if programs:
        qs = qs.filter(program_slug__in=programs)

    for w_meta in qs:
        w_meta.curators.add(user)
        w_meta.save()


def _get_wp_apps_filtered(w_slug, p_slug, filter_func):
    users = [
        p.user
        for p in WorkshopPetitionConnection.objects.filter(
            workshop_slug=w_slug, program_slug=p_slug
        )
    ]
    qs = ApplicationCase.objects.filter(
        is_closed=True, user__in=filter(filter_func, users)
    )
    return qs


def _get_workshop_apps_filtered(w_slug, filter_func):
    users = [
        p.user for p in WorkshopPetitionConnection.objects.filter(workshop_slug=w_slug)
    ]
    qs = ApplicationCase.objects.filter(
        is_closed=True, user__in=filter(filter_func, users)
    )
    return qs


def __first_step(x):
    return has_buck(x, "passed_first_step")


def __w_accepted(x):
    return has_buck(x, "accepted_by_workshop")


def __w_declined(x):
    return has_buck(x, "declined_by_workshop")


def __w_retry(x):
    return has_buck(x, "retry_granted", is_latest=True)


def __w_accepted_retry(x):
    return has_buck_after_buck(x, "accepted_by_workshop", "retry_granted")


def _passed(x):
    return __first_step(x) and not (__w_accepted(x) or __w_declined(x))


def _passed_or_retry(x):
    return (__first_step(x) and not (__w_accepted(x) or __w_declined(x))) or __w_retry(
        x
    )


def _accepted(x):
    return (__w_accepted(x) and not __w_declined(x)) or __w_accepted_retry(x)  # HUH?


def _declined(x):
    return __w_declined(x) and not __w_retry(x)


def _retry(x):
    return __w_retry(x)


def get_wp_passed_apps(w_slug, p_slug, order_by_created=True):
    qs = _get_wp_apps_filtered(w_slug, p_slug, _passed)
    if order_by_created:
        return qs.order_by("-created")

    return qs


def get_wp_retry_apps(w_slug, p_slug):
    return _get_wp_apps_filtered(w_slug, p_slug, _retry).order_by("-last_modified")


def get_wp_passed_or_retry_apps(w_slug, p_slug):
    return _get_wp_apps_filtered(w_slug, p_slug, _passed_or_retry).order_by(
        "-last_modified"
    )


def get_wp_accepted_apps(w_slug, p_slug):
    return _get_wp_apps_filtered(w_slug, p_slug, _accepted).order_by("-last_modified")


def get_workshop_accepted_apps(w_slug):
    return _get_workshop_apps_filtered(w_slug, _accepted).order_by("-last_modified")


def get_workshop_declined_apps(w_slug):
    return _get_workshop_apps_filtered(w_slug, _declined).order_by("-last_modified")


def get_wp_declined_apps(w_slug, p_slug):
    return _get_wp_apps_filtered(w_slug, p_slug, _declined).order_by("-last_modified")


def get_wp_apps_counts_dict(w_slug, p_slug=None):
    """ (w_slug, p_slug) -> {
            'total': <num>,
            'passed': <num>,
            'accepted': <num>,
            'declined': <num>,
            'retry': <num>
        }

        Using count() here should be more efficient than quering for the objects

        if p_slug is None, dumps everything for this workshop

    """
    if p_slug is None:
        users = [
            p.user
            for p in WorkshopPetitionConnection.objects.filter(workshop_slug=w_slug)
        ]
    else:
        users = [
            p.user
            for p in WorkshopPetitionConnection.objects.filter(
                workshop_slug=w_slug, program_slug=p_slug
            )
        ]
    total = ApplicationCase.objects.filter(is_closed=True, user__in=users).count()
    passed = ApplicationCase.objects.filter(
        is_closed=True, user__in=filter(_passed, users)
    ).count()
    accepted = ApplicationCase.objects.filter(
        is_closed=True, user__in=filter(_accepted, users)
    ).count()
    declined = ApplicationCase.objects.filter(
        is_closed=True, user__in=filter(_declined, users)
    ).count()
    retry = ApplicationCase.objects.filter(user__in=filter(_retry, users)).count()
    return {
        "total": total,
        "passed": passed,
        "accepted": accepted,
        "declined": declined,
        "retry": retry,
    }


def get_wp_prepod_count(w_slug, p_slug):
    return UserPrepodInfo.objects.filter(
        workshop_program__workshop_slug=w_slug, workshop_program__program_slug=p_slug
    ).count()


def get_wp_curator_count(w_slug, p_slug):
    return UserCuratorInfo.objects.filter(
        workshop_program__workshop_slug=w_slug, workshop_program__program_slug=p_slug
    ).count()


def get_wp_counts_dict(w_slug, p_slug):
    d = get_wp_apps_counts_dict(w_slug, p_slug)
    d["prepods"] = get_wp_prepod_count(w_slug, p_slug)
    d["curators"] = get_wp_curator_count(w_slug, p_slug)
    return d


def get_assignments_dict():
    """ Returns dict with all assignments
    '{{ workshop_slug }}/{{ program_slug}}' -> WorkshopProgramAssignment

    TODO: if multiple assignments found, return latest
    """

    return {
        "{}/{}".format(
            x.workshop_program.workshop_slug, x.workshop_program.program_slug
        ): x
        for x in WorkshopProgramAssignment.objects.filter(autosend=True)
    }


def prepare_second_round(app, intention=None, automatic=True):
    if app.datetime_workshop_notified:
        assignments = get_assignments_dict()
        wpc = app.user.workshoppetitionconnection
        program = "{}/{}".format(wpc.workshop_slug, wpc.program_slug)
        if program in assignments:
            hw = assignments[program]

            resp, created = ApplicationAssignmentResponse.objects.get_or_create(
                assignment=hw, user=app.user
            )
            if created:
                if intention is None:
                    intention = u"intake.tools.prepare_second_round"
                attach_buck(
                    app.user,
                    "assignment_auto_sent" if automatic else "assignment_sent",
                    intention=intention,
                )
                return True

    return False


def send_mail_to_applicant(app, subject, text):
    """ Send an email to application.

    :param app: ApplicationCase object
    :param subject: email subject
    :param text: email text

    :returns bool: True if the message was successfully send, False otherwise
    """

    from_email = get_config("DEFAULT_FROM_EMAIL", "no-reply@letnyayashkola.ru")
    return send_mail(subject, text, from_email, [app.user.email]) == 1


def workshop_assignments_for_curator(user):
    if not user.userprofile.is_curator:
        return []

    assignments = []
    w_metas = user.workshopprogrammetainfo_set.all()
    for w_p in w_metas:
        hw = WorkshopProgramAssignment.objects.filter(
            workshop_program__workshop_slug=w_p.workshop_slug,
            workshop_program__program_slug=w_p.program_slug,
        )
        if len(hw) == 0:
            hw = None
        elif len(hw) > 1:
            assignments.append(hw.latest("date_uploaded"))
        elif len(hw) == 1:
            assignments.append(hw[0])
    return assignments


def workshops_dict(db_tag="default"):
    """ Returns a dict with workshops {slug: workshop_name}
        See also `intake.tools.programs_dict`

        :param db_tag:	database tag to use (optional)
        :returns:       dict {slug: workshop_name} for all workshops
    """
    d = {
        w["workshop_slug"]: w["workshop_name"]
        for w in WorkshopProgramMetaInfo.objects.using(db_tag)
        .values("workshop_slug", "workshop_name")
        .distinct()
    }
    return d


def programs_dict(db_tag="default"):
    """ Returns a dict with workshop programs {slug: program_name}
        See also `intake.tools.workshops_dict`

        :param db_tag:	database tag to use (optional)
        :returns:       dict {slug: program_name} for all programs
    """
    d = {}
    for w in WorkshopProgramMetaInfo.objects.using(db_tag).all():
        if w.program_slug in d:
            continue
        else:
            d[w.program_slug] = w.program_name
    return d


def get_workshop_programs(w_slug=None, lean=False):
    if w_slug is None:
        if lean:
            return WorkshopProgramMetaInfo.objects.all().values_list(
                "workshop_slug", "workshop_name", "program_slug", "program_name"
            )
        else:
            return WorkshopProgramMetaInfo.objects.all()
    else:
        if lean:
            return WorkshopProgramMetaInfo.objects.filter(
                workshop_slug=w_slug
            ).values_list(
                "workshop_slug", "workshop_name", "program_slug", "program_name"
            )
        else:
            return WorkshopProgramMetaInfo.objects.filter(workshop_slug=w_slug)


def get_wp_slugs_and_names():
    return get_workshop_programs(None, lean=True)


def get_wp_curators(w_slug, p_slug):
    return UserCuratorInfo.objects.filter(
        workshop_program__workshop_slug=w_slug, workshop_program__program_slug=p_slug
    )


def create_locked_wpc(user, w_slug, p_slug, motivation):
    """ Create an wpc and a buck with it """
    wpc = WorkshopPetitionConnection(
        user=user, workshop_slug=w_slug, program_slug=p_slug, wait_but_why=motivation
    )
    wpc.save()
    if not attach_buck(user, "locked_to_workshop"):
        mail_admins(
            u"Creating buck for locked wpc failed",
            u"And it's probably should not have happened. User_pk: %s and wpc_pk: %s"
            % (user.pk, wpc.pk),
        )
    return wpc


def get_wp_prepods(w_slug, p_slug):
    return UserPrepodInfo.objects.filter(
        workshop_program__workshop_slug=w_slug, workshop_program__program_slug=p_slug
    )


def generate_random_password(length=13, chars=None):
    if chars is None:
        chars = string.ascii_letters + string.digits
    random.seed = os.urandom(1024)

    return "".join(random.choice(chars) for i in range(length))


def check_user_jailbait(userprofile, workshop_slug, program_slug):
    if not userprofile.is_underage:
        wp_metainfo = (
            WorkshopProgramMetaInfo
            .objects
            .filter(
                workshop_slug=workshop_slug,
                program_slug=program_slug,
            )
            .first()
        )
        if wp_metainfo is None:
            return False
        return wp_metainfo.is_underage_friendly

    return False
