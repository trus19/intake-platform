# coding: utf-8

from collections import Counter
from annoying.decorators import ajax_request

from intake_platform.intake.models import UserBuck


@ajax_request
def bucks_by_date(request):
    slugs = ('complete_first_step', 'passed_first_step')
    counts = {}
    for slug in slugs:
        dates = []
        for buck in UserBuck.objects.filter(slug=slug):
            dates.append(buck.when.strftime('%Y-%m-%d'))
        counts[slug] = {
            date: count for date, count in Counter(dates).items()
        }
    return counts
