# coding: utf-8

import json

from django.core.management.base import BaseCommand

from intake.models import UserBuck

DB_USING = "data_2017"
FILENAME = "accepted_apps_pk_%s.JSON" % DB_USING


class Command(BaseCommand):
    help = "Show short info on all wp accepted apps known"

    def handle(self, *args, **options):
        pks = [
            x
            for x in set(
                [
                    x.user.pk
                    for x in UserBuck.objects.using(DB_USING).filter(
                        slug="accepted_by_workshop"
                    )
                ]
            )
        ]

        with open(FILENAME, "w") as fp:
            json.dump(pks, fp)
        print(("Saved %s entries in %s" % (len(pks), FILENAME)))
