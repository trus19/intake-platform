# coding: utf-8

import tqdm

from django.core.management.base import BaseCommand

from intake.models import (
    WorkshopProgramMetaInfo,
    WorkshopPetitionConnection,
    ApplicationCase,
)
from intake.tools import get_workshop_accepted_apps


OUTPUT_FILE = "/tmp/accepted_apps.txt"


class Command(BaseCommand):
    help = "Dumps all emails of all accepted apps"

    def handle(self, *args, **options):
        wp_slugs = set()

        with open(OUTPUT_FILE, "w") as f:
            for wp in tqdm.tqdm(WorkshopProgramMetaInfo.objects.all()):
                if wp.workshop_slug in wp_slugs:
                    continue
                else:
                    for app in get_workshop_accepted_apps(wp.workshop_slug):
                        print(app.user.email, file=f)
                    wp_slugs.add(wp.workshop_slug)

        print("Output saved as %s" % OUTPUT_FILE)
