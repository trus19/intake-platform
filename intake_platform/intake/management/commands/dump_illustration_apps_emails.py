# coding: utf-8

from django.core.management.base import BaseCommand

from intake.models import WorkshopProgramMetaInfo, WorkshopPetitionConnection
from intake.tools import get_wp_accepted_apps


class Command(BaseCommand):
    help = "Dumps all illustration apps of all accepted apps"

    def handle(self, *args, **options):
        for wp in WorkshopProgramMetaInfo.objects.filter(workshop_slug="illustration"):
            users = [
                p.user
                for p in WorkshopPetitionConnection.objects.filter(
                    workshop_slug=wp.workshop_slug, program_slug=wp.program_slug
                )
            ]
            for u in users:
                print('"%s %s" <%s>' % (u.first_name, u.last_name, u.email))
