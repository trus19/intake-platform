# coding: utf-8

from django.core.management.base import BaseCommand
from django.conf import settings

from intake.models import UserEducationInfo, WorkshopProgramMetaInfo
from intake.tools import get_wp_accepted_apps
from common.utils import UnicodeWriter

FILENAME = "dura.csv"


class Command(BaseCommand):
    help = "Show short info on all wp accepted apps known"

    def handle(self, *args, **options):
        lsh_dt_start_tuple = settings.LSH_DATES_DICT["start"]

        print("DUMPING DATA TO %s" % FILENAME)

        with open(FILENAME, "w") as fp:
            csvwriter = UnicodeWriter(fp)
            for wp in WorkshopProgramMetaInfo.objects.all():
                for app in get_wp_accepted_apps(wp.workshop_slug, wp.program_slug):
                    u = app.user
                    p = u.userprofile
                    ued = UserEducationInfo.objects.get(user=u)
                    # degree = ued.get_degree_ed_display() or u"(нет)",
                    # print u",".join([
                    csvwriter.writerow(
                        [
                            "%s" % x
                            for x in (
                                u.last_name,
                                u.first_name,
                                p.middle_name,
                                p.age_on_date(lsh_dt_start_tuple),
                                p.birthdate,
                                wp.workshop_name,
                                wp.program_name,
                            )
                            # ued.get_finished_ed_display(),
                            # degree)
                        ]
                    )
