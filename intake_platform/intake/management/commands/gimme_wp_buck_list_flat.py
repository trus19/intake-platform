# coding: utf-8


from collections import defaultdict
import datetime

from django.core.management.base import BaseCommand

from intake.models import WorkshopPetitionConnection, UserBuck
from intake.tools import workshops_dict, get_workshop_programs, has_buck


FULL_FORM = False


def t(dt):
    epoch = dt.replace(year=1970, month=1, day=1, hour=0, minute=0, second=0)
    return int((dt - epoch).total_seconds())


class Command(BaseCommand):
    help = "Print list of bucks in 'clean' form"
    OUTPUT = "/tmp/blyat.tsv"

    def handle(self, *args, **options):
        count = 0
        with open(self.OUTPUT, "w") as fp:
            for w_slug, w_name in workshops_dict().items():
                wplist = get_workshop_programs(w_slug=w_slug, lean=True)
                for _, _, p_slug, p_name in wplist:
                    wp_users = [
                        p.user
                        for p in WorkshopPetitionConnection.objects.filter(
                            workshop_slug=w_slug, program_slug=p_slug
                        )
                    ]
                    for u in wp_users:
                        wpc = WorkshopPetitionConnection.objects.get(user=u)
                        bucks = UserBuck.objects.filter(user=u)
                        slugs = UserBuck.objects.values_list("slug", flat=True).filter(
                            user=u
                        )
                        info = []
                        if "passed_first_step" in slugs:
                            for b in UserBuck.objects.filter(user=u):
                                if FULL_FORM:
                                    info.append(
                                        (
                                            u.pk,
                                            t(b.when),
                                            b.slug,
                                            b.intention or "",
                                            wpc.workshop_slug,
                                            wpc.program_slug,
                                        )
                                    )
                                else:
                                    info.append((u.pk, t(b.when), b.slug))
                                count += 1
                        for i in sorted(info, key=lambda x: x[1]):
                            print(i[0], i[1], i[2], file=fp)
        print("Saved %s entries to %s" % (count, self.OUTPUT))
