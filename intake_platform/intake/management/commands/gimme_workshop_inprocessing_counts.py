from collections import Counter, defaultdict

from django.core.management.base import BaseCommand

from intake_platform.intake.tools import (
    get_workshop_programs,
    get_wp_passed_apps,
    workshops_dict,
)


class Command(BaseCommand):
    help = 'Show info about passed apps counts on each workshop'  # noqa

    def handle(self, *args, **options) -> None:
        counts = defaultdict(int)
        ws_dict = workshops_dict()
        for w_slug, w_name in ws_dict.items():
            wplist = get_workshop_programs(w_slug=w_slug, lean=True)
            for _, _, p_slug, p_name in wplist:
                counts[w_slug] += len(get_wp_passed_apps(w_slug, p_slug))

        mensaje = 'Пятёрка тех, у кого сейчас больше всего в процессе\n'
        mensaje += ', \n'.join(
            ['%s (%s)' % (ws_dict[w], c) for w, c in Counter(counts).most_common(5)]
        )
