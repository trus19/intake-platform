# coding: utf-8

from django.core.management.base import BaseCommand

from intake_platform.intake.models import WorkshopProgramMetaInfo as WPMetaInfo
from intake_platform.common.tools import get_workshops_info


class Command(BaseCommand):
    help = 'Create or update workshop programs'

    def handle(self, *args, **options):
        wshops = get_workshops_info()
        counter = 0
        for i, w in enumerate(wshops):
            w_slug = w['slug']
            for j, p in enumerate(w['programs']):
                p_slug = p['slug']
                wp_meta, created = WPMetaInfo.objects.get_or_create(
                    workshop_slug=w_slug, program_slug=p_slug
                )

                action = 'Creating' if created else 'Updating'
                print(
                    '#%s.%s %s %s/%s description'
                    % (i + 1, j + 1, action, w_slug, p_slug)
                )

                wp_meta.workshop_name = w['name']
                wp_meta.program_name = p['name']
                wp_meta.is_underage_friendly = p['accepts_underage']
                wp_meta.save()
                counter += 1

        print('total workshop programs created/updated:', counter)
