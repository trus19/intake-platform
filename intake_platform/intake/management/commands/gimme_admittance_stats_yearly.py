# coding: utf-8

from django.core.management.base import BaseCommand
from intake.models import WorkshopPetitionConnection
from intake.tools import get_wp_apps_counts_dict, has_buck, workshops_dict


def process_wdict(d):
    print("slug;name;accepted;total")
    for w_slug, w_name in d.items():
        wpcs = WorkshopPetitionConnection.objects.filter(workshop_slug=w_slug)
        passed_cnt = sum([has_buck(c.user, "passed_first_step") for c in wpcs])
        if passed_cnt == 0:
            continue
        b_d = get_wp_apps_counts_dict(w_slug)
        tot = b_d["total"]
        acpt = b_d["accepted"]
        print("%s;%s;%s;%s" % (w_slug, w_name, acpt, tot))


class Command(BaseCommand):
    help = u"Print available admittance info per year"

    def handle(self, *args, **options):
        hardest_pass = {"slug": "", "transition_coefficient": 1}
        db_tags = ["data_2015", "data_2016", "data_2017", "data_2018", "default"]
        for tag in db_tags:
            print("=====================")
            print("Info on %s" % (2019 if tag.startswith("default") else tag))
            print("=====================")
            ws_dict = workshops_dict(tag)  # FIXME: use corresponding db!!
            process_wdict(ws_dict)
