# coding: utf-8

from collections import defaultdict

from django.core.management.base import BaseCommand

from intake.models import WorkshopPetitionConnection, UserBuck
from intake.tools import workshops_dict, get_workshop_programs, has_buck


class Command(BaseCommand):
    help = "Show various info and stats"
    IMPORTANT_LOCATION_THRESHOLD = 10

    def handle(self, *args, **options):
        distinct_slugs = UserBuck.objects.values_list("slug", flat=True).distinct()

        for w_slug, w_name in workshops_dict().items():
            print("[%s]" % w_slug)
            wplist = get_workshop_programs(w_slug=w_slug, lean=True)
            for _, _, p_slug, p_name in wplist:
                print("  %s" % p_slug)
                wp_users = [
                    p.user
                    for p in WorkshopPetitionConnection.objects.filter(
                        workshop_slug=w_slug, program_slug=p_slug
                    )
                ]
                counts = defaultdict(list)
                # for b in distinct_slugs:
                #     counts[b] = len(filter(
                #         lambda x: has_buck(x, b),
                #         wp_users
                #     ))
                counts["passed_first_step"] = len(
                    [x for x in wp_users if has_buck(x, "passed_first_step")]
                )
                counts["doehali"] = len(
                    [
                        x
                        for x in wp_users
                        if (
                            has_buck(x, "accepted_by_workshop")
                            and (not has_buck(x, "son_of_a_bitch"))
                        )
                    ]
                )
                for k, v in counts.items():
                    print("    %s x '%s'" % (v, k))

        # pprint.pprint(deg_stats)
