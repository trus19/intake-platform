from django.core.management.base import BaseCommand

from intake_platform.intake.models import UserBuck
from intake_platform.intake.tools import check_user_jailbait


THRESHOLD_BUCK_SLUG = 'complete_first_step'


class Command(BaseCommand):
    help = 'Update jailbait statuses for all users'

    def handle(self, *args, **options):
        users = [
            buck.user for buck in UserBuck.objects.filter(slug=THRESHOLD_BUCK_SLUG)
        ]

        fixes = 0
        for user in users:
            w_slug, p_slug = user.applicationcase.wp_slug_tuple
            wpc = user.workshoppetitionconnection

            old_jailbait = wpc.is_jailbait
            new_jailbait = check_user_jailbait(user.userprofile, w_slug, p_slug)

            if old_jailbait != new_jailbait:
                wpc.is_jailbait = new_jailbait
                wpc.save()
                fixes += 1

        print('total jailbait fixes:', fixes)

