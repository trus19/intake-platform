from collections import Counter, defaultdict
from typing import Union

from annoying.functions import get_config
from django.core.management.base import BaseCommand
import requests

from intake_platform.intake.tools import (
    workshops_dict,
    get_workshop_programs,
    get_wp_passed_apps,
    get_wp_accepted_apps,
    get_wp_declined_apps,
    has_buck_after_buck,
)
from intake_platform.intake.models import UserBuck, WorkshopPetitionConnection


# LSH_ORGS_CHAT_ID = -1001141542030
LSH_MAIN_CHAT_ID = -1001146790095  # -108626978
CRAMUR_ID = 50921357
LSH_DRCTRT_CHAT_ID = -1001346543137
TOKEN = get_config('TELEGRAM_BOT_TOKEN')


class Bot:
    BASE_URL = 'https://api.telegram.org'

    def __init__(self, token: str) -> None:
        self.token = token
        self.base_url = f'{self.BASE_URL}/bot{token}'

    def sendMessage(
        self,
        chat_id: Union[str, int],
        text: str,
        parse_mode: str = 'Markdown',
        disable_notification: bool = True,
    ) -> None:
        payload = {
            'chat_id': chat_id,
            'text': text,
            'parse_mode': parse_mode,
            'disable_notification': disable_notification,
        }
        r = requests.post(f'{self.base_url}/sendMessage', json=payload)
        return r.json()


# Uncomment the line below for debug
# LSH_MAIN_CHAT_ID = CRAMUR_ID
LSH_MAIN_CHAT_ID = LSH_DRCTRT_CHAT_ID = CRAMUR_ID


def get_has_ass_count(apps_list):
    non_slackers = [x for x in apps_list if x.has_complete_homework()]
    return len([_ for _ in non_slackers])


class Command(BaseCommand):
    help = 'Pings orgs chat some counters'

    def handle(self, *args, **options):
        total_apps = UserBuck.objects.filter(slug='complete_first_step').count()
        passed_nabor = UserBuck.objects.filter(slug='passed_first_step').count()
        declined_nabor = UserBuck.objects.filter(slug='nabor_rejected').count()
        accepted_ws = UserBuck.objects.filter(slug='accepted_by_workshop').count()
        declined_ws = UserBuck.objects.filter(slug='declined_by_workshop').count()

        accepted_females = [b.user for b in UserBuck.objects.filter(slug='accepted_by_workshop')]
        accepted_females = len([x for x in accepted_females if x.userprofile.is_male == False])

        females_perc = 0 if accepted_females == 0 else (accepted_females) / accepted_ws * 100

        txt_bucks = '''`Инфа по заявкам ЛШ-2019`
Всего анкет: *%s*
Прошло через Набор: *%s* (Отказано: %s)''' % (
            total_apps,
            passed_nabor,
            declined_nabor,
        )
        txt_bucks += '''
Сейчас железно приняты мастерской: *%s* (из них %s девочек, %.2g%%)
Отвергнуты мастерской: %s''' % (
            accepted_ws,
            accepted_females,
            females_perc,
            declined_ws,
        )

        counts = defaultdict(int)
        passed_counts = defaultdict(int)

        ws_dict = workshops_dict()
        for w_slug, w_name in list(ws_dict.items()):
            wplist = get_workshop_programs(w_slug=w_slug, lean=True)
            for _, _, p_slug, p_name in wplist:
                wp_users = [
                    p.user
                    for p in WorkshopPetitionConnection.objects.filter(
                        workshop_slug=w_slug, program_slug=p_slug
                    )
                ]
                passed_counts[w_slug] += len(get_wp_passed_apps(w_slug, p_slug))
                passed = get_wp_passed_apps(w_slug, p_slug)
                counts[w_slug + '-passed'] += len(passed)
                counts[w_slug + '-has-ass'] += get_has_ass_count(passed)
                counts[w_slug + '-wp-accepted'] += len(get_wp_accepted_apps(w_slug, p_slug))
                counts[w_slug + '-wp-declined'] += len(get_wp_declined_apps(w_slug, p_slug))
                counts[w_slug + '-cunts'] += sum(
                    has_buck_after_buck(x, 'son_of_a_bitch', 'accepted_by_workshop')
                    for x in wp_users
                )

        mensaje = (
            '`Заявки по мастерским`\n`(N_процесс[:N_прислали_задание] + N_принято + N_отказ)`\n\n'
        )
        mensaje += ', \n'.join(
            [
                '  %s: %s[:%s] + *%s* + %s'
                % (
                    w,  # ws_dict[w],
                    counts[w + '-passed'],
                    counts[w + '-has-ass'],
                    counts[w + '-wp-accepted'],
                    counts[w + '-wp-declined'],
                )
                for w, c in sorted(
                    [t for t in iter(list(Counter(passed_counts).items())) if t[1] > 0],
                    key=lambda x: x[1],
                    reverse=True,
                )
            ]
        )
        c_iter = iter(list(Counter(passed_counts).items()))

        def tell_perc(w):
            acc = counts[w + '-wp-accepted']
            dec = counts[w + '-wp-declined']
            if (acc + dec) == 0:
                return 0
            return 100 * acc / (acc + dec)

        def tell_sonofabitchiness(w):
            acc = counts[w + '-wp-accepted']
            cunts = counts[w + '-cunts']
            if acc == 0:
                return 0
            return 100 * (acc - cunts) / acc

        mensaje += '\n`---------`\nА эти всех разобрали:\n\n'
        # mensaje += u', \n'.join(
        #     [
        #         u'  %s взяли %s отказали %s, т.е. прошло %.0f%%. Доезд %.1f%%' % (
        #             w, #ws_dict[w],
        #         u'  %s взяли %s отказали %s, т.е. прошло %.0f%%' % (
        #             w,  # ws_dict[w],
        #             counts[w+'-wp-accepted'],
        #             counts[w+'-wp-declined'],
        #             tell_perc(w),
        #             tell_sonofabitchiness(w),
        #         ) for w, c in sorted(
        #             #filter(lambda t: t[1] == 0, c_iter),
        #             filter(lambda t: True, c_iter),
        #             key=lambda x: tell_perc(x[0]), reverse=True
        #         )
        #     ]
        # )
        total_accepted = sum(v for k, v in list(counts.items()) if k.endswith('-wp-accepted'))
        total_cunts = sum(v for k, v in list(counts.items()) if k.endswith('-cunts'))
        waiting = sum(v for k, v in list(counts.items()) if k.endswith('-has-ass'))
        mensaje += '\n\nВсего принятых на сейчас: *%s*' % total_accepted
        mensaje += '\nЖдут решения (с заданием): %s' % waiting

        mensaje += '\nНе доехали в целом: %.1f%%' % (
            100 * (total_accepted - total_cunts) / total_accepted
        )

        TelegramBot = Bot(TOKEN)
        # txt_bucks += '\n'.join(
        #    [
        #        'Я буду продолжать писать пока мы не разберём все заявки',
        #        'Я вижу в базе заявок с заданием, но без решения по ним: {waiting}',
        #    ]
        # )
        # TelegramBot.sendMessage(LSH_MAIN_CHAT_ID, txt_bucks, parse_mode='Markdown', disable_notification=True)
        TelegramBot.sendMessage(
            LSH_MAIN_CHAT_ID, txt_bucks, parse_mode='Markdown', disable_notification=True,
        )
        # TelegramBot.sendMessage(LSH_MAIN_CHAT_ID, u'Доброе утро!', disable_notification=True)
        # TelegramBot.sendMessage(LSH_ORGS_CHAT_ID, mensaje, parse_mode='Markdown', disable_notification=True)
        TelegramBot.sendMessage(
            LSH_DRCTRT_CHAT_ID, mensaje, parse_mode='Markdown', disable_notification=True,
        )
