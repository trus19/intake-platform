from django import template
from intake_platform.intake.tools import has_buck, has_buck_after_buck

register = template.Library()


@register.filter
def is_passable(case):
    u = case.user
    has_first_step_buck = has_buck(u, "passed_first_step")
    if not has_first_step_buck:
        return True

    if has_buck_after_buck(u, "retry_granted", "passed_first_step"):
        return True

    return False


@register.simple_tag
def w_slug_to_name(w_slug, slug2name_dict):
    if w_slug in slug2name_dict:
        return slug2name_dict[w_slug]
    else:
        return w_slug


@register.simple_tag
def p_slug_to_name(p_slug, slug2name_dict):
    if p_slug in slug2name_dict:
        return slug2name_dict[p_slug]
    else:
        return p_slug
