from typing import Dict, List, Optional

from annoying.decorators import render_to
from annoying.functions import get_object_or_None, get_config
from django.shortcuts import redirect
from django.contrib import messages

from intake_platform.intake.models import UserPhoto, UnderageUserInfo
from intake_platform.intake.tools import first_step_completed, has_buck, set_curator
from .tools import get_user, non, get_app_profile_context_dict


@render_to()
def profile(request):
    ''' Основная страница пользователя после логина.
    FIXME: Сейчас этот view громоздок, потому что это, по сути, три разных view.
    '''
    u = get_user(request)

    if u.is_anonymous:
        messages.error(request, 'Ваш пользователь не найден, вы точно вошли?')
        return redirect('index')

    if not has_buck(u, 'passed_idiot_test'):
        return redirect('intake:check_user_not_dumb_prequel')

    context = {}

    context['INTAKE_IS_CLOSED'] = get_config('INTAKE_IS_CLOSED', False)

    prof = u.userprofile
    photo = non(get_object_or_None(UserPhoto, user=u))
    context['profile'] = prof
    context['photo'] = photo

    if u.email.endswith('@letnyayashkola.org') and not prof.is_curator:
        curators: Dict[str, List[str]] = get_config('CURATORS', {})
        ws_list: Optional[List[str]] = curators.get(u.email)
        if ws_list is not None:
            prof.is_curator = True
            prof.save()
            for workshop_slug in ws_list:
                set_curator(u, workshop_slug, programs=None)

    if u.userprofile.is_nabor or u.userprofile.is_curator:
        if not photo:
            messages.warning(request, u'Рекомендуем повесить фотографию')
        context['TEMPLATE'] = 'common/profile_staff.html'
        return context

    p_1st_step = first_step_completed(prof)
    p_has_buck = has_buck(u, 'complete_first_step')

    if p_has_buck:
        return redirect('intake:timeline')
    else:
        context['lock_app'] = False
        if p_1st_step:
            context['lock_button'] = False
        else:
            context['lock_button'] = True

    context['TEMPLATE'] = 'common/profile.html'
    context.update(get_app_profile_context_dict(u))

    if prof.is_underage:
        context['underage_data'] = non(get_object_or_None(UnderageUserInfo, user=u))

    return context


@render_to("common/help.html")
def help(request):
    return {}
