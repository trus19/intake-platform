from django.core.management.base import BaseCommand

from common.models import UserProfile, UserRestrictions


class Command(BaseCommand):
    help = "Toggles user nabor flag"

    def add_arguments(self, parser):
        parser.add_argument("email", help="substring", type=str)
        parser.add_argument("restriction_id", help="restriction type", type=int)

    def handle(self, *args, **options):
        email, r_id = options["email"], options["restriction_id"]

        up = UserProfile.objects.get(user__email__icontains=options["email"])
        restriction = [
            x for x in UserRestrictions.RESTRICTION_TYPE_CHOICES if x[0] == r_id
        ]
        if not restriction:
            print("Unknown restriction id %s" % r_id)
            return
        else:
            print(
                "Накладываю ограничение %s на пользователя с емейлом <%s>"
                % (restriction[0][1], email)
            )

        restriction = UserRestrictions()
        restriction.user = up.user
        restriction.restriction_type = r_id
        restriction.save()

        print("Успешно!")
