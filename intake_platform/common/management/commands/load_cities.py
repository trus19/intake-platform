import csv
from argparse import FileType

from django.core.management.base import BaseCommand

from common.models import Location
from common.utils import UnicodeReader


class Command(BaseCommand):
    help = u"Loads the cities from the ipgeobase to 'common.models.Location'"

    def add_arguments(self, parser):
        parser.add_argument("inp", type=FileType("rb"))

    def handle(self, *args, **options):
        csv.register_dialect("tabdel", delimiter="\t", quoting=csv.QUOTE_NONE)
        reader = UnicodeReader(options["inp"], dialect="tabdel")
        for row in reader:
            l = Location()
            l.city = row[1]
            l.region = row[2]
            l.district = row[3]
            l.save()
