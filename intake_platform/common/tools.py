from datetime import datetime
from threading import Thread
from typing import Any, Dict, List

import requests

from annoying.functions import get_config, get_object_or_None
from django.contrib import auth
from django.core.mail import send_mass_mail
from intake_platform.intake_platform.cache import get_wp_cache, warm_up_wp_info_cache
from intake_platform.intake.forms import WPCMotivationForm
from intake_platform.intake.models import (
    GiveBackInfo,
    PastFuturePresentInfo,
    UserActivityInfo,
    UserAdditionalContacts,
    UserEducationInfo,
    UserHandymanshipInfo,
    UserLogisticsInfo,
    WorkshopPetitionConnection,
    WorkshopProgramMetaInfo,
)
from intake_platform.intake.tools import (
    STEP1_NICE_NAMESDICT,
    has_buck,
    user_1st_step_info,
)


def non(obj):
    return obj if obj is not None else False


def get_user(request):
    if not hasattr(request, "_cached_user"):
        request._cached_user = auth.get_user(request)
    return request._cached_user


def get_rich_wpc_info(workshop_slug, program_slug):
    """ raises ValueError when there is no connection.
    """
    URL = "https://letnyayashkola.org/api/v1.0/wp/"
    w_link = u"%s/%s/%s/" % (URL, workshop_slug, program_slug)

    try:
        rich_wpc_info = requests.get(w_link, verify=False).json()
    except:  # noqa
        rich_wpc_info = {
            "w_name": u"Секретная неизвестная мастерская",
            "p_name": u"Неизвестная программа",
        }

    return rich_wpc_info


def get_workshops_info(is_adult: bool = True) -> List[Dict[str, Any]]:
    workshops_info = get_wp_cache(is_adult=is_adult)
    if not workshops_info:
        warm_up_wp_info_cache()
        workshops_info = get_wp_cache(is_adult=is_adult)
    return workshops_info


def _send_delayed_mails(emails, subject, text):
    """ A helper function to send delayed email. Depends on std library
    :py:mod`threading`.

    :param emails: iterable of email addresses to send
    :param subject: email subject
    :param text: email text
    """

    if get_config("DEBUG"):
        print("[DEBUG] Entering 'Delayed mail' thread")
        print("[DEBUG] In here I do magic with mailing")
        print("[DEBUG] Recipients: ", emails)
        print("[DEBUG] Subj: ", subject)
        print("[DEBUG] Text: ", text)

    from_email = get_config("DEFAULT_FROM_EMAIL")
    messages = []
    for email in emails:
        messages += [(subject, text, from_email, [email])]

    send_mass_mail(messages, fail_silently=False)


def send_delayed_mails(emails, subject, text):
    thr = Thread(target=_send_delayed_mails, args=[emails, subject, text])
    thr.start()


def get_start_date():
    start_tuple = get_config("LSH_DATES_DICT", {"start": "LSH_DICT NOT FOUND SUKA"})[
        "start"
    ]

    return datetime(*start_tuple).date()


def get_app_profile_context_dict(user):
    d = {}
    wpc = non(get_object_or_None(WorkshopPetitionConnection, user=user))
    uac = non(get_object_or_None(UserAdditionalContacts, user=user))
    uei = non(get_object_or_None(UserEducationInfo, user=user))
    uact = non(get_object_or_None(UserActivityInfo, user=user))
    uhnd = non(get_object_or_None(UserHandymanshipInfo, user=user))
    gibe = non(get_object_or_None(GiveBackInfo, user=user))
    trip = non(get_object_or_None(UserLogisticsInfo, user=user))
    pfpi = non(get_object_or_None(PastFuturePresentInfo, user=user))

    d["wpc"] = wpc
    d["uac"] = uac
    d["uac"] = uac
    d["uei"] = uei
    d["uact"] = uact
    d["uhnd"] = uhnd
    d["gibe"] = gibe
    d["trip"] = trip
    d["pfpi"] = pfpi

    if not gibe and user.userprofile.is_underage:
        gibe = GiveBackInfo(user=user)
        gibe.money = ""
        gibe.save()
        d["gibe"] = gibe

    if wpc:
        w_meta = get_object_or_None(
            WorkshopProgramMetaInfo,
            workshop_slug=wpc.workshop_slug,
            program_slug=wpc.program_slug,
        )
        if w_meta is not None:
            d["rich_wpc_info"] = {
                "w_name": w_meta.workshop_name,
                "p_name": w_meta.program_name,
            }
        else:
            d["rich_wpc_info"] = get_rich_wpc_info(wpc.workshop_slug, wpc.program_slug)

    p_locked_to_workshop = has_buck(user, "locked_to_workshop")
    if p_locked_to_workshop:
        d["locked_to_workshop"] = p_locked_to_workshop
        d["wpcmotivation_form"] = WPCMotivationForm(instance=wpc)

    d["1st_step_info"] = {"required": [], "optional": []}
    for k, v in user_1st_step_info(user)["tuples"]:
        name, href_tag, required = STEP1_NICE_NAMESDICT[k]
        tag = "required" if required else "optional"
        d["1st_step_info"][tag] += [(name, href_tag, v is not None)]

    return d
